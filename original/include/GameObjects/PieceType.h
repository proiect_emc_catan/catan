#pragma once

#include <stdint.h>

enum class PieceType : uint8_t {
	City,
	Settlement,
	Road,
	DevelopmentCard
};