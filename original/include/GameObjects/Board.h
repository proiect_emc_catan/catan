//
// Created by timi on 06.11.2018.
//

#ifndef CATAN_BOARD_H
#define CATAN_BOARD_H


#include <cstdint>
#include <array>
#include <vector>
#include <optional>
#include <map>
#include <set>
#include "GameObjects/Piece.h"
#include "GameObjects/ResourceType.h"
#include "GameObjects/HarborType.h"

struct RoadComparator {
    bool operator()(const std::tuple<int, int, int, int>& e1,
            const std::tuple<int, int, int, int>& e2 ) const {
        const auto e1fixed = (std::get<0>(e1) > std::get<2>(e1) ||
                std::get<0>(e1) == std::get<2>(e1) && std::get<1>(e1) > std::get<3>(e1)) ?
                        std::tuple{std::get<2>(e1), std::get<3>(e1), std::get<0>(e1), std::get<1>(e1)} : e1;
        const auto e2fixed = (std::get<0>(e2) > std::get<2>(e2) ||
                              std::get<0>(e2) == std::get<2>(e2) && std::get<1>(e2) > std::get<3>(e2)) ?
                             std::tuple{std::get<2>(e2), std::get<3>(e2), std::get<0>(e2), std::get<1>(e2)} : e2;
        return e1fixed < e2fixed;
    }
};

class Board {

public:
    // Aligned with corresponding produced resources
    enum class TileType : uint8_t {
        Hills,
        Fields,
        Forest,
        Mountains,
        Pasture,
        Desert
    };
public:
    static constexpr std::array<int, 6> tileDirectionX = {-1, -1, 0, 1, 1, 0};
    static constexpr std::array<int, 6> tileDirectionY = {0, -1, -1, -1, 0, 1};

    static int SpotDirectionX(uint8_t index, uint8_t currentLine, uint8_t currentCol);
    static int SpotDirectionY(uint8_t index, uint8_t currentLine, uint8_t currentCol); /// because of the hexagonal shape, there are different indexes for different lines
    static std::pair<int, int> StepDirection(uint8_t index, uint8_t row, uint8_t col);


public:
    Board();

	const Board::TileType& GetTileAtPosition(uint8_t rowIndex, uint8_t columnIndex) const;
	uint8_t GetNumberAtTilePosition(uint8_t rowIndex, uint8_t columnIndex) const;
    const std::array<std::vector<uint8_t>, 5>& GetNumberMap() const;
    const std::pair<int, int> GetThiefLocation() const;
	std::optional<std::shared_ptr<Piece>> GetBuildingAtCornerCoordinates(uint8_t rowIndex, uint8_t columnIndex);
	std::shared_ptr<Piece> GetRoadAtEdgeCoordinates(int firstRow, int firstColumn, int secondRow, int secondColumn);

    bool CanPlaceBuilding(uint8_t rowIndex, uint8_t columnIndex, PieceType pieceType, const Player & player) const;
    /// Assumes CanPlaceBuilding was already checked
    void PlaceBuilding(uint8_t rowIndex, uint8_t columnIndex, std::shared_ptr<Piece> piece);

    /// To be used on the first round of the game, for the second free settlement
    void TriggerResourceGatheringForBuilding(Player &player, uint8_t buildRow, uint8_t buildCol);
    void TriggerResourceGathering(uint8_t tileRow, uint8_t tileCol);
    void TriggerResourceGathering(uint8_t diceValue);

    bool CanPlaceRoad(uint8_t row1, uint8_t col1, uint8_t row2, uint8_t col2,PieceType pieceType , const Player & player) const;
    /// Assumes CanPlaceRoad was already checked
    void PlaceRoad(uint8_t row1, uint8_t col1, uint8_t row2, uint8_t col2, std::shared_ptr<Piece>);

    /// Proper place for this method is in Board as Board has knowledge about the sea discounts
    bool CanPlayerExchangeWithBank(const Player &player, ResourceType giveResource) const;

    void PlayerExchangeWithBank(Player& player, ResourceType giveResource,
                                   ResourceType getResource);

    /// Assumes Player has card was already checked
    void PlayerPlayCard(Player& player, DevelopmentCardAction action);

    void MoveThief(Player& whoMoves, uint8_t tileRow, uint8_t tileCol);

    std::pair<const Player&, uint8_t > GetLongestRoad() const;
    uint8_t CountBuildingScore(const Player& player) const;

private:
    void InitTileMap();
    void InitNumberMap();
    void InitBuildSpotMap();
    void InitHarbors();

    /// Checks if there is a road connected to this buildingSpot
    bool ContinueRoad(uint8_t rowIndex, uint8_t colIndex, const Player& owner) const;
    uint8_t CountHighNumberNeighbours(uint8_t row, uint8_t col) const;
    std::pair<uint8_t , uint8_t > FirstFreeNeighbourhood() const;
    std::array<std::pair<uint8_t , uint8_t >, 6> GetTileNeighbours(uint8_t tileRow, uint8_t tileCol) const;
    ResourceType ResourceFrom(uint8_t tileRow, uint8_t tileCol) const;
    uint8_t CostFactorInHarbor(const Player &player, ResourceType resource) const;
    uint8_t GetLongestPathForPlayer(const std::pair<uint8_t, uint8_t>& position,
            const Player& player, std::set<const Piece*>& visited) const;

private:
    std::array<std::vector<Board::TileType>, 5> tileMap;
    std::array<std::vector<std::optional<std::shared_ptr<Piece>>>, 6> buildSpotMap;
    std::array<std::vector<uint8_t>, 5> numberMap;
    std::map<std::pair<int, int>, HarborType> harbors;
    std::map<std::tuple<int, int, int , int>, std::shared_ptr<Piece>, RoadComparator> placedRoads;
    std::pair<int, int> thiefLocation;
};


#endif //CATAN_BOARD_H
