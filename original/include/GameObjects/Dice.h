#pragma once

#include <random>

class Dice
{
public:

	explicit Dice(int numberOfSides);
	uint8_t RollOneDie();
	uint8_t RollTwoDice();
private:
	std::random_device m_seedGenerator;
	std::mt19937 m_numberGenerator{ m_seedGenerator() };
	std::uniform_int_distribution<int> m_distribution;
};

