#ifndef CATAN_RESOURCETYPE_H
#define CATAN_RESOURCETYPE_H

#include <cstdint>

enum class ResourceType : uint8_t {
	Brick,
	Grain,
	Lumber,
	Ore,
	Wool,
	None
};

#endif //CATAN_RESOURCE_H
