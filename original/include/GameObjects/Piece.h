//
// Created by timi on 06.11.2018.
//

#ifndef CATAN_PIECE_H
#define CATAN_PIECE_H

class Player;

#include <cstdint>
#include <array>
#include "GameObjects/Player.h"
#include "GameObjects/PieceType.h"

class Piece {
public:
	static Player nobody; // used for empty reference
public:
    static const std::array<const std::array<uint8_t , 5>, 4> cost;
    static const std::array<uint8_t, 4> pieceValue;

public:
	Piece();
	virtual ~Piece() = default;
    Piece(PieceType type, Player& owner);
	Piece& operator=(const Piece&);
    Player& GetOwner() const;
    PieceType GetType() const;

private:
    PieceType m_type;
    Player& m_owner;

};


#endif //CATAN_PIECE_H
