#pragma once

#include "Graphics/DrawableObject.h"
#include "GameObjects/Piece.h"

class DrawablePiece : public DrawableObject
{
public:
	DrawablePiece(const Piece &);
	~DrawablePiece() = default;
private:
	static std::vector<std::string> m_playerNames;
	static sf::Texture m_pieceTexture;
	std::shared_ptr<Piece> m_piece;
};

