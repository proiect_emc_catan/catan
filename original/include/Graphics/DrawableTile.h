#pragma once

#include "Graphics/DrawableObject.h"
#include "GameObjects/Board.h"

class DrawableTile : public DrawableObject
{
public:
	DrawableTile();
	~DrawableTile() = default;

	void LoadNewTile(const Board::TileType &);
private:
	static sf::Texture m_tileTexture;
};

