#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>
#include <string>

class DrawableObject
{
public:
	explicit DrawableObject(const std::string & = "");
	virtual ~DrawableObject() = default;

	virtual void Draw(sf::RenderWindow &);
	void SetPosition(const sf::Vector2f &);
	void SetRotation(float angle);
	void Transpose(const sf::Vector2f &);
	void SetOriginToCenter();
	void SetOriginToDefault();

	virtual bool IsClicked(const sf::Vector2i &);

	const sf::Sprite& GetSprite() const;
	const sf::FloatRect GetBoundingBox() const;
	const sf::Vector2f& GetPosition() const;
protected:
	static sf::Font m_font;
	sf::Sprite m_sprite;
};