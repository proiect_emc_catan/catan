#pragma once

class GameController;

#include <array>
#include <vector>
#include <string>
#include <variant>

#include "Graphics/BasicWindow.h"
#include "Graphics/DrawableObject.h"
#include "Graphics/DrawableLayer.h"
#include "Graphics/DrawableNumberCircle.h"
#include "Graphics/DrawablePiece.h"
#include "Graphics/DrawableThief.h"
#include "Graphics/DrawableText.h"
#include "Graphics/DrawableTile.h"
#include "Graphics/DrawableResourceIcon.h"
#include "Graphics/DrawableBackground.h"
#include "Graphics/DrawableWindowBackground.h"
#include "Graphics/Button.h"
#include "Graphics/Textbox.h"
#include "Graphics/WindowEventHandler.h"

#include "Logic/GameController.h"

#include "GameObjects/DevelopmentCardAction.h"

class GUI
{
public:
	GUI(BasicWindow &, GameController &);
	~GUI() = default;

	void CheckIfAButtonIsPressed(const sf::Vector2i &);
	std::variant<std::string, DevelopmentCardAction, int> GetPressedButtonInfo();
	std::tuple<uint16_t, uint16_t> GetCornerCoordinates(const sf::Vector2i &) const;
	std::tuple<uint16_t, uint16_t, uint16_t, uint16_t> GetEdgeCoordinates(const sf::Vector2i &) const;
	std::array<int8_t, 5> GetResourceQuantitiesFromTradeTextboxes() const;
	void ActivateClickedTextbox(const sf::Vector2i &);
	void AddLetterToActiveTextbox(char);
	void RemoveLetterFromActiveTextbox();

	void MoveThiefOnBoard(const std::pair<int, int> &);
	std::pair<uint8_t, uint8_t> GetNewThiefPosition(const sf::Vector2i &);
	void RefreshPieces();
	void RefreshIcons(const std::array<uint8_t, 5> &);
	void RefreshCards(const std::array<uint16_t, 4> &);
	void DrawWindow(const std::string &);
	void EraseWindow();
	void Draw();

	void SetGameInfoFeed(const std::string &);
	void UpdateWithPlayersNames();
private:
	void InitializeBackground();
	void InitializeTileMap();
	void InitializeButtons();
	void InitializeResourceIcons();
	void InitializeTradeWindow();
	void InitializeTradeWithBankWindow();
	void InitializeTradeWithPlayerWindow();
	void DrawTradeWindow();
	void DrawTradeWithBankWindow();
	void DrawTradeWithPlayerWindow();
	void AddLayer(const std::string &);
private:
	// connections to other game objects
	BasicWindow *m_whereToDrawPointer;
	GameController *m_controllerPointer;
	// layers
	std::map<std::string, DrawableLayer> m_layers;
	// objects that'll be drawn (tiles, numbers, pieces)
	DrawableText m_feedText;
	DrawableBackground m_background;
	std::array<std::vector<DrawableTile>, 5> m_drawableTiles;
	std::array<std::vector<DrawableNumberCircle>, 5> m_drawableNumberCircles;
	std::vector<DrawablePiece> m_piecesOnBoard;
	std::array<DrawableResourceIcon, 5> m_resourceIcons;
	DrawableThief m_thiefSprite;
	Textbox *m_activeTextbox;
	// buttons
	Button<std::string> m_rollButton;
	Button<std::string> m_tradeButton;
	Button<std::string> m_exitButton;
	Button<std::string> m_endTurnButton;
	Button<DevelopmentCardAction> m_knightButton;
	Button<DevelopmentCardAction> m_buildRoadsButton;
	Button<DevelopmentCardAction> m_yearOfPlentyButton;
	Button<DevelopmentCardAction> m_monopolyButton;
	Button<std::string> m_buyCardButton;
	// player or bank window
	DrawableWindowBackground m_playerOrBankWindowBackground;
	Button<std::string> m_playerOrBankWindowPlayerButton;
	Button<std::string> m_playerOrBankWindowBankButton;
	// trade with bank window
	DrawableWindowBackground m_tradeWithBankWindowBackground;
	std::array<Button<std::string>, 5> m_tradeWithBankWindowResourceButtons;
	// trade with player window
	DrawableWindowBackground m_tradeWithPlayerWindowBackground;
	std::array<Button<std::string>, 4> m_tradeWithPlayerWindowPlayerButtons;
	std::array<Textbox, 5> m_tradeWithPlayerWindowTextboxes;
};