#pragma once

#include "Graphics/DrawableObject.h"

class DrawableWindowBackground : public DrawableObject
{
public:
	DrawableWindowBackground();
	~DrawableWindowBackground() = default;

	void SetWindowSize(const sf::Vector2u &);
private:
	static sf::Texture m_windowBackgroundTexture;
};

