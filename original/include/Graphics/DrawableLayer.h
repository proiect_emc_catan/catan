#pragma once

class BasicWindow;

#include <vector>
#include <memory>
#include "Graphics/DrawableObject.h"
#include "Graphics/BasicWindow.h"

class DrawableLayer
{
public:
	void AddObjectInLayer(DrawableObject &);
	void Draw(BasicWindow &);

	const std::vector<DrawableObject*>& GetObjects() const;
	bool IsObjectInLayer(const DrawableObject& object) const;
	void ClearLayer();
private:
	std::vector<DrawableObject*> m_objectsInLayer;
};

