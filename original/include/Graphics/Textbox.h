#pragma once

#include <string>

#include "SFML/Graphics/Text.hpp"
#include "Graphics/DrawableObject.h"

class Textbox : public DrawableObject
{
public:
	Textbox(const std::string & = "");
	~Textbox() = default;

	void SetString(const std::string &);
	void AddString(const std::string &);
	void AddLetter(const char &);
	void RemoveLetter();
	void Draw(sf::RenderWindow &) override;
	bool IsClicked(const sf::Vector2i &) override;
	const std::string& GetString() const;
	const std::string& Deactivate();
private:
	static sf::Texture m_textboxTexture;
	std::string m_textboxString;
	sf::Text m_stringOnScreen;
	bool m_active;
};