//
// Created by timi on 24.11.2018.
//

#include <numeric>
#include "GameObjects/DevelopmentCard.h"
#include "GameObjects/Dice.h"

ResourceArray DevelopmentCard::pool = { 15, 2, 2, 2, 5 };


DevelopmentCard::DevelopmentCard(Player &owner) : Piece(PieceType::DevelopmentCard, owner) {
    uint8_t total = std::accumulate(pool.begin(), pool.end(), static_cast<uint8_t >(0));
    if (total == 0)
        throw "Development cards used up";
    uint8_t choice = Dice(total).RollOneDie();
    uint8_t sum = 0;
    for (uint8_t i = 0; i < pool.size(); i++) {
        sum += pool[i];
        if (choice <= sum) {
            m_Action = DevelopmentCardAction (i);
            pool[i]--;
            break;
        }
    }
}

DevelopmentCardAction DevelopmentCard::GetAction() const {
    return m_Action;
}
