

#include "GameObjects/Dice.h"


Dice::Dice(int numberOfSides)
	:m_distribution(1, numberOfSides)
{
}

uint8_t Dice::RollOneDie()
{
	return m_distribution(m_numberGenerator);
}

uint8_t Dice::RollTwoDice() {
	return m_distribution(m_numberGenerator) + m_distribution(m_numberGenerator);
}
