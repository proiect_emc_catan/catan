//
// Created by timi on 06.11.2018.
//

#include "GameObjects/Piece.h"

Player Piece::nobody;

const std::array<const ResourceArray, 4> Piece::cost = {
		std::array<uint8_t , 5>({0, 2, 0, 3, 0}),
		std::array<uint8_t , 5>({1, 1, 1, 0, 1}),
		std::array<uint8_t , 5>({1, 0, 1, 0, 0}),
		std::array<uint8_t , 5>({0, 1, 0, 1, 1})
};

const std::array<uint8_t, 4> Piece::pieceValue = { 2, 1, 0, 0 };

Piece::Piece() : m_owner(nobody) {

}

Piece::Piece(PieceType type, Player &owner) : m_type(type), m_owner(owner) {

}

Piece& Piece::operator=(const Piece &other) {
	m_type = other.m_type;
	m_owner = other.m_owner;
	return *this;
}

Player &Piece::GetOwner() const {
	return m_owner;
}

PieceType Piece::GetType() const {
	return m_type;
}
