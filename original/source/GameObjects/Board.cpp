#include <random>

//
// Created by timi on 06.11.2018.
//

#include <algorithm>
#include <tuple>
#include <GameObjects/Board.h>
#include <GameObjects/Dice.h>

#include "GameObjects/DevelopmentCard.h"
#include "GameObjects/Board.h"

Board::Board() {
	InitTileMap();
	InitNumberMap();
	InitBuildSpotMap();
	InitHarbors();
}

const Board::TileType & Board::GetTileAtPosition(uint8_t rowIndex, uint8_t columnIndex) const
{
	return tileMap.at(rowIndex).at(columnIndex);
}

uint8_t Board::GetNumberAtTilePosition(uint8_t rowIndex, uint8_t columnIndex) const
{
	return numberMap.at(rowIndex).at(columnIndex);
}

void Board::InitTileMap() {
	tileMap[0].resize(3);
	tileMap[1].resize(4);
	tileMap[2].resize(5);
	tileMap[3].resize(4);
	tileMap[4].resize(3);
	std::array<TileType, 19> options = {
			TileType::Hills, TileType::Hills, TileType::Hills,
			TileType::Fields, TileType::Fields, TileType::Fields, TileType::Fields,
			TileType::Forest, TileType::Forest, TileType::Forest, TileType::Forest,
			TileType::Mountains, TileType::Mountains, TileType::Mountains,
			TileType::Pasture, TileType::Pasture, TileType::Pasture, TileType::Pasture,
			TileType::Desert
	};
	std::shuffle(options.begin(), options.end(), std::mt19937(std::random_device()()));
	uint8_t index = 0;
	for (auto& tileRow : tileMap)
		for (auto& tile : tileRow)
			tile = options[index++];
}

void Board::InitNumberMap() {
	numberMap[0].resize(3);
	numberMap[1].resize(4);
	numberMap[2].resize(5);
	numberMap[3].resize(4);
	numberMap[4].resize(3);
	std::array<uint8_t, 18> options = { 2, 3, 3, 4, 4, 5, 5, 6, 6, 8, 8, 9, 9, 10, 10, 11, 11, 12 };
	std::shuffle(options.begin(), options.end(), std::mt19937(std::random_device()()));
	uint8_t index = 0;
	for (uint8_t row = 0; row < numberMap.size(); ++row)
		for (uint8_t col = 0; col < numberMap[row].size(); ++col)
			if (tileMap[row][col] != TileType::Desert)
				numberMap[row][col] = options[index++];
			else
				thiefLocation = { row, col };
	for (uint8_t row = 0; row < numberMap.size(); ++row)
		for (uint8_t col = 0; col < numberMap[row].size(); ++col)
			if ((numberMap.at(row).at(col) == 6 || numberMap.at(row).at(col) == 8) &&
				CountHighNumberNeighbours(row, col) != 0) {
				const auto&[crow, ccol] = FirstFreeNeighbourhood();
				if (crow == thiefLocation.first && ccol == thiefLocation.second)
					thiefLocation = {row, col};
				std::swap(tileMap[row][col], tileMap[crow][ccol]);
				std::swap(numberMap[row][col], numberMap[crow][ccol]);
			}
}

uint8_t Board::CountHighNumberNeighbours(uint8_t row, uint8_t col) const {
	uint8_t count = 0;
	for (uint8_t d = 0; d < tileDirectionX.size(); d++) {
		int newRow = row + tileDirectionX[d];
		int newCol = col + tileDirectionY[d];
		if (newRow >= 0 && newRow < numberMap.size() &&
			newCol >= 0 && newCol < numberMap[newRow].size() &&
			(numberMap.at(newRow).at(newCol) == 6 || numberMap.at(newRow).at(newCol) == 8))
			++count;
	}
	return count;
}

std::pair<uint8_t, uint8_t> Board::FirstFreeNeighbourhood() const {
	for (uint8_t row = 0; row < numberMap.size(); ++row)
		for (uint8_t col = 0; col < numberMap[row].size(); ++col)
			if (numberMap.at(row).at(col) != 6 && numberMap.at(row).at(col) != 8 &&
				CountHighNumberNeighbours(row, col) == 0) {
				return { row, col };
			}
	throw "No free neighbourhood found";
}

void Board::InitBuildSpotMap() {
	buildSpotMap[0].resize(7);
	buildSpotMap[1].resize(9);
	buildSpotMap[2].resize(11);
	buildSpotMap[3].resize(11);
	buildSpotMap[4].resize(9);
	buildSpotMap[5].resize(7);
}

void Board::InitHarbors() {
    harbors[{0, 0}] = HarborType ::ThreeToOne;
    harbors[{0, 1}] = HarborType ::ThreeToOne;
    harbors[{0, 3}] = HarborType ::GrainChange;
    harbors[{0, 4}] = HarborType ::GrainChange;

    harbors[{1, 0}] = HarborType ::LumberChange;
    harbors[{1, 7}] = HarborType ::OreChange;
    harbors[{1, 8}] = HarborType ::OreChange;

    harbors[{2, 1}] = HarborType ::LumberChange;
    harbors[{2, 10}] = HarborType ::ThreeToOne;

    harbors[{3, 1}] = HarborType ::BrickChange;
    harbors[{3, 10}] = HarborType ::ThreeToOne;

    harbors[{4, 0}] = HarborType ::BrickChange;
    harbors[{4, 7}] = HarborType ::WoolChange;
    harbors[{4, 8}] = HarborType ::WoolChange;

    harbors[{5, 0}] = HarborType ::ThreeToOne;
    harbors[{5, 1}] = HarborType ::ThreeToOne;
    harbors[{5, 3}] = HarborType ::ThreeToOne;
    harbors[{5, 4}] = HarborType ::ThreeToOne;
}



int Board::SpotDirectionX(uint8_t index, uint8_t currentLine, uint8_t currentCol) {
	if (currentLine <= 2 && currentCol % 2 == 1)
		return -1 * !index; /// Basically {-1, 0, 0}[index] but simpler
	else if (currentLine <= 2 && currentCol % 2 == 0)
		return std::array{0, 1, 0}[index];
	else if (currentCol % 2 == 1)
		return std::array{0, 1, 0}[index];
	else
		return std::array{-1, 0, 0}[index];
}

int Board::SpotDirectionY(uint8_t index, uint8_t currentLine, uint8_t currentCol) {
	if (currentLine <= 2 && currentCol % 2 == 1)
		return std::array{-1, -1, 1}[index];
	else if (currentLine < 2 && currentCol % 2 == 0)
		return std::array{-1, 1, 1}[index];
	else if (currentLine == 2 && currentCol % 2 == 0)
		return std::array{-1, 0, 1}[index];
	else if (currentLine == 3 && currentCol % 2 == 1)
		return std::array{-1, -1, 1}[index];
	else if (currentLine == 3 && currentCol % 2 == 0)
		return std::array{0, -1, 1}[index];
	else if (currentCol % 2 == 1)
		return std::array{-1, -1, 1}[index];
	else
		return std::array{1, -1, 1}[index];
//	if (index == 1) return -1;
//	if (index == 2) return 1;
//	if (currentLine <= 2) return -1;
//	if (currentLine == 3) return 0;
//	return 1;
}

std::pair<int, int> Board::StepDirection(uint8_t index, uint8_t row, uint8_t col) {
	return { row + SpotDirectionX(index, row, col), col + SpotDirectionY(index, row, col) };
}


void Board::PlaceBuilding(uint8_t rowIndex, uint8_t columnIndex, std::shared_ptr<Piece> piece) {
	buildSpotMap[rowIndex][columnIndex] = piece;
    if (piece->GetType() == PieceType::Settlement &&
        piece->GetOwner().GetAvailable(PieceType::Settlement) == 3 && /// Just placing the second settlement
        piece->GetOwner().GetAvailable(PieceType::City) == 4)
        TriggerResourceGatheringForBuilding(piece->GetOwner(), rowIndex, columnIndex);
}

bool Board::CanPlaceBuilding(uint8_t rowIndex, uint8_t columnIndex, PieceType pieceType , const Player & player) const {
	if (rowIndex < 0 || rowIndex >= buildSpotMap.size())
		return false;
	if (columnIndex < 0 || columnIndex >= buildSpotMap[rowIndex].size())
		return false;
	if (buildSpotMap[rowIndex][columnIndex].has_value()) {
		const Piece& there = *buildSpotMap[rowIndex][columnIndex].value();
		return (&there.GetOwner() == &player &&
			there.GetType() == PieceType::Settlement && pieceType == PieceType::City);
	}

	if (pieceType != PieceType::Settlement)
		return false;

	/// Checking neighbours
	for (uint8_t dir = 0; dir < 3; dir++) {
		const auto[newX, newY] = StepDirection(dir, rowIndex, columnIndex);
		if (newX >= 0 && newX < buildSpotMap.size() &&
			newY >= 0 && newY < buildSpotMap[newX].size() &&
			buildSpotMap[newX][newY].has_value())
			return false;
	}

	/// First two settlements don't need roads
	if (player.GetAvailable(PieceType::City) == 4 &&
		player.GetAvailable(PieceType::Settlement) > 3)
		return true;

	return ContinueRoad(rowIndex, columnIndex,player);
}

std::array<std::pair<uint8_t, uint8_t >, 6> Board::GetTileNeighbours(uint8_t tileRow, uint8_t tileCol) const {
	std::array<std::pair<uint8_t, uint8_t >, 6> a;
	int ados = static_cast<int>(tileRow > 2); // adds one more if condition is true
	a[0] = { tileRow, 2 * tileCol + ados };
	a[1] = { tileRow, 2 * tileCol + 1 + ados };
	a[2] = { tileRow, 2 * tileCol + 2 + ados };

	ados = static_cast<int>(tileRow < 2);
	a[3] = { tileRow + 1, 2 * tileCol + ados };
	a[4] = { tileRow + 1, 2 * tileCol + 1 + ados };
	a[5] = { tileRow + 1, 2 * tileCol + 2 + ados };

	return a;
}

void Board::TriggerResourceGathering(uint8_t tileRow, uint8_t tileCol) {
	if (tileRow < 0 || tileRow >= tileMap.size() ||
		tileCol < 0 || tileCol >= tileMap[tileRow].size())
		throw "Invalid Tile Bounds";
	if (thiefLocation.first == tileRow && thiefLocation.second == tileCol)
		return;

	auto arr = GetTileNeighbours(tileRow, tileCol);
	auto resource = ResourceFrom(tileRow, tileCol);
	if (resource == ResourceType::None)
        return;
	for (const auto& p : arr) {
		if (buildSpotMap[p.first][p.second].has_value())
			buildSpotMap[p.first][p.second].value()->GetOwner()
			.GiveResource(resource,
				Piece::pieceValue[static_cast<uint8_t>(buildSpotMap[p.first][p.second].value()->GetType())]);
	}
}


void Board::TriggerResourceGatheringForBuilding(Player &player, uint8_t buildRow, uint8_t buildCol) {
    if (buildRow < 0 || buildRow >= buildSpotMap.size() ||
        buildCol < 0 || buildCol >= buildSpotMap[buildRow].size())
        throw "Invalid Building Bounds";

    std::array<std::pair<uint8_t , uint8_t >, 3> locations{
        std::pair<uint8_t , uint8_t >{buildRow-(buildRow<=2 || buildCol%2 == 0), (buildCol-2)/2},
        std::pair<uint8_t , uint8_t >{buildRow, (buildCol-1)/2},
        std::pair<uint8_t , uint8_t >{buildRow-(buildRow >2 || buildCol%2 == 1), buildCol/2}
    };
    for (const auto&p : locations)
        if (p.first < tileMap.size() && p.second < tileMap[p.first].size())
            player.GiveResource(ResourceFrom(p.first, p.second), 1);
}


void Board::TriggerResourceGathering(uint8_t diceValue) {
	if (diceValue < 2 || diceValue > 12)
		throw "Invalid dice value";
	for (uint8_t row = 0; row < numberMap.size(); ++row)
		for (uint8_t col = 0; col < numberMap[row].size(); ++col)
			if (numberMap[row][col] == diceValue)
				TriggerResourceGathering(row, col);
}


ResourceType Board::ResourceFrom(uint8_t tileRow, uint8_t tileCol) const {
	return static_cast<ResourceType>(tileMap[tileRow][tileCol]);
}

bool Board::ContinueRoad(uint8_t rowIndex, uint8_t colIndex, const Player& owner) const {
	for (uint8_t dir = 0; dir < 3; dir++) {
		int newX = rowIndex + SpotDirectionX(dir, rowIndex, colIndex);
		int newY = colIndex + SpotDirectionY(dir, rowIndex, colIndex);
		const auto& iterator = placedRoads.find({ rowIndex, colIndex, newX, newY });
		if (iterator == placedRoads.end())
            continue;
		const Player& p = iterator->second->GetOwner();
		if (iterator != placedRoads.end() && &iterator->second->GetOwner() == &owner)
			return true;
	}
	return false;
}

bool Board::CanPlaceRoad(uint8_t row1, uint8_t col1, uint8_t row2, uint8_t col2,PieceType pieceType, const Player & player) const {
	if (pieceType != PieceType::Road)
		return false;
	if (placedRoads.find({ row1, col1, row2, col2 }) != placedRoads.end())
		return false;
	/// Checking neighbours
	bool isOk = false; /// checks if the locations are neighbours
	for (uint8_t dir = 0; dir < 3; dir++) {
		int newX = row1 + SpotDirectionX(dir, row1, col1);
		int newY = col1 + SpotDirectionY(dir, row1, col1);
		if (newX == row2 && newY == col2)
			isOk = true;
	}
	if (!isOk)
		return false;
	if (ContinueRoad(row1, col1, player) || ContinueRoad(row2, col2, player))
		return true;
	if (buildSpotMap[row1][col1].has_value() && &buildSpotMap[row1][col1].value()->GetOwner() == &player)
		return true;
	if (buildSpotMap[row2][col2].has_value() && &buildSpotMap[row2][col2].value()->GetOwner() == &player)
		return true;
	return false;
}

void Board::PlaceRoad(uint8_t row1, uint8_t col1, uint8_t row2, uint8_t col2, std::shared_ptr<Piece> piece) {
	placedRoads[{row1, col1, row2, col2}] = piece;
}

uint8_t Board::CostFactorInHarbor(const Player &player, ResourceType resource) const {
	uint8_t factor = 4;
	for (const auto&[location, harbor] : harbors) {
		const auto& spot = buildSpotMap[location.first][location.second];
		if (!spot.has_value())
			continue;
		if (&spot.value()->GetOwner() != &player)
			continue;
		if (harbor == HarborType::ThreeToOne)
			factor = 3;
		else if (static_cast<uint8_t >(harbor) == static_cast<uint8_t >(resource))
			return 2;
	}
	return factor;
}

bool Board::CanPlayerExchangeWithBank(const Player &player, ResourceType giveResource) const {

	uint8_t costFactor = CostFactorInHarbor(player, giveResource);

	return player.CanExpand(giveResource, costFactor);
}

void Board::PlayerExchangeWithBank(Player &player, ResourceType giveResource, ResourceType getResource) {

	uint8_t costFactor = CostFactorInHarbor(player, giveResource);

	player.Expand(giveResource, costFactor);
	player.GiveResource(getResource, 1);
}

std::pair<const Player &, uint8_t> Board::GetLongestRoad() const {
    uint8_t maxim = 0;
    Player* who = &Piece::nobody;
    for (uint8_t i = 0; i < buildSpotMap.size(); ++i)
        for (uint8_t j = 0; j < buildSpotMap[i].size(); ++j) {
            const auto& spot = buildSpotMap[i][j];
            if (!spot.has_value()) continue;
            std::set<const Piece*> visited;
            uint8_t length = GetLongestPathForPlayer({i, j}, spot.value()->GetOwner(), visited);
            if (length > maxim) {
            	maxim = length;
            	who = &spot.value()->GetOwner();
            }
        }
	return {*who, maxim};
}

/// NP Hard problem, so backtrack it
uint8_t Board::GetLongestPathForPlayer(const std::pair<uint8_t, uint8_t>& position, const Player &player,
									   std::set<const Piece *>& visited) const {
	auto[rowIndex, colIndex] = position;
	/// Enemy building breaks the road
	const auto& spot = buildSpotMap[rowIndex][colIndex];
	if (spot.has_value() && &spot.value()->GetOwner() != &player)
		return 0;

	uint8_t maxi = 0;
	for (uint8_t dir = 0; dir < 3; dir++) {
		int newX = rowIndex + SpotDirectionX(dir, rowIndex, colIndex);
		int newY = colIndex + SpotDirectionY(dir, rowIndex, colIndex);
		const auto& iterator = placedRoads.find({ rowIndex, colIndex, newX, newY });
		if (iterator != placedRoads.end() && &iterator->second->GetOwner() == &player) {
			if (visited.find(iterator->second.get()) == visited.end()) {
				visited.insert(iterator->second.get());
				maxi = std::max(maxi, static_cast<uint8_t >(GetLongestPathForPlayer({newX, newY}, player, visited) + 1));
				visited.erase(iterator->second.get());
			}
		}
	}
	return maxi;
}

uint8_t Board::CountBuildingScore(const Player &player) const {
    uint8_t score = 0;
    for (uint8_t i = 0; i < buildSpotMap.size(); ++i)
        for (uint8_t j = 0; j < buildSpotMap[i].size(); ++j) {
            const auto &spot = buildSpotMap[i][j];
            if (!spot.has_value()) continue;
            if (&spot.value()->GetOwner() != &player) continue;
            score += Piece::pieceValue[static_cast<uint8_t >(spot.value()->GetType())];
        }
    return score;
}

void Board::MoveThief(Player &whoMoves, uint8_t tileRow, uint8_t tileCol) {
	if (tileRow < 0 || tileRow >= tileMap.size() ||
		tileCol < 0 || tileCol >= tileMap[tileRow].size())
		throw "Invalid Tile Bounds";

	/// Rules don't specify that you can't leave the thief in the same place so we allow it
	thiefLocation.first = tileRow, thiefLocation.second = tileCol;

	auto arr = GetTileNeighbours(tileRow, tileCol);
	for (const auto& p : arr) {
		if (buildSpotMap[p.first][p.second].has_value()) {
			auto optResource = buildSpotMap[p.first][p.second].value()->GetOwner().LoseRandomResource();
			if (optResource.has_value())
				whoMoves.GiveResource(optResource.value());
		}
	}
}

const std::array<std::vector<uint8_t>, 5> &Board::GetNumberMap() const {
    return numberMap;
}

const std::pair<int, int> Board::GetThiefLocation() const {
    return thiefLocation;
}

std::optional<std::shared_ptr<Piece>> Board::GetBuildingAtCornerCoordinates(uint8_t rowIndex, uint8_t columnIndex)
{
	return buildSpotMap.at(rowIndex).at(columnIndex);
}

std::shared_ptr<Piece> Board::GetRoadAtEdgeCoordinates(int firstRow, int firstColumn, int secondRow, int secondColumn)
{
	if (placedRoads.count({ firstRow, firstColumn, secondRow, secondColumn }) == 0)
		return nullptr;
	return placedRoads[{ firstRow, firstColumn, secondRow, secondColumn }];
}

void Board::PlayerPlayCard(Player &player, DevelopmentCardAction cardAction) {
	std::shared_ptr<DevelopmentCard> card = *std::find_if(player.GetPlayableCards().begin(), player.GetPlayableCards().end(),
				[cardAction](auto card){ return card->GetAction() == cardAction; });
	if (card->GetAction() == DevelopmentCardAction::KnightCard) {
        /* Action must be handled by GameController on thief tile choice */
	}
	else if (card->GetAction() == DevelopmentCardAction::Monopoly) {
        /* Action must be handled by GameController on resource type choice */
    }
	else if (card->GetAction() == DevelopmentCardAction::YearOfPlenty) {
	    // Give 2 random resources
	    Dice dice(5);
        player.GiveResource(static_cast<ResourceType >(dice.RollOneDie()-1), 1);
        player.GiveResource(static_cast<ResourceType >(dice.RollOneDie()-1), 1);
	}
	else if (card->GetAction() == DevelopmentCardAction::RoadBuilding) {
	    // Player gets 2 free roads
        player.AddFreeRoads(2);
	}
	player.PlayCard(cardAction);
}



