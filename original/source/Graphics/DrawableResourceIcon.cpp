#include "Graphics/DrawableResourceIcon.h"

sf::Texture DrawableResourceIcon::m_iconTexture;

DrawableResourceIcon::DrawableResourceIcon(ResourceType resource, const std::string &iconText) :
	m_type(resource)
{
	if (m_iconTexture.getSize().x == 0)
	{
		m_iconTexture.loadFromFile("resources/sprites/resource_icons.png");
		m_sprite.setTexture(m_iconTexture);
	}
	else
		m_sprite.setTexture(m_iconTexture);
	if (m_type == ResourceType::Brick)
		m_sprite.setTextureRect({ 0, 0, 32, 32 });
	else if(m_type == ResourceType::Grain)
		m_sprite.setTextureRect({ 32, 0, 32, 32 });
	else if (m_type == ResourceType::Lumber)
		m_sprite.setTextureRect({ 64, 0, 32, 32 });
	else if (m_type == ResourceType::Ore)
		m_sprite.setTextureRect({ 96, 0, 32, 32 });
	else if (m_type == ResourceType::Wool)
		m_sprite.setTextureRect({ 128, 0, 32, 32 });
	m_iconText.setFont(m_font);
	m_iconText.setCharacterSize(24);
	m_iconText.setFillColor(sf::Color::Black);
	m_iconText.setOutlineColor(sf::Color::White);
	m_iconText.setOutlineThickness(3);
	SetIconCount(0);
}

void DrawableResourceIcon::Draw(sf::RenderWindow &drawHere)
{
	DrawableObject::Draw(drawHere);
	sf::Transform transform = this->m_sprite.getTransform();
	transform.translate({ 38, 6 });
	drawHere.draw(m_iconText, transform);
}

void DrawableResourceIcon::SetIconCount(uint16_t count)
{
	m_iconText.setString(std::to_string(count));
}
