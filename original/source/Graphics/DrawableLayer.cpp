
#include <Graphics/DrawableLayer.h>

#include "Graphics/DrawableLayer.h"

void DrawableLayer::AddObjectInLayer(DrawableObject &objectToAdd)
{
	m_objectsInLayer.push_back(&objectToAdd);
}

void DrawableLayer::Draw(BasicWindow &drawHere)
{
	for (auto drawableObject : m_objectsInLayer)
		drawHere.DrawObject(drawableObject);
}

const std::vector<DrawableObject*>& DrawableLayer::GetObjects() const
{
	return m_objectsInLayer;
}

void DrawableLayer::ClearLayer()
{
	m_objectsInLayer.clear();
}

bool DrawableLayer::IsObjectInLayer(const DrawableObject &object) const {
	return std::find(m_objectsInLayer.begin(), m_objectsInLayer.end(), &object) != m_objectsInLayer.end();
}
