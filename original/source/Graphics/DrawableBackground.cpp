#include "Graphics/DrawableBackground.h"

sf::Texture DrawableBackground::m_backgroundTexture;

DrawableBackground::DrawableBackground()
{
	if (m_backgroundTexture.getSize().x == 0)
	{
		m_backgroundTexture.loadFromFile("resources/sprites/background.png");
		m_sprite.setTexture(m_backgroundTexture);
	}
	else
		m_sprite.setTexture(m_backgroundTexture);
}

void DrawableBackground::LoadTexture(const std::string &filePath, const sf::Vector2u &screenSize)
{
	m_backgroundTexture.loadFromFile(filePath);
	m_sprite.setTexture(m_backgroundTexture);
	float xPercent = m_backgroundTexture.getSize().x / screenSize.x, yPercent = m_backgroundTexture.getSize().y / screenSize.y;
	m_sprite.setScale({ xPercent, yPercent });
}
