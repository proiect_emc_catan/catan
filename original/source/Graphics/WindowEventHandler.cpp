#include <typeinfo>

#include "Graphics/WindowEventHandler.h"

ResourceType resourceTypeFromString(const std::string& );

void WindowEventHandler::MainWindowHandle(BasicWindow &eventfulWindow, GameController &gameController, GUI &gui)
{
	sf::Event event;
	while (eventfulWindow.GetEvent(event)) {
		switch (event.type)
		{
            case sf::Event::Closed:
                eventfulWindow.CloseWindow();
                break;
            case sf::Event::MouseButtonPressed:
            {
                // all you need for button pressing action
                gui.CheckIfAButtonIsPressed({ event.mouseButton.x, event.mouseButton.y });
                if (gameController.GetCurrentPlayerState() == GameController::CurrentPlayerState::InitialState) {
                    std::tuple<uint16_t, uint16_t, uint16_t, uint16_t> edgeCoords = gui.GetEdgeCoordinates({ event.mouseButton.x,event.mouseButton.y });
                    std::tuple<uint16_t, uint16_t> cornerCoords = gui.GetCornerCoordinates({ event.mouseButton.x,event.mouseButton.y });
                    if (std::get<0>(edgeCoords) != static_cast<uint16_t >(-1))
                    {
                        if (gameController.GetBoard().CanPlaceRoad(std::get<0>(edgeCoords), std::get<1>(edgeCoords), std::get<2>(edgeCoords), std::get<3>(edgeCoords), PieceType::Road, gameController.GetCurrentPlayer())) {
                            if (gameController.GetCurrentPlayer().CanPickPiece(PieceType::Road)) {
                                auto boughtPiece = gameController.GetCurrentPlayer().BuyPiece(PieceType::Road);
                                gameController.builtRoad = true;
                                gameController.GetBoard().PlaceRoad(std::get<0>(edgeCoords), std::get<1>(edgeCoords), std::get<2>(edgeCoords), std::get<3>(edgeCoords), boughtPiece);
                                gui.RefreshPieces();
                            }
                        }
                    }
                    if (std::get<0>(cornerCoords) != static_cast<uint16_t >(-1)) {
                        if (gameController.GetBoard().CanPlaceBuilding(std::get<0>(cornerCoords), std::get<1>(cornerCoords), PieceType::Settlement, gameController.GetCurrentPlayer())) {
                            if (gameController.GetCurrentPlayer().CanPickPiece(PieceType::Settlement)) {
                                auto boughtPiece = gameController.GetCurrentPlayer().BuyPiece(PieceType::Settlement);
                                gameController.builtSettlement = true;
                                gameController.GetBoard().PlaceBuilding(std::get<0>(cornerCoords), std::get<1>(cornerCoords), boughtPiece);
                                gui.RefreshPieces();
                            }
                        }
                    }
                    if (gameController.builtRoad && gameController.builtSettlement) {
                        gameController.NextTurn();
                        gui.SetGameInfoFeed("Player: " + gameController.GetCurrentPlayer().GetName() + " on move");
                    }
                } else if (gameController.GetCurrentPlayerState() == GameController::CurrentPlayerState::MovingThief) {
                    std::pair<uint8_t, uint8_t> newThiefPosition = gui.GetNewThiefPosition(
                            {event.mouseButton.x, event.mouseButton.y});
                    if (newThiefPosition.first == static_cast<uint8_t >(-1)) {
                        return;
                    }
                    gameController.GetBoard().MoveThief(gameController.GetCurrentPlayer(), newThiefPosition.first,
                                                        newThiefPosition.second);
                    gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::PressingButtons);
                    gui.MoveThiefOnBoard(std::pair<int, int>{newThiefPosition.first, newThiefPosition.second});
                } else if (gameController.GetCurrentPlayerState() == GameController::CurrentPlayerState::PressingButtons) {
                    std::tuple<uint16_t, uint16_t, uint16_t, uint16_t> edgeCoords = gui.GetEdgeCoordinates(
                            {event.mouseButton.x, event.mouseButton.y});
                    std::tuple<uint16_t, uint16_t> cornerCoords = gui.GetCornerCoordinates(
                            {event.mouseButton.x, event.mouseButton.y});
                    if (std::get<0>(edgeCoords) != static_cast<uint16_t >(-1)) {
                        if (gameController.GetBoard().CanPlaceRoad(std::get<0>(edgeCoords), std::get<1>(edgeCoords),
                                                                   std::get<2>(edgeCoords), std::get<3>(edgeCoords),
                                                                   PieceType::Road,
                                                                   gameController.GetCurrentPlayer())) {
                            if (gameController.GetCurrentPlayer().CanPickPiece(PieceType::Road)) {
                                auto boughtPiece = gameController.GetCurrentPlayer().BuyPiece(PieceType::Road);
                                gameController.GetBoard().PlaceRoad(std::get<0>(edgeCoords), std::get<1>(edgeCoords),
                                                                    std::get<2>(edgeCoords), std::get<3>(edgeCoords),
                                                                    boughtPiece);
                                gui.RefreshPieces();
                                //gui.RefreshResources(gameController.GetCurrentPlayer());
                            }
                        }
                    }
                    if (std::get<0>(cornerCoords) != static_cast<uint16_t >(-1)) {
                        if (gameController.GetBoard().CanPlaceBuilding(std::get<0>(cornerCoords),
                                                                       std::get<1>(cornerCoords), PieceType::City,
                                                                       gameController.GetCurrentPlayer())) {
                            if (gameController.GetCurrentPlayer().CanPickPiece(PieceType::City)) {
                                auto boughtPiece = gameController.GetCurrentPlayer().BuyPiece(PieceType::City);
                                gameController.GetBoard().PlaceBuilding(std::get<0>(cornerCoords),
                                                                        std::get<1>(cornerCoords), boughtPiece);
                                gui.RefreshPieces();
                                //gui.RefreshResources(gameController.GetCurrentPlayer());
                            }
                        } else if (gameController.GetBoard().CanPlaceBuilding(std::get<0>(cornerCoords),
                                                                              std::get<1>(cornerCoords),
                                                                              PieceType::Settlement,
                                                                              gameController.GetCurrentPlayer())) {
                            if (gameController.GetCurrentPlayer().CanPickPiece(PieceType::Settlement)) {
                                auto boughtPiece = gameController.GetCurrentPlayer().BuyPiece(PieceType::Settlement);
                                gameController.GetBoard().PlaceBuilding(std::get<0>(cornerCoords),
                                                                        std::get<1>(cornerCoords), boughtPiece);
                                gui.RefreshPieces();
                                //gui.RefreshResources(gameController.GetCurrentPlayer());
                            }
                        }
                    }
                }
                gui.RefreshIcons(gameController.GetCurrentPlayer().GetAvailableResources());
                break;
            }
            case sf::Event::MouseButtonReleased:
            {
                // this is where you check for button info

                auto pressedButtonInfo = gui.GetPressedButtonInfo();
                if (gameController.GetCurrentPlayerState() == GameController::CurrentPlayerState::PressingButtons) {
                    if (std::holds_alternative<std::string>(pressedButtonInfo)) {
                        if (std::get<std::string>(pressedButtonInfo) == "trade") {
                            gui.DrawWindow("tradeWindow");
                            gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::Trade);
                        } else if (std::get<std::string>(pressedButtonInfo) == "endTurn") {
                            if (!gameController.m_diceRolledThisTurn)
                                continue;
                            gameController.NextTurn();
                            gui.SetGameInfoFeed("Player: " + gameController.GetCurrentPlayer().GetName() + " on move");
                        } else if (std::get<std::string>(pressedButtonInfo) == "buyCard") {
                            if (gameController.GetCurrentPlayer().CanPickPiece(PieceType::DevelopmentCard))
                                gameController.GetCurrentPlayer().BuyPiece(PieceType::DevelopmentCard);
                        } else if (std::get<std::string>(pressedButtonInfo) == "roll" &&
                                   gameController.m_diceRolledThisTurn == false) {
                            if (gameController.RollDie() == 7) {
                                for (uint8_t index = 0; index < gameController.GetNumberOfPlayers(); ++index) {
                                    gameController.GetSpecificPlayer(index).DiscardHalfResourcesIfNecessary();
                                }
                                gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::MovingThief);
                            } else {
                                gameController.GetBoard().TriggerResourceGathering(gameController.m_currentRolledValue);
                            }
                            gui.SetGameInfoFeed("Player: " + gameController.GetCurrentPlayer().GetName() +
                                " rolled " + std::to_string(static_cast<int>(gameController.m_currentRolledValue)));
                        }

                    }
                    else if (std::holds_alternative<DevelopmentCardAction >(pressedButtonInfo)) {
                        switch (std::get<DevelopmentCardAction>(pressedButtonInfo)) {
                            case DevelopmentCardAction::KnightCard:
                                if (gameController.GetCurrentPlayer().HasCard(DevelopmentCardAction::KnightCard) &&
                                    !gameController.m_developmentCardPlayedThisTurn) {
                                    gameController.GetBoard().PlayerPlayCard(gameController.GetCurrentPlayer(),
                                                                             DevelopmentCardAction::KnightCard);
                                    gameController.m_developmentCardPlayedThisTurn = true;
                                    gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::MovingThief);
                                    //gui.RefreshResources(gameController.GetCurrentPlayer());
                                    //gui.RefreshDevelopmentCards(gameController.GetCurrentPlayer());
                                }
                                break;
                            case DevelopmentCardAction::Monopoly:
                                if (gameController.GetCurrentPlayer().HasCard(DevelopmentCardAction::Monopoly) &&
                                    !gameController.m_developmentCardPlayedThisTurn) {
                                    gameController.GetBoard().PlayerPlayCard(gameController.GetCurrentPlayer(), DevelopmentCardAction::Monopoly);
                                    gameController.m_developmentCardPlayedThisTurn = true;

                                    gui.DrawWindow("tradeWithBankWindow");
                                    gui.SetGameInfoFeed("Choose resource to receive");
                                    gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::PickResourceForMonopoly);

                                    //gui.RefreshResources(gameController.GetCurrentPlayer());
                                    //gui.RefreshDevelopmentCards(gameController.GetCurrentPlayer());
                                }
                                break;
                            case DevelopmentCardAction::RoadBuilding:
                                if (gameController.GetCurrentPlayer().HasCard(DevelopmentCardAction::RoadBuilding) &&
                                    !gameController.m_developmentCardPlayedThisTurn) {
                                    gameController.GetBoard().PlayerPlayCard(gameController.GetCurrentPlayer(),
                                                                             DevelopmentCardAction::RoadBuilding);

                                    gameController.m_developmentCardPlayedThisTurn = true;
                                    //gui.RefreshDevelopmentCards(gameController.GetCurrentPlayer());
                                }
                                break;
                            case DevelopmentCardAction::YearOfPlenty:
                                if (gameController.GetCurrentPlayer().HasCard(DevelopmentCardAction::YearOfPlenty) &&
                                    !gameController.m_developmentCardPlayedThisTurn) {
                                    gameController.GetBoard().PlayerPlayCard(gameController.GetCurrentPlayer(),
                                                                             DevelopmentCardAction::YearOfPlenty);
                                    gameController.m_developmentCardPlayedThisTurn = true;
                                    //gui.RefreshResources(gameController.GetCurrentPlayer());
                                    //gui.RefreshDevelopmentCards(gameController.GetCurrentPlayer());
                                }
                                break;
                        }
                    }

                } else if (gameController.GetCurrentPlayerState() == GameController::CurrentPlayerState::Trade) {
                    if (std::holds_alternative<std::string>(pressedButtonInfo)) {
                        if (std::get<std::string>(pressedButtonInfo) == "trade") {
                            gui.EraseWindow();
                            gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::PressingButtons);
                        } else if (std::get<std::string>(pressedButtonInfo) == "player") {
                            gui.DrawWindow("tradeWithPlayerWindow");
                            gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::TradeWithPlayer);
                        } else if (std::get<std::string>(pressedButtonInfo) == "bank") {
                            gui.DrawWindow("tradeWithBankWindow");
                            gui.SetGameInfoFeed("Choose resource to receive");
                            gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::ChoosingWantedResource);
                        }
                    }

                    /*
                   if (typeid(pressedButtonInfo).name() == typeid(const std::string &).name()) {
                       if (std::get<std::string>(pressedButtonInfo) == "exit") {
                           gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::PressingButtons);
                       }
                       if (std::get<std::string>(pressedButtonInfo) == "bankTrade")
                       {
                           std::pair<ResourceType, ResourceType> tradeWithBankResources = gui.GetTradeWithBankResources({ event.mouseButton.x,event.mouseButton.y });
                           if (gameController.GetBoard().CanPlayerExchangeWithBank(gameController.GetCurrentPlayer(), tradeWithBankResources.first))
                           {
                               gameController.GetBoard().PlayerExchangeWithBank(gameController.GetCurrentPlayer(), tradeWithBankResources.first, tradeWithBankResources.second);
                               gui.RefreshResources(gameController.GetCurrentPlayer());
                           }
                       }
                       else if (std::get <std::string>(pressedButtonInfo) == "playerTrade")
                       {
                           std::array<int, 6> tradeWithPlayerResources = gui.GetTradeWithPlayerResources({ event.mouseButton.x,event.mouseButton.y });
                           Player otherPlayer = gameController.GetSpecificPlayer(tradeWithPlayerResources[5]);
                           std::array<uint8_t, 5> givenResources;
                           std::array<uint8_t, 5> wantedResources;
                           for (int index = 0; index < 5; ++index)
                           {
                               if (tradeWithPlayerResources[index] < 0)
                               {
                                   givenResources[index] = tradeWithPlayerResources[index] * -1;
                                   wantedResources[index] = 0;
                               }
                               else
                               {
                                   givenResources[index] = 0;
                                   wantedResources[index] = tradeWithPlayerResources[index];
                               }
                           }
                           if (gameController.GetCurrentPlayer().CanExpand(givenResources) && otherPlayer.CanExpand(wantedResources))
                           {
                               gameController.GetCurrentPlayer().ExchangeWithPlayer(givenResources, wantedResources, std::move(otherPlayer));
                               gui.RefreshResources(gameController.GetCurrentPlayer());
                           }
                       }
                   }
                   */
                } else if (gameController.GetCurrentPlayerState() == GameController::CurrentPlayerState::ChoosingWantedResource) {
                    auto nextState = [&](){
                        gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::ChoosingExchangeResource);
                        gui.SetGameInfoFeed("Choose resource to offer");
                    };
                    if (std::holds_alternative<std::string>(pressedButtonInfo)) {
                        if (std::get<std::string>(pressedButtonInfo) == "brick") {
                            gameController.wantedResource = 0;
                            nextState();
                        } else if (std::get<std::string>(pressedButtonInfo) == "grain") {
                            gameController.wantedResource = 1;
                            nextState();
                        } else if (std::get<std::string>(pressedButtonInfo) == "lumber") {
                            gameController.wantedResource = 2;
                            nextState();
                        }
                        else if (std::get<std::string>(pressedButtonInfo) == "ore") {
                            gameController.wantedResource = 3;
                            nextState();
                        }
                        else if (std::get<std::string>(pressedButtonInfo) == "wool") {
                            gameController.wantedResource = 4;
                            nextState();
                        }
                    }
                }
                else if (gameController.GetCurrentPlayerState() == GameController::CurrentPlayerState::ChoosingExchangeResource) {
                    auto nextState = [&](uint8_t exchangeIndex) {
                        if (gameController.ExchangeWithBank(exchangeIndex)) {
                            gui.SetGameInfoFeed("Player: " + gameController.GetCurrentPlayer().GetName() + " on move");
                        }
                        else {
                            gui.SetGameInfoFeed("Not enough resources");
                        }
                        gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::PressingButtons);
                        gui.EraseWindow();
                    };
                    if (std::holds_alternative<std::string>(pressedButtonInfo)) {
                        if (std::get<std::string>(pressedButtonInfo) == "brick") {
                            nextState(0);
                        } else if (std::get<std::string>(pressedButtonInfo) == "grain") {
                            nextState(1);
                        } else if (std::get<std::string>(pressedButtonInfo) == "lumber") {
                            nextState(2);
                        }
                        else if (std::get<std::string>(pressedButtonInfo) == "ore") {
                            nextState(3);
                        }
                        else if (std::get<std::string>(pressedButtonInfo) == "wool") {
                            nextState(4);
                        }
                    }
                }
                else if (gameController.GetCurrentPlayerState() == GameController::CurrentPlayerState::TradeWithPlayer) {
                    auto nextState = [&](uint8_t exchangePlayer) {
                        /*if (gameController.ExchangeWithBank(exchangeIndex)) {
                            gui.SetGameInfoFeed("Player: " + gameController.GetCurrentPlayer().GetName() + " on move");
                        }
                        else {
                            gui.SetGameInfoFeed("Not enough resources");
                        }*/
                        gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::PressingButtons);
                        gui.EraseWindow();
                    };
                    if (std::holds_alternative<std::string>(pressedButtonInfo)) {
                        auto playerIndex = static_cast<uint8_t>(std::get<std::string>(pressedButtonInfo).back() - '0');
                        if (std::get<std::string>(pressedButtonInfo) == "trade") {
                            gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::PressingButtons);
                            gui.EraseWindow();
                        } else if (playerIndex >= 0 && playerIndex < gameController.m_numberOfPlayers) {
                            auto tradeWithPlayerResources = gui.GetResourceQuantitiesFromTradeTextboxes();
                            auto& otherPlayer = gameController.GetSpecificPlayer(playerIndex);
                            ResourceArray givenResources {0, 0, 0, 0, 0};
                            ResourceArray wantedResources{0, 0, 0, 0, 0};
                            for (uint8_t index = 0; index < ResourcesTypeCount; ++index)
                            {
                                if (tradeWithPlayerResources[index] < 0)
                                    givenResources[index] = static_cast<uint8_t >(tradeWithPlayerResources[index] * -1);
                                else
                                    wantedResources[index] = static_cast<uint8_t >(tradeWithPlayerResources[index]);
                            }
                            if (gameController.GetCurrentPlayer().CanExpand(givenResources) && otherPlayer.CanExpand(wantedResources))
                            {
                                gameController.GetCurrentPlayer().ExchangeWithPlayer(givenResources, wantedResources, otherPlayer);
                                gui.SetGameInfoFeed("Player: " + gameController.GetCurrentPlayer().GetName() + " on move");
                            }
                            else
                                gui.SetGameInfoFeed("Not enough resources");

                            gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::PressingButtons);
                            gui.EraseWindow();
                        }
                    }
                }
                else if (gameController.GetCurrentPlayerState() == GameController::CurrentPlayerState::PickResourceForMonopoly) {
                    if (std::holds_alternative<std::string>(pressedButtonInfo)) {
                        ResourceType resourceToReceive = resourceTypeFromString(std::get<std::string>(pressedButtonInfo));
                        if (resourceToReceive != ResourceType::None) {
                            for (uint8_t index = 0; index < gameController.GetNumberOfPlayers(); ++index)
                            {
                                if (index != gameController.GetCurrentPlayerIndex())
                                {
                                    uint8_t available = gameController.GetSpecificPlayer(index).GetAvailableResources()[static_cast<int>(resourceToReceive)];
                                    if (available > 0) {
                                        gameController.GetCurrentPlayer().GiveResource(resourceToReceive, available);
                                        gameController.GetSpecificPlayer(index).Expand(resourceToReceive, available);
                                    }
                                }
                            }
                            gui.SetGameInfoFeed("Player: " + gameController.GetCurrentPlayer().GetName() + " on move");
                            gameController.SetCurrentPlayerState(GameController::CurrentPlayerState::PressingButtons);
                            gui.EraseWindow();
                        }
                    }
                }
                //	//event.mouseButton.button <- the pressed button
                //	// event.mouseButton.x <- x coord of mouse
                //	// event.mouseButton.y <- y coord of mouse
                gui.RefreshCards(gameController.GetCurrentPlayer().GetCardsCount());
                gui.RefreshIcons(gameController.GetCurrentPlayer().GetAvailableResources());
                break;
            }
            case sf::Event::MouseMoved:
                return;
		    case sf::Event::KeyPressed:
		        if (gameController.GetCurrentPlayerState() == GameController::CurrentPlayerState::TradeWithPlayer) {
                    if(event.key.code == sf::Keyboard::BackSpace)
                        gui.RemoveLetterFromActiveTextbox();
                    else if (event.key.code == sf::Keyboard::Hyphen)
                        gui.AddLetterToActiveTextbox('-');
                    else {
                        char x = event.key.code - 26; /// Unicode
                        if (x >= 0 && x <= 9)
                            gui.AddLetterToActiveTextbox('0' + x);
                    }
                }
		        break;
		}
	}
}

/* Not really the place to put this, but works for now */
ResourceType resourceTypeFromString(const std::string& str) {
    if (str == "brick")
        return ResourceType ::Brick;
    if (str == "grain")
        return ResourceType ::Grain;
    if (str == "lumber")
        return ResourceType ::Lumber;
    if (str == "ore")
        return ResourceType ::Ore;
    if (str == "wool")
        return ResourceType ::Wool;
    return ResourceType ::None;
}