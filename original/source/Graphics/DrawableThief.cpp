#include "Graphics/DrawableThief.h"

sf::Texture DrawableThief::m_thiefTexture;

DrawableThief::DrawableThief()
{
	if (m_thiefTexture.getSize().x == 0)
	{
		m_thiefTexture.loadFromFile("resources/sprites/thief.png");
		m_sprite.setTexture(m_thiefTexture);
	}
	else
		m_sprite.setTexture(m_thiefTexture);
}

