#include <memory>
#include "Graphics/DrawablePiece.h"
#include "GameObjects/DevelopmentCard.h"

sf::Texture DrawablePiece::m_pieceTexture;
std::vector<std::string> DrawablePiece::m_playerNames;

DrawablePiece::DrawablePiece(const Piece &piece) :
	m_piece(std::make_shared<Piece>(piece))
{
	if (m_pieceTexture.getSize().x == 0)
	{
		m_pieceTexture.loadFromFile("resources/sprites/pieces.png");
		m_sprite.setTexture(m_pieceTexture);
	}
	else
		m_sprite.setTexture(m_pieceTexture);
	int16_t tileOffset = 64;
	if (std::none_of(m_playerNames.begin(), m_playerNames.end(), [&](const std::string &name) { return name == m_piece->GetOwner().GetName(); }))
		m_playerNames.push_back(m_piece->GetOwner().GetName());
	for (uint8_t playerIndex = 0; playerIndex < m_playerNames.size(); ++playerIndex)
	{
		if (playerIndex == 0 && m_playerNames[playerIndex] == m_piece->GetOwner().GetName())
			m_sprite.setColor(sf::Color(255, 0, 0));
		else if(playerIndex == 1 && m_playerNames[playerIndex] == m_piece->GetOwner().GetName())
			m_sprite.setColor(sf::Color(0, 255, 0));
		else if(playerIndex == 2 && m_playerNames[playerIndex] == m_piece->GetOwner().GetName())
			m_sprite.setColor(sf::Color(0, 0, 255));
		else if(playerIndex == 3 && m_playerNames[playerIndex] == m_piece->GetOwner().GetName())
			m_sprite.setColor(sf::Color(255, 255, 0));
	}
	for (int pieceTypeIndex = 0; pieceTypeIndex < 4; ++pieceTypeIndex)
		if (pieceTypeIndex < 3 && pieceTypeIndex == static_cast<int16_t>(piece.GetType()))
		{
			m_sprite.setTextureRect({ tileOffset * pieceTypeIndex, 0, 64, 64 });
			break;
		}
}
