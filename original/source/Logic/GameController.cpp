#include <Logic/GameController.h>
#include <GameObjects/DevelopmentCard.h>

#include "Logic/GameController.h"

#include "Graphics/GUI.h"
#include "SFML/Graphics/Color.hpp"


GameController::GameController() :
	m_window("Catan"), m_currentPlayerState(CurrentPlayerState::InitialState), m_playingDice(6)
{
	m_winFlag = false;
	m_developmentCardPlayedThisTurn = false;
	m_diceRolledThisTurn = false;
	m_guiPointer = new GUI(m_window, *this);
}

GameController::~GameController()
{
	delete m_guiPointer;
}

Board & GameController::GetBoard()
{
	return m_gameBoard;
}

void GameController::InitialSetup()
{
	std::cout << "Enter number of players:";
	std::cin >> m_numberOfPlayers;
	m_numberOfPlayers -= '0'; /// uint8_t is read as ASCII
	if (m_numberOfPlayers <= 0 || m_numberOfPlayers > 4) {
		throw "Invalid number of players";
	}
	std::string name;
	for (int index = 0; index < m_numberOfPlayers; ++index) {
		std::cout << "Enter your name:";
		std::cin >> name;
		m_players.emplace_back(name);
	}
	SetPlayerOrder();
	m_guiPointer->SetGameInfoFeed("Player: " + GetCurrentPlayer().GetName() + " on move");

	m_guiPointer->UpdateWithPlayersNames();
}

void GameController::UpdateLongestRoad()
{
	auto longestRoad = m_gameBoard.GetLongestRoad();
	if (longestRoad.second > m_longestRoad.second) {
		m_longestRoad.first = std::make_shared<const Player>(longestRoad.first);
		m_longestRoad.second = longestRoad.second;
	}
}

/// Call just before going to next players turn
void GameController::UpdateLargestArmy()
{
	auto armySize = static_cast<uint8_t>(std::count_if(GetCurrentPlayer().GetPlayedCards().begin(), GetCurrentPlayer().GetPlayedCards().end(),
		[](auto card) {
			return card->GetAction() == DevelopmentCardAction::KnightCard;
		}));
	if (armySize > m_largestArmy.second) {
		m_largestArmy.first = std::make_shared<const Player>(GetCurrentPlayer());
		m_largestArmy.second = armySize;
	}
}
bool GameController::CheckIfCurrentPlayerWon()
{
	if (CountPlayerVP(GetCurrentPlayer()) == 10) {
		m_winFlag = true;
		return true;
	}
	return false;
}


void GameController::SetPlayerOrder()
{
	for (int index = 0; index < m_players.size(); ++index) {
		RollDie();
		std::cout << m_players[index].GetName() << " rolled a " << m_currentRolledValue << std::endl;
		m_playerOrder.emplace_back(index, m_currentRolledValue);
	}
	std::sort(m_playerOrder.begin(), m_playerOrder.end(), [](const std::pair<int, int> &left, const std::pair<int, int> &right) {
		return left.second > right.second;
		});
	m_currentPlayerIndex = 0;

}

int GameController::GetCurrentPlayerIndex() const
{
	return m_currentPlayerIndex;
}

Player & GameController::GetSpecificPlayer(uint8_t index)
{
	return m_players[m_playerOrder[index].first];
}

Player & GameController::GetCurrentPlayer()
{
	return m_players[m_playerOrder[m_currentPlayerIndex].first];
}

GameController::CurrentPlayerState GameController::GetCurrentPlayerState() const
{
	return m_currentPlayerState;
}


void GameController::SetCurrentPlayerState(const CurrentPlayerState & otherPlayerState)
{
	m_currentPlayerState = otherPlayerState;
}

void GameController::PrintCurrentPlayerOrder() const
{
	std::cout << "The order of the players is \n";
	for (int index = 0; index < m_players.size(); ++index) {
		std::cout << m_players[m_playerOrder[index].first].GetName() << std::endl;
	}
}

void GameController::Play()
{
	try {
		InitialSetup();
		PrintCurrentPlayerOrder();
        ReinitFieldsForTurn();
    }
	catch (const char * exception)
	{
		std::cout << "Invalid Number Of Players";
		Quit();
	}
	while (m_window.IsOpen()) {
		WindowEventHandler::MainWindowHandle(m_window, *this, *m_guiPointer);
		m_window.Clear(sf::Color::Black);
		m_guiPointer->Draw();
		m_window.Display();
		if (m_winFlag) {
			m_window.CloseWindow();
		}
	}
}




void GameController::NextTurn()
{
	UpdateLargestArmy();
	UpdateLongestRoad();
	if (CheckIfCurrentPlayerWon())
		return;

	if (AreAllPlayersSettled()) {
        SetCurrentPlayerState(GameController::CurrentPlayerState::PressingButtons);
    }

	if (m_currentPlayerIndex == m_numberOfPlayers - 1) {
		m_currentPlayerIndex = 0;
	}
	else {
		m_currentPlayerIndex++;
	}
    ReinitFieldsForTurn();
}

uint8_t GameController::RollDie()
{
	m_diceRolledThisTurn = true;
	m_currentRolledValue = m_playingDice.RollTwoDice();
	if (m_currentPlayerState != CurrentPlayerState::InitialState)
	    std::cout << GetCurrentPlayer().GetName() << " rolled " << static_cast<int>(m_currentRolledValue) << "\n";
	return m_currentRolledValue;
}

uint8_t GameController::GetNumberOfPlayers()
{
	return m_numberOfPlayers;
}

void GameController::Quit()
{
	m_window.CloseWindow();
}

uint8_t GameController::CountPlayerVP(const Player &player) const {
	uint8_t vp = m_gameBoard.CountBuildingScore(player);
	vp += std::count_if(player.GetPlayedCards().begin(), player.GetPlayedCards().end(),
		[](auto card) {
			return card->GetAction() == DevelopmentCardAction::VictoryPointCard;
		});

	if (m_longestRoad.first.get() == &player && m_longestRoad.second >= 5) {
		vp += 2;
	}
	if (m_largestArmy.first.get() == &player && m_largestArmy.second >= 3) {
		vp += 2;
	}
	return vp;
}

bool GameController::AreAllPlayersSettled() const {
	for (const Player& player : m_players)
		if (player.GetAvailable(PieceType::Road) > 13)
			return false;
    return true;
}

void GameController::ReinitFieldsForTurn() {
	m_developmentCardPlayedThisTurn = false;
	m_diceRolledThisTurn = false;
	builtRoad = false;
	builtSettlement = false;
    std::cout << "Now is " << GetCurrentPlayer().GetName() << "'s turn\n";
}

bool GameController::ExchangeWithBank(uint8_t exchangeResource) {
    if (m_gameBoard.CanPlayerExchangeWithBank(GetCurrentPlayer(), ResourceType (exchangeResource))) {
        m_gameBoard.PlayerExchangeWithBank(GetCurrentPlayer(), ResourceType(exchangeResource), ResourceType(wantedResource));
        return true;
    }
    return false;
}



