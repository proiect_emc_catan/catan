project(UnitTesting)

add_subdirectory(lib/gtest-1.7.0)

include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})

if (WIN32)
    #Paths
    set(INCROOT ${catan_SOURCE_DIR}/include)
    set(SRCROOT ${catan_SOURCE_DIR}/source)
    set(INCSFMLROOT ${catan_SOURCE_DIR}/third-party/SFML-Windows/include/SFML)
    set(SRC "${catan_SOURCE_DIR}")


    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${catan_SOURCE_DIR}/bin")
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${catan_SOURCE_DIR}/bin")
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${catan_SOURCE_DIR}/bin")

    #Files
    file(GLOB_RECURSE INCS
            "${INCROOT}/*.h"
            )
    file(GLOB_RECURSE INCSFML
            "${INCSFMLROOT}/*.h"
            )
    file(GLOB_RECURSE SRC_GameObjectsS
            "${SRC}/source/GameObjects/*.cpp"
            )
    file(GLOB_RECURSE SRC_LogicS
            "${SRC}/source/Logic/*.cpp"
            )
    file(GLOB_RECURSE SRC_GraphicsS
            "${SRC}/source/Graphics/*.cpp"
            )
    file(GLOB_RECURSE SRC_GameObjectsI
            "${SRC}/include/GameObjects/*.h"
            )
    file(GLOB_RECURSE SRC_LogicI
            "${SRC}/include/Logic/*.h"
            )
    file(GLOB_RECURSE SRC_GraphicsI
            "${SRC}/include/Graphics/*.h"
            )


    find_package(SFML 2 COMPONENTS audio graphics network system window REQUIRED)

    set(CMAKE_AUTOMOC ON)

    include_directories("${catan_SOURCE_DIR}/include")

    add_executable(RunTests
            GameObjectsTests.cpp
            ${INCS}
            ${SRCS}
            ${INCSFML}
            ${SRC_GameObjectsS}
            ${SRC_LogicS}
            ${SRC_GraphicsS}
            ${SRC_GameObjectsI}
            ${SRC_LogicI}
            ${SRC_GraphicsI})

    #Create groups
    source_group("Source files\\GameObjects" FILES ${SRC_GameObjectsS})
    source_group("Source files\\Logic" FILES ${SRC_LogicS})
    source_group("Source files\\Graphics" FILES ${SRC_GraphicsS})
    source_group("Include files\\GameObjects" FILES ${SRC_GameObjectsI})
    source_group("Include files\\Logic" FILES ${SRC_LogicI})
    source_group("Include files\\Graphics" FILES ${SRC_GraphicsI})

    #Copy necesary files
    set(openal32_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/openal32.dll)

    set ( sfml_audio_DEBUG_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/sfml-audio-d-2.dll)
    set ( sfml_graphics_DEBUG_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/sfml-graphics-d-2.dll)
    set ( sfml_network_DEBUG_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/sfml-network-d-2.dll)
    set ( sfml_system_DEBUG_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/sfml-system-d-2.dll)
    set ( sfml_window_DEBUG_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/sfml-window-d-2.dll)

    set ( sfml_audio_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/sfml-audio-2.dll)
    set ( sfml_graphics_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/sfml-graphics-2.dll)
    set ( sfml_network_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/sfml-network-2.dll)
    set ( sfml_system_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/sfml-system-2.dll)
    set ( sfml_window_DLL ${catan_SOURCE_DIR}/third-party/SFML-Windows/bin/openal32.dll)

    set (DEBUG_BIN ${catan_SOURCE_DIR}/bin/Debug)
    set (RELEASE_BIN ${catan_SOURCE_DIR}/bin/Release)

    #Copy necesary files to debug .exe
    file(COPY
            ${sfml_audio_DEBUG_DLL}
            DESTINATION ${DEBUG_BIN})
    file(COPY
            ${sfml_graphics_DEBUG_DLL}
            DESTINATION ${DEBUG_BIN})
    file(COPY
            ${sfml_network_DEBUG_DLL}
            DESTINATION ${DEBUG_BIN})
    file(COPY
            ${sfml_system_DEBUG_DLL}
            DESTINATION ${DEBUG_BIN})
    file(COPY
            ${sfml_window_DEBUG_DLL}
            DESTINATION ${DEBUG_BIN})

    #Copy necesary files to release .exe
    file(COPY
            ${sfml_audio_DLL}
            DESTINATION ${RELEASE_BIN})
    file(COPY
            ${sfml_graphics_DLL}
            DESTINATION ${RELEASE_BIN})
    file(COPY
            ${sfml_network_DLL}
            DESTINATION ${RELEASE_BIN})
    file(COPY
            ${sfml_system_DLL}
            DESTINATION ${RELEASE_BIN})
    file(COPY
            ${sfml_window_DLL}
            DESTINATION ${RELEASE_BIN})

endif (WIN32)

if(UNIX)

    #Paths
    set(INCROOT ${catan_SOURCE_DIR}/include)
    set(SRCROOT ${catan_SOURCE_DIR}/source)
    set(INCSFMLROOT ${catan_SOURCE_DIR}/third-party/SFML-Linux/include/SFML)
    set(SRC "${catan_SOURCE_DIR}")


    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${catan_SOURCE_DIR}/bin")
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${catan_SOURCE_DIR}/bin")
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${catan_SOURCE_DIR}/bin")

    #Files
    file(GLOB_RECURSE INCS
            "${INCROOT}/*.h"
            )
    file(GLOB_RECURSE INCSFML
            "${INCSFMLROOT}/*.h"
            )
    file(GLOB_RECURSE SRC_GameObjectsS
            "${SRC}/source/GameObjects/*.cpp"
            )
    file(GLOB_RECURSE SRC_LogicS
            "${SRC}/source/Logic/*.cpp"
            )
    file(GLOB_RECURSE SRC_GraphicsS
            "${SRC}/source/Graphics/*.cpp"
            )
    file(GLOB_RECURSE SRC_GameObjectsI
            "${SRC}/include/GameObjects/*.h"
            )
    file(GLOB_RECURSE SRC_LogicI
            "${SRC}/include/Logic/*.h"
            )
    file(GLOB_RECURSE SRC_GraphicsI
            "${SRC}/include/Graphics/*.h"
            )

    find_package(SFML 2 COMPONENTS audio graphics network system window REQUIRED)

    set(CMAKE_AUTOMOC ON)


    add_executable(RunTests
            GameObjectsTests.cpp
            ${INCS}
            ${SRCS}
            ${INCSFML}
            ${SRC_GameObjectsS}
            ${SRC_LogicS}
            ${SRC_GraphicsS}
            ${SRC_GameObjectsI}
            ${SRC_LogicI}
            ${SRC_GraphicsI})

    #Create groups
    source_group("Source files\\GameObjects" FILES ${SRC_GameObjectsS})
    source_group("Source files\\Logic" FILES ${SRC_LogicS})
    source_group("Source files\\Graphics" FILES ${SRC_GraphicsS})
    source_group("Include files\\GameObjects" FILES ${SRC_GameObjectsI})
    source_group("Include files\\Logic" FILES ${SRC_LogicI})
    source_group("Include files\\Graphics" FILES ${SRC_GraphicsI})

endif (UNIX)


include_directories(catan)
target_link_libraries(RunTests gtest gtest_main)
target_link_libraries(RunTests sfml-audio sfml-graphics sfml-network sfml-system sfml-window)


