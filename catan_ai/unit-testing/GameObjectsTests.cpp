#include <GameObjects/Player.h>
#include <GameObjects/Board.h>
#include <GameObjects/DevelopmentCard.h>

#include "gtest/gtest.h"
#include <algorithm>
#include <numeric>

class EmptyBoardTestFixture : public ::testing::Test {
protected:
    virtual void TearDown() {
    }

    virtual void SetUp() {

    }

public:
    EmptyBoardTestFixture() : Test() {

    }

    virtual ~EmptyBoardTestFixture() {

    }
    Board board;

};

class BasicBoardTestFixture : public ::testing::Test {
protected:
    virtual void TearDown() {
    }

    virtual void SetUp() {
        this->playerA = Player("A");
        /// Setting Player A
        {
            playerA.GiveResource(ResourceType::Brick, 5);
            playerA.GiveResource(ResourceType::Lumber, 5);
            playerA.GiveResource(ResourceType::Grain, 5);
            playerA.GiveResource(ResourceType::Ore, 5);
            playerA.GiveResource(ResourceType::Wool, 5);

            auto piece = playerA.BuyPiece(PieceType::Settlement);
            board.PlaceBuilding(1, 1, piece);
            piece = playerA.BuyPiece(PieceType::Road);
            board.PlaceRoad(1, 1, 1, 2, piece);

            piece = playerA.BuyPiece(PieceType::Settlement);
            board.PlaceBuilding(3, 3, piece);
            piece = playerA.BuyPiece(PieceType::Road);
            board.PlaceRoad(3, 3, 3, 4, piece);
        }

        this->playerB = Player("B");
        /// Setting Player B
        {
            playerB.GiveResource(ResourceType::Brick, 5);
            playerB.GiveResource(ResourceType::Lumber, 5);
            playerB.GiveResource(ResourceType::Grain, 5);
            playerB.GiveResource(ResourceType::Ore, 5);
            playerB.GiveResource(ResourceType::Wool, 5);

            auto piece = playerB.BuyPiece(PieceType::Settlement);
            board.PlaceBuilding(4, 0, piece);
            piece = playerB.BuyPiece(PieceType::Road);
            board.PlaceRoad(4, 0, 4, 1, piece);

            piece = playerB.BuyPiece(PieceType::Settlement);
            board.PlaceBuilding(5, 0, piece);
            piece = playerB.BuyPiece(PieceType::Road);
            board.PlaceRoad(4, 1, 5, 0, piece);

            piece = playerB.BuyPiece(PieceType::Road);
            board.PlaceRoad(5, 0, 5, 1, piece);

            piece = playerB.BuyPiece(PieceType::Road);
            board.PlaceRoad(5, 1, 5, 2, piece);

            piece = playerB.BuyPiece(PieceType::Road);
            board.PlaceRoad(5, 2, 5, 3, piece);
        }

        this->playerC = Player("C");
        /// Setting Player C
        {
            playerC.GiveResource(ResourceType::Brick, 5);
            playerC.GiveResource(ResourceType::Lumber, 5);
            playerC.GiveResource(ResourceType::Grain, 5);
            playerC.GiveResource(ResourceType::Ore, 5);
            playerC.GiveResource(ResourceType::Wool, 5);

            auto piece = playerC.BuyPiece(PieceType::Settlement);
            board.PlaceBuilding(3, 8, piece);
            piece = playerC.BuyPiece(PieceType::Road);
            board.PlaceRoad(3, 8, 3, 9, piece);

            piece = playerC.BuyPiece(PieceType::Settlement);
            board.PlaceBuilding(2, 6, piece);
            piece = playerC.BuyPiece(PieceType::Road);
            board.PlaceRoad(2, 6, 2, 7, piece);

            piece = playerC.BuyPiece(PieceType::Road);
            board.PlaceRoad(2, 7, 1, 6, piece);

            piece = playerC.BuyPiece(PieceType::Settlement);
            board.PlaceBuilding(1, 6, piece);

            piece = playerC.BuyPiece(PieceType::City);
            board.PlaceBuilding(1, 6, piece);
        }
    }

public:
    BasicBoardTestFixture() : Test() {

    }

    virtual ~BasicBoardTestFixture() {

    }
    Board board;
    Player playerA, playerB, playerC;

};

TEST_F(EmptyBoardTestFixture, ObviousCheck) {
    EXPECT_EQ(1, 1);
}

TEST_F(EmptyBoardTestFixture, CheckNewPlayerAttributes) {
    Player player("John");
    EXPECT_EQ(player.GetName(), "John");
    EXPECT_EQ(player.GetAvailable(PieceType::Settlement), 5);
    EXPECT_EQ(player.GetAvailable(PieceType::City), 4);
    EXPECT_EQ(player.GetAvailable(PieceType::Road), 15);
}

TEST_F(EmptyBoardTestFixture, TestBoardGeneration) {
    uint8_t tileCount[13];
    memset(tileCount, 0, sizeof(tileCount));
    for (uint8_t row = 0; row < board.GetNumberMap().size(); ++row)
        for (uint8_t col = 0; col < board.GetNumberMap()[row].size(); ++col)
            tileCount[board.GetNumberAtTilePosition(row, col)]++;
    EXPECT_EQ(tileCount[2], 1);
    EXPECT_EQ(tileCount[12], 1);
    EXPECT_EQ(tileCount[0], 1);
    for (uint8_t i = 3; i < 12; i++)
        if (i != 7)
            EXPECT_EQ(tileCount[i], 2);

    for (uint8_t row = 0; row < board.GetNumberMap().size(); ++row)
        for (uint8_t col = 0; col < board.GetNumberMap()[row].size(); ++col)
            if (board.GetNumberAtTilePosition(row, col) == 0 &&
                    (board.GetTileAtPosition(row, col) != Board::TileType::Desert ||
                     board.GetThiefLocation().first != row ||
                     board.GetThiefLocation().second != col)) {
                FAIL();
            }
}

TEST_F(EmptyBoardTestFixture, CheckBasicBehaviour) {
    Player player("John");

    /// First settlement
    EXPECT_EQ(player.CanPickPiece(PieceType::Settlement), true);

    auto piece = player.BuyPiece(PieceType::Settlement);

    EXPECT_EQ(board.CanPlaceBuilding(0, 0, PieceType::Settlement, player), true);

    board.PlaceBuilding(0, 0, piece);

    const auto& lambda = [](uint8_t res){ return res == 0; };
    EXPECT_EQ(std::all_of(player.GetAvailableResources().begin(), player.GetAvailableResources().end(), lambda), true);
}


TEST_F(EmptyBoardTestFixture, CheckSecondSettlementResources) {
    Player player("John");

    auto expectedResources = std::array<uint8_t , 5>{0, 0, 0, 0, 0};
    if (board.GetTileAtPosition(0, 0) != Board::TileType::Desert)
        expectedResources[static_cast<uint8_t >(board.GetTileAtPosition(0, 0))]++;
    if (board.GetTileAtPosition(0, 1) != Board::TileType::Desert)
        expectedResources[static_cast<uint8_t >(board.GetTileAtPosition(0, 1))]++;
    if (board.GetTileAtPosition(1, 1) != Board::TileType::Desert)
        expectedResources[static_cast<uint8_t >(board.GetTileAtPosition(1, 1))]++;

    /// First settlement
    auto piece = player.BuyPiece(PieceType::Settlement);
    board.PlaceBuilding(4, 2, piece);

    EXPECT_EQ(player.CanPickPiece(PieceType::Settlement), false); /// Has to build a road first
    piece = player.BuyPiece(PieceType::Road);
    board.PlaceRoad(4, 2, 4, 3, piece);

    /// Second settlement
    piece = player.BuyPiece(PieceType::Settlement);
    board.PlaceBuilding(1, 3, piece);

    EXPECT_EQ(player.GetAvailableResources(), expectedResources);
}

TEST_F(BasicBoardTestFixture, CheckInvalidSettlementPosition) {
    EXPECT_EQ(board.CanPlaceBuilding(5, 2, PieceType::Settlement, playerB), true); /// Valid placement

    EXPECT_EQ(board.CanPlaceBuilding(1, 6, PieceType::Settlement, playerA), false); /// Already taken
    EXPECT_EQ(board.CanPlaceBuilding(3, 1, PieceType::Settlement, playerA), false); /// To close to B's building
    EXPECT_EQ(board.CanPlaceBuilding(1, 2, PieceType::Settlement, playerA), false); /// To close to own building
    EXPECT_EQ(board.CanPlaceBuilding(1, 3, PieceType::Settlement, playerA), false); /// No road connection
}

TEST_F(BasicBoardTestFixture, CheckInvalidCityPosition) {
    EXPECT_EQ(board.CanPlaceBuilding(4, 0, PieceType::City, playerB), true); /// Valid placement

    EXPECT_EQ(board.CanPlaceBuilding(4, 0, PieceType::City, playerA), false); /// Someone else's Settlement
    EXPECT_EQ(board.CanPlaceBuilding(1, 6, PieceType::City, playerA), false); /// Someone else's building
    EXPECT_EQ(board.CanPlaceBuilding(1, 3, PieceType::City, playerA), false); /// No building here
}

TEST_F(BasicBoardTestFixture, CheckInvalidRoadPosition) {
    EXPECT_EQ(board.CanPlaceRoad(5, 3, 5, 4, PieceType::Road, playerB), true); /// Valid placement

    EXPECT_EQ(board.CanPlaceRoad(1, 1, 1, 2, PieceType::Road, playerA), false); /// Road taken
    EXPECT_EQ(board.CanPlaceRoad(0, 1, 0, 2, PieceType::Road, playerA), false); /// Road not connected with none
    EXPECT_EQ(board.CanPlaceRoad(5, 3, 5, 4, PieceType::Road, playerA), false); /// Road not connected with self
}

TEST_F(BasicBoardTestFixture, CheckResourceTrigger) {
    auto resourceIndex = static_cast<uint8_t >(board.GetTileAtPosition(3, 0));
    auto resourcesPlayerA = ResourceArray(playerA.GetAvailableResources());
    auto resourcesPlayerB = ResourceArray(playerB.GetAvailableResources());
    auto resourcesPlayerC = ResourceArray(playerC.GetAvailableResources());

    board.TriggerResourceGathering(3, 0);

    if (resourceIndex < 5) {
        resourcesPlayerA[resourceIndex]++;
        resourcesPlayerB[resourceIndex]++;
    }
    EXPECT_EQ(playerA.GetAvailableResources(), resourcesPlayerA);
    EXPECT_EQ(playerB.GetAvailableResources(), resourcesPlayerB);
    EXPECT_EQ(playerC.GetAvailableResources(), resourcesPlayerC);
}

TEST_F(BasicBoardTestFixture, CheckResourceTriggerWithCity) {
    auto resourceIndex = static_cast<uint8_t >(board.GetTileAtPosition(1, 2));
    auto resourcesPlayerA = ResourceArray(playerA.GetAvailableResources());
    auto resourcesPlayerB = ResourceArray(playerB.GetAvailableResources());
    auto resourcesPlayerC = ResourceArray(playerC.GetAvailableResources());
    board.TriggerResourceGathering(1, 2);

    if (resourceIndex < 5) {
        resourcesPlayerC[resourceIndex]+=3; /// Player C has 1 settlement and a city near this tile
    }

    EXPECT_EQ(playerA.GetAvailableResources(), resourcesPlayerA);
    EXPECT_EQ(playerB.GetAvailableResources(), resourcesPlayerB);
    EXPECT_EQ(playerC.GetAvailableResources(), resourcesPlayerC);
}


TEST_F(BasicBoardTestFixture, CheckMoveThief) {
    board.MoveThief(playerA, 3, 2);
    EXPECT_EQ(board.GetThiefLocation(), (std::pair{static_cast<int >(3), static_cast<int >(2)}));
}

TEST_F(BasicBoardTestFixture, CheckResourceTriggerWithThief) {
    board.MoveThief(playerA, 3, 0);
    /// Resources after stealing from the players
    auto resourcesPlayerA = std::array(playerA.GetAvailableResources());
    auto resourcesPlayerB = std::array(playerB.GetAvailableResources());
    auto resourcesPlayerC = std::array(playerC.GetAvailableResources());

    board.TriggerResourceGathering(3, 0);
    /// Check Resource count is unchanged
    EXPECT_EQ(playerA.GetAvailableResources(), resourcesPlayerA);
    EXPECT_EQ(playerB.GetAvailableResources(), resourcesPlayerB);
    EXPECT_EQ(playerC.GetAvailableResources(), resourcesPlayerC);
}


TEST_F(BasicBoardTestFixture, CheckPlayerStealFromPlayer) {
    auto resourceCountA = static_cast<uint8_t>(std::accumulate(playerA.GetAvailableResources().begin(), playerA.GetAvailableResources().end(), 0));
    auto resourceCountB = static_cast<uint8_t>(std::accumulate(playerB.GetAvailableResources().begin(), playerB.GetAvailableResources().end(), 0));
    board.MoveThief(playerA, 4, 0); /// Player A steals one resource from player B

    auto resourceCountAAfter = static_cast<uint8_t>(std::accumulate(playerA.GetAvailableResources().begin(), playerA.GetAvailableResources().end(), 0));
    auto resourceCountBAfter = static_cast<uint8_t>(std::accumulate(playerB.GetAvailableResources().begin(), playerB.GetAvailableResources().end(), 0));

    EXPECT_EQ(resourceCountAAfter, resourceCountA + 1);
    EXPECT_EQ(resourceCountBAfter, resourceCountB - 1);
}

TEST_F(BasicBoardTestFixture, CheckPlayerLoseHalf) {
    auto resourceCountA = static_cast<uint8_t>(std::accumulate(playerA.GetAvailableResources().begin(), playerA.GetAvailableResources().end(), 0));
    auto didDiscard = playerA.DiscardHalfResourcesIfNecessary();
    auto resourceCountAAfter = static_cast<uint8_t>(std::accumulate(playerA.GetAvailableResources().begin(), playerA.GetAvailableResources().end(), 0));

    EXPECT_EQ(didDiscard, true);
    EXPECT_EQ(resourceCountAAfter, (resourceCountA+1) / 2);

}

TEST_F(BasicBoardTestFixture, CheckPlayerLoseHalfSafe) {
    auto resourceCountA = static_cast<uint8_t>(std::accumulate(playerA.GetAvailableResources().begin(), playerA.GetAvailableResources().end(), 0));
    for (uint8_t i = 0; i < resourceCountA-7; i++)
        playerA.LoseRandomResource();
    auto didDiscard = playerA.DiscardHalfResourcesIfNecessary();
    auto resourceCountAAfter = static_cast<uint8_t>(std::accumulate(playerA.GetAvailableResources().begin(), playerA.GetAvailableResources().end(), 0));

    EXPECT_EQ(didDiscard, false);
    EXPECT_EQ(resourceCountAAfter, 7);
}

TEST_F(BasicBoardTestFixture, CheckPlayerCanExchangeWithBank) {
    auto canExchange = board.CanPlayerExchangeWithBank(playerC,
                                                       ResourceType::Wool);
    EXPECT_EQ(canExchange, true);
    while (playerC.GetAvailableResources()[static_cast<uint8_t >(ResourceType::Lumber)] >= 4)
        playerC.LoseRandomResource();
    canExchange = board.CanPlayerExchangeWithBank(playerC,
                                                  ResourceType::Lumber);
    EXPECT_EQ(canExchange, false);
}

TEST_F(BasicBoardTestFixture, CheckPlayerCanExchangeWithBankOnHarbor) {
    auto canExchange = board.CanPlayerExchangeWithBank(playerB,
                                                       ResourceType::Brick);
    /// Has 2:1 Harbor on Brick
    EXPECT_EQ(canExchange, true);

    while (playerB.GetAvailableResources()[static_cast<uint8_t >(ResourceType::Lumber)] >= 3)
        playerB.Expand(ResourceArray{0, 0, 1, 0, 0});

    canExchange = board.CanPlayerExchangeWithBank(playerB,
                                                  ResourceType::Lumber);

    /// Not enough Lumber
    EXPECT_EQ(canExchange, false);

    playerB.Expand(std::array<uint8_t , 5>{0, 1, 0, 0, 0});
    canExchange = board.CanPlayerExchangeWithBank(playerB,
                                                  ResourceType::Grain);
    /// Has 3:1 Harbor
    EXPECT_EQ(canExchange, true);
}

TEST_F(BasicBoardTestFixture, CheckPlayerVPCalculation) {
    EXPECT_EQ(board.CountBuildingScore(playerA), 2);
    EXPECT_EQ(board.CountBuildingScore(playerB), 2); /// Largest Road doesn't count here
    EXPECT_EQ(board.CountBuildingScore(playerC), 4);
}

TEST_F(BasicBoardTestFixture, CheckPlayerKnightCardPlay) {
    playerA.GiveResource(ResourceType::Grain, 50);
    playerA.GiveResource(ResourceType::Wool, 50);
    playerA.GiveResource(ResourceType::Ore, 50);
    auto piece = std::dynamic_pointer_cast<DevelopmentCard>(playerA.BuyPiece(PieceType::DevelopmentCard));
    while (piece->GetAction() != DevelopmentCardAction::KnightCard)
        piece = std::dynamic_pointer_cast<DevelopmentCard>(playerA.BuyPiece(PieceType::DevelopmentCard));

    EXPECT_EQ(playerA.HasCard(DevelopmentCardAction::KnightCard), true);
    board.PlayerPlayCard(playerA, piece->GetAction());

    EXPECT_EQ(playerA.HasCard(DevelopmentCardAction::KnightCard), false);
    bool wasPlayed = std::any_of(playerA.GetPlayedCards().begin(), playerA.GetPlayedCards().end(),
            [](auto card){ return card->GetAction() == DevelopmentCardAction::KnightCard; });
    EXPECT_EQ(wasPlayed, true);
}

TEST_F(BasicBoardTestFixture, CheckPlayerYearOfPlentyPlay) {
    playerA.GiveResource(ResourceType::Grain, 50);
    playerA.GiveResource(ResourceType::Wool, 50);
    playerA.GiveResource(ResourceType::Ore, 50);
    auto piece = std::dynamic_pointer_cast<DevelopmentCard>(playerA.BuyPiece(PieceType::DevelopmentCard));
    while (piece->GetAction() != DevelopmentCardAction::YearOfPlenty)
        piece = std::dynamic_pointer_cast<DevelopmentCard>(playerA.BuyPiece(PieceType::DevelopmentCard));

    auto resourceCountA = static_cast<uint8_t>(std::accumulate(playerA.GetAvailableResources().begin(), playerA.GetAvailableResources().end(), 0));
    board.PlayerPlayCard(playerA, DevelopmentCardAction::YearOfPlenty);
    auto resourceCountAAfter = static_cast<uint8_t>(std::accumulate(playerA.GetAvailableResources().begin(), playerA.GetAvailableResources().end(), 0));
    EXPECT_EQ(resourceCountAAfter, resourceCountA+2);
}

TEST_F(BasicBoardTestFixture, CheckPlayerBuild2RoadsPlay) {
    playerA.GiveResource(ResourceType::Grain, 50);
    playerA.GiveResource(ResourceType::Wool, 50);
    playerA.GiveResource(ResourceType::Ore, 50);
    auto piece = std::dynamic_pointer_cast<DevelopmentCard>(playerA.BuyPiece(PieceType::DevelopmentCard));
    while (piece->GetAction() != DevelopmentCardAction::RoadBuilding)
        piece = std::dynamic_pointer_cast<DevelopmentCard>(playerA.BuyPiece(PieceType::DevelopmentCard));

    playerA.Expand(playerA.GetAvailableResources());
    EXPECT_EQ(playerA.GetAvailableResources(), (ResourceArray{0, 0, 0, 0, 0}));
    EXPECT_EQ(playerA.CanPickPiece(PieceType::Road), false);

    board.PlayerPlayCard(playerA, DevelopmentCardAction::RoadBuilding);

    EXPECT_EQ(playerA.CanPickPiece(PieceType::Road), true);
    playerA.BuyPiece(PieceType::Road);

    EXPECT_EQ(playerA.CanPickPiece(PieceType::Road), true);
    playerA.BuyPiece(PieceType::Road);

    EXPECT_EQ(playerA.CanPickPiece(PieceType::Road), false);
}

TEST_F(BasicBoardTestFixture, CheckBoardLongestRoad) {
    auto val = board.GetLongestRoad();
    EXPECT_EQ(&val.first, &playerB);
    EXPECT_EQ(val.second, 5);
}

int main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}