#include <iostream>
#include <Logic/AIAgent.h>
#include <Logic/PlayerGameAction.h>
#include "Logic/GameController.h"

int main(int argc, char *argv[])
{
	/*PlayerGameActionType actionType = PlayerGameActionType ::BUILD_SETTLEMENT;
	Player* player = new Player("John");
	std::vector<Player> players({*player});
	AIAgent agent(players);
	GameState state(Board{}, *player);


	for (int epi = 0; epi < 1000; epi++) {
        float val = agent.HeuristicStateEvaluation(state, *player);
        auto realVP = static_cast<int>(agent.EvaluateExpectedVP(state, *player));
        printf("Step %d -> Current evaluation %f | RealVP %d | Resources (", epi, val, realVP);
        for (uint8_t res = 0; res < 5; res++)
            std::cout << int(player->GetAvailableResources()[res]) << " ";
        std::cout << ")\n";
        const auto bestAction = agent.FindBestAction(state, *player, 3);
        printf("Best action is %d , final yielding %f\n", bestAction.first.GetAction(), bestAction.second);
        if (bestAction.first.GetAction() == PlayerGameActionType::END_TURN)
            state = agent.ExecuteAction(state, PlayerGameAction(PlayerGameActionType::DICE_ROLL, *player, Dice(6).RollTwoDice()));
        else
            state = agent.ExecuteAction(state, bestAction.first);

        if (realVP >= 10) {
            std::cout << "Finished the game atfer " << epi << " moves\n";
            break;
        }
    }*/


    /*SimpleGameStateHasher hasher;

    std::cout << "Hash1: " << hasher(state) << "\n";
	GameState state1 = agent.ExecuteAction(state, PlayerGameAction(PlayerGameActionType::BUILD_SETTLEMENT, *player, {3, 5}));
	std::cout << "Hash2: " << hasher(state1) << "\n";
	player->GiveResource(ResourceType::Brick, 2);
	GameState state2(state1.GetBoard(), avoid);
    std::cout << "Hash3: " << hasher(state2) << "\n";

	float val = agent.HeuristicStateEvaluation(state, *player);
	std::cout << "Before settlement: " << val << "\n";
	val = agent.HeuristicStateEvaluation(state1, *player);
	std::cout << "After settlement: " << val << "\n";*/

	GameController Catan;
	Catan.Play();

	return 0;
}