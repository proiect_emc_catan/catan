#include "Graphics/DrawableTile.h"

sf::Texture DrawableTile::m_tileTexture;

DrawableTile::DrawableTile()
{
	if (m_tileTexture.getSize().x == 0)
	{
		m_tileTexture.loadFromFile("resources/sprites/resource_tiles.png");
		m_sprite.setTexture(m_tileTexture);
	}
	else
		m_sprite.setTexture(m_tileTexture);
}

void DrawableTile::LoadNewTile(const Board::TileType &type)
{
	int16_t tileWidth = 200, tileHeight = 200;
	for (uint16_t offsetMultiplier = 0; offsetMultiplier < 6; ++offsetMultiplier)
		if (offsetMultiplier == static_cast<uint16_t>(type))
		{
			m_sprite.setTextureRect({ tileWidth * offsetMultiplier, 0, tileWidth, tileHeight });
			m_sprite.setScale({ 0.775f, 0.775f });
			break;
		}
}
