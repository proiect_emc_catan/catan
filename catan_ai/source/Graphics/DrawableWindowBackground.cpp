#include "Graphics/DrawableWindowBackground.h"

sf::Texture DrawableWindowBackground::m_windowBackgroundTexture;

DrawableWindowBackground::DrawableWindowBackground()
{
	if (m_windowBackgroundTexture.getSize().x == 0)
	{
		m_windowBackgroundTexture.loadFromFile("resources/sprites/window_background.png");
		m_sprite.setTexture(m_windowBackgroundTexture);
	}
	else
		m_sprite.setTexture(m_windowBackgroundTexture);
}

void DrawableWindowBackground::SetWindowSize(const sf::Vector2u &windowSize)
{
	float xPercent = static_cast<float>(windowSize.x) / m_windowBackgroundTexture.getSize().x, 
		  yPercent = static_cast<float>(windowSize.y) / m_windowBackgroundTexture.getSize().y;
	m_sprite.setScale({ xPercent, yPercent });
}
