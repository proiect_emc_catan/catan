#include <SFML/Graphics/Texture.hpp>

#include "Graphics/DrawablePieceHighlight.h"

sf::Texture DrawablePieceHighlight::m_pieceTexture;

DrawablePieceHighlight::DrawablePieceHighlight(const PieceType &pieceType)
{
	if (m_pieceTexture.getSize().x == 0)
	{
		m_pieceTexture.loadFromFile("resources/sprites/pieces.png");
		m_sprite.setTexture(m_pieceTexture);
	}
	else
		m_sprite.setTexture(m_pieceTexture);
	int16_t tileOffset = 64;
	for (int pieceTypeIndex = 0; pieceTypeIndex < 4; ++pieceTypeIndex)
		if (pieceTypeIndex < 3 && pieceTypeIndex == static_cast<int16_t>(pieceType))
		{
			m_sprite.setTextureRect({ tileOffset * pieceTypeIndex, 0, 64, 64 });
			break;
		}
	m_sprite.setColor(sf::Color(255, 255, 255));
}

void DrawablePieceHighlight::SetType(const PieceType &pieceType)
{
	int16_t tileOffset = 64;
	for (int pieceTypeIndex = 0; pieceTypeIndex < 4; ++pieceTypeIndex)
		if (pieceTypeIndex < 3 && pieceTypeIndex == static_cast<int16_t>(pieceType))
		{
			m_sprite.setTextureRect({ tileOffset * pieceTypeIndex, 0, 64, 64 });
			break;
		}
	m_sprite.setColor(sf::Color(255, 255, 255));
}
