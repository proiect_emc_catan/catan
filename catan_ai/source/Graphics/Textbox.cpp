#include "Graphics/Textbox.h"

sf::Texture Textbox::m_textboxTexture;

Textbox::Textbox(const std::string &textboxString) :
	m_active(false),
	m_textboxString(textboxString)
{
	if (m_textboxTexture.getSize().x == 0)
	{
		m_textboxTexture.loadFromFile("resources/sprites/textbox.png");
		m_sprite.setTexture(m_textboxTexture);
	}
	else
		m_sprite.setTexture(m_textboxTexture);
	m_stringOnScreen.setFont(m_font);
	m_stringOnScreen.setCharacterSize(24);
	m_stringOnScreen.setFillColor(sf::Color::Black);
	m_stringOnScreen.setString(m_textboxString);
}

void Textbox::SetString(const std::string &textboxString)
{
	m_textboxString = textboxString;
	m_stringOnScreen.setString(m_textboxString);
}

void Textbox::AddString(const std::string &addMe)
{
	if (m_active == true)
	{
		m_textboxString += addMe;
		m_stringOnScreen.setString(m_textboxString);
	}
}

void Textbox::AddLetter(const char &addMe)
{
	if (m_active == true)
	{
		m_textboxString += std::string(1, addMe);
		m_stringOnScreen.setString(m_textboxString);
	}
}

void Textbox::RemoveLetter()
{
	if (m_active == true)
	{
		m_textboxString.erase(m_textboxString.size() - 1, 1);
		m_stringOnScreen.setString(m_textboxString);
	}
}

void Textbox::Draw(sf::RenderWindow &drawHere)
{
	DrawableObject::Draw(drawHere);
	sf::Transform transform = this->m_sprite.getTransform();
	transform.translate({ 6, 6 });
	drawHere.draw(m_stringOnScreen, transform);
}

bool Textbox::IsClicked(const sf::Vector2i &mousePosition)
{
	if (DrawableObject::IsClicked(mousePosition) == true)
	{
		m_active = true;
		return true;
	}
	return false;
}

const std::string& Textbox::GetString() const
{
	return m_textboxString;
}

const std::string& Textbox::Deactivate()
{
	m_active = false;
	return m_textboxString;
}
