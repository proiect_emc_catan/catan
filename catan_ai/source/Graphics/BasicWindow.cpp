#include "Graphics/BasicWindow.h"

BasicWindow::BasicWindow(const std::string &windowTitle) :
	m_window(sf::VideoMode(1024, 768), windowTitle)
{}

void BasicWindow::DrawObject(DrawableObject *object)
{
	object->Draw(m_window);
}

void BasicWindow::DrawLayer(const DrawableLayer &layer)
{
	for (auto objectPtr : layer.GetObjects())
		DrawObject(objectPtr);
}

void BasicWindow::Clear(const sf::Color &color)
{
	m_window.clear(color);
}

void BasicWindow::Display()
{
	m_window.display();
}

const sf::Vector2u BasicWindow::GetSize() const
{
	return m_window.getSize();
}

bool BasicWindow::GetEvent(sf::Event &event)
{
	return m_window.pollEvent(event);
}

bool BasicWindow::IsOpen() const
{
	return m_window.isOpen();
}

void BasicWindow::CloseWindow()
{
	m_window.close();
}
