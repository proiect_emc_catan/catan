#include <random>
#include <functional>

#include "Graphics/GUI.h"

GUI::GUI(BasicWindow &window, GameController &controller) :
	m_whereToDrawPointer(&window),
	m_controllerPointer(&controller),
	m_activeTextbox(nullptr)
{
	m_piecesOnBoard.reserve(126);
	AddLayer("0 Background");
	AddLayer("1 Tiles");
	AddLayer("2 Pieces");
	AddLayer("3 Buttons");
	AddLayer("4 Buildings");
	AddLayer("5 Window");
	AddLayer("6 Hints");
	m_layers["3 Buttons"].AddObjectInLayer(m_feedText);
	m_feedText.SetPosition({ m_whereToDrawPointer->GetSize().x * 0.03f , m_whereToDrawPointer->GetSize().y * 0.03f });
	InitializeBackground();
	InitializeTileMap();
	InitializeResourceIcons();
	InitializeButtons();
	InitializeTradeWindow();
	InitializeTradeWithBankWindow();
	InitializeTradeWithPlayerWindow();
}

void GUI::CheckIfAButtonIsPressed(const sf::Vector2i &mousePosition)
{
	m_hintButton.IsClicked(mousePosition);
	m_rollButton.IsClicked(mousePosition);
	m_buyCardButton.IsClicked(mousePosition);
	m_tradeButton.IsClicked(mousePosition);
	m_exitButton.IsClicked(mousePosition);
	m_endTurnButton.IsClicked(mousePosition);
	m_knightButton.IsClicked(mousePosition);
	m_buildRoadsButton.IsClicked(mousePosition);
	m_yearOfPlentyButton.IsClicked(mousePosition);
	m_monopolyButton.IsClicked(mousePosition);

    if (m_layers["5 Window"].IsObjectInLayer(m_playerOrBankWindowBankButton)) {
        m_playerOrBankWindowBankButton.IsClicked(mousePosition);
        m_playerOrBankWindowPlayerButton.IsClicked(mousePosition);
    }
    if (m_layers["5 Window"].IsObjectInLayer(m_tradeWithBankWindowBackground)) {
        for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
            m_tradeWithBankWindowResourceButtons[resourceIndex].IsClicked(mousePosition);
    }
	if (m_layers["5 Window"].IsObjectInLayer(m_tradeWithPlayerWindowBackground)) {
        for (uint16_t playerIndex = 0; playerIndex < m_controllerPointer->GetNumberOfPlayers(); ++playerIndex)
            if (m_controllerPointer->GetCurrentPlayerIndex() != playerIndex)
                m_tradeWithPlayerWindowPlayerButtons[playerIndex].IsClicked(mousePosition);
		ActivateClickedTextbox(mousePosition);
	}

}

std::variant<std::string, DevelopmentCardAction, int> GUI::GetPressedButtonInfo()
{
	m_layers["6 Hints"].ClearLayer();

	const std::string *stringReturn;

	if (m_playerOrBankWindowBankButton.IsHighlighted() == false)
	{
		if (m_tradeWithBankWindowResourceButtons[static_cast<uint8_t>(std::get<1>(m_resourceHighlight))].IsHighlighted() == true)
			for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
				if (static_cast<ResourceType>(resourceIndex) == std::get<0>(m_resourceHighlight))
					m_tradeWithBankWindowResourceButtons[resourceIndex].Dehighlight();
		if(m_tradeWithBankWindowResourceButtons[static_cast<uint8_t>(std::get<0>(m_resourceHighlight))].IsHighlighted() == true)
			for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
				if (static_cast<ResourceType>(resourceIndex) == std::get<0>(m_resourceHighlight)) 
				{
					m_tradeWithBankWindowResourceButtons[resourceIndex].Dehighlight();
					m_tradeWithBankWindowResourceButtons[static_cast<uint8_t>(std::get<1>(m_resourceHighlight))].Highlight();
				}
	}
	if (m_tradeButton.IsHighlighted() == false)
		m_playerOrBankWindowBankButton.Dehighlight();
	m_tradeButton.Dehighlight();

	m_exitButton.Dehighlight();
	m_endTurnButton.Dehighlight();
	m_knightButton.Dehighlight();
	m_buildRoadsButton.Dehighlight();
	m_yearOfPlentyButton.Dehighlight();
	m_rollButton.Dehighlight();
	m_buyCardButton.Dehighlight();

	stringReturn = m_hintButton.GetInfo();
	if (stringReturn != nullptr) 
		return *stringReturn;
	stringReturn = m_tradeButton.GetInfo();
	if (stringReturn != nullptr)
		return *stringReturn;
	stringReturn = m_exitButton.GetInfo();
	if (stringReturn != nullptr)
		return *stringReturn;
	stringReturn = m_endTurnButton.GetInfo();
	if (stringReturn != nullptr)
		return *stringReturn;

    stringReturn = m_playerOrBankWindowBankButton.GetInfo();
    if (stringReturn != nullptr)
		return *stringReturn;
    stringReturn = m_playerOrBankWindowPlayerButton.GetInfo();
    if (stringReturn != nullptr)
		return *stringReturn;

	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
	{
		stringReturn = m_tradeWithBankWindowResourceButtons[resourceIndex].GetInfo();
		if (stringReturn != nullptr)
		{
			return *stringReturn;
		}
	}
	for (uint8_t playerIndex = 0; playerIndex < m_controllerPointer->GetNumberOfPlayers(); ++playerIndex)
	{
		stringReturn = m_tradeWithPlayerWindowPlayerButtons[playerIndex].GetInfo();
		if (stringReturn != nullptr)
			return *stringReturn;
	}

	const DevelopmentCardAction *cardReturn;
	cardReturn = m_knightButton.GetInfo();
	if (cardReturn != nullptr)
		return *cardReturn;
	cardReturn = m_buildRoadsButton.GetInfo();
	if (cardReturn != nullptr)
		return *cardReturn;
	cardReturn = m_yearOfPlentyButton.GetInfo();
	if (cardReturn != nullptr)
		return *cardReturn;
	cardReturn = m_monopolyButton.GetInfo();
	if (cardReturn != nullptr)
		return *cardReturn;

	stringReturn = m_rollButton.GetInfo();
	if (stringReturn != nullptr)
		return *stringReturn;
	stringReturn = m_buyCardButton.GetInfo();
	if (stringReturn != nullptr)
		return *stringReturn;
	return 0;
}

std::tuple<uint16_t, uint16_t> GUI::GetCornerCoordinates(const sf::Vector2i &mousePosition) const
{
	auto windowSize = m_whereToDrawPointer->GetSize();
	float yOffsetFromTop = 0.08f * windowSize.y, xOffsetFromLeftEdge = 0.1114f * windowSize.x, yOffsetBetweenHitboxes = 0.1552f * windowSize.y, xOffsetBetweenHitboxes = 0.075f * windowSize.x;
	float xLineOffset = 0.075f * windowSize.x, hitboxHeight = 0.02f * windowSize.y, hitboxWidth = 0.02f * windowSize.x, hitboxHeightDifference = 0.059f * windowSize.y;
	for(uint16_t rowIndex = 0; rowIndex < 6; ++rowIndex)
	{
		if(rowIndex < 3)
		{
			float yOddColumnOffset = yOffsetFromTop + yOffsetBetweenHitboxes * rowIndex, yEvenColumnOffset = yOffsetFromTop + yOffsetBetweenHitboxes * rowIndex + hitboxHeightDifference;
			float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex); 
			for (uint16_t columnIndex = 0; columnIndex < 2 * rowIndex + 7; ++columnIndex) 
			{
				if(columnIndex % 2 == 0)
				{
					if((yEvenColumnOffset <= mousePosition.y && mousePosition.y <= yEvenColumnOffset + hitboxHeight) &&
					   (xOffset + xOffsetBetweenHitboxes * columnIndex <= mousePosition.x && mousePosition.x <= xOffset + xOffsetBetweenHitboxes * columnIndex + hitboxWidth))
						return {rowIndex, columnIndex}; 
				}
				else
				{
					if((yOddColumnOffset <= mousePosition.y && mousePosition.y <= yOddColumnOffset + hitboxHeight) &&
					   (xOffset + xOffsetBetweenHitboxes * columnIndex <= mousePosition.x && mousePosition.x <= xOffset + xOffsetBetweenHitboxes * columnIndex + hitboxWidth))
						return {rowIndex, columnIndex}; 
				}
			}
		}
		else
		{
			float yOddColumnOffset = yOffsetFromTop + yOffsetBetweenHitboxes * rowIndex + hitboxHeightDifference, yEvenColumnOffset = yOffsetFromTop + yOffsetBetweenHitboxes * rowIndex;
			float xOffset = xOffsetFromLeftEdge + xLineOffset * (rowIndex - 3); 
			for (uint16_t columnIndex = 0; columnIndex < (-2) * rowIndex + 17; ++columnIndex)
			{
				if(columnIndex % 2 == 0)
				{
					if((yEvenColumnOffset <= mousePosition.y && mousePosition.y <= yEvenColumnOffset + hitboxHeight) &&
					   (xOffset + xOffsetBetweenHitboxes * columnIndex <= mousePosition.x && mousePosition.x <= xOffset + xOffsetBetweenHitboxes * columnIndex + hitboxWidth))
						return {rowIndex, columnIndex}; 
				}
				else
				{
					if((yOddColumnOffset <= mousePosition.y && mousePosition.y <= yOddColumnOffset + hitboxHeight) &&
					   (xOffset + xOffsetBetweenHitboxes * columnIndex <= mousePosition.x && mousePosition.x <= xOffset + xOffsetBetweenHitboxes * columnIndex + hitboxWidth))
						return {rowIndex, columnIndex}; 
				}
			}
		}
	}
	return {-1, -1};
}

std::tuple<uint16_t, uint16_t, uint16_t, uint16_t> GUI::GetEdgeCoordinates(const sf::Vector2i &mousePosition) const
{
	auto windowSize = m_whereToDrawPointer->GetSize();
	float yOffsetFromTop = 0.1f * windowSize.y, xOffsetFromLeftEdge = 0.1314f * windowSize.x, yOffsetBetweenHitboxes = 0.1552f * windowSize.y, xOffsetBetweenHitboxes = 0.075f * windowSize.x;
	float xLineOffset = 0.075f * windowSize.x, hitboxHeight = 0.039f * windowSize.y, hitboxWidth = 0.055f * windowSize.x;
	for (uint16_t rowIndex = 0; rowIndex < 6; ++rowIndex)
	{
		if (rowIndex < 3)
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenHitboxes * rowIndex;
			for (uint16_t columnIndex = 0; columnIndex < 2 * rowIndex + 6; ++columnIndex)
			{
				float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex) + xOffsetBetweenHitboxes * columnIndex;
				if ((yOffset <= mousePosition.y && mousePosition.y <= yOffset + hitboxHeight) && (xOffset <= mousePosition.x && mousePosition.x <= xOffset + hitboxWidth))
					return { rowIndex, columnIndex, rowIndex, columnIndex + 1 };
			}
		}
		else
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenHitboxes * rowIndex;
			for (uint16_t columnIndex = 0; columnIndex < (-2) * rowIndex + 16; ++columnIndex)
			{
				float xOffset = xOffsetFromLeftEdge + xLineOffset * (rowIndex - 3) + xOffsetBetweenHitboxes * columnIndex;
				if ((yOffset <= mousePosition.y && mousePosition.y <= yOffset + hitboxHeight) && (xOffset <= mousePosition.x && mousePosition.x <= xOffset + hitboxWidth))
					return { rowIndex, columnIndex, rowIndex, columnIndex + 1 };
			}
		}
	}
	yOffsetFromTop = 0.159f * windowSize.y, xOffsetFromLeftEdge = 0.1114f * windowSize.x, yOffsetBetweenHitboxes = 0.1552f * windowSize.y, xOffsetBetweenHitboxes = 0.1514f * windowSize.x;
	xLineOffset = 0.075f * windowSize.x, hitboxHeight = 0.063f * windowSize.y, hitboxWidth = 0.02f * windowSize.x;
	for (uint16_t rowIndex = 0; rowIndex < 5; ++rowIndex)
	{
		if (rowIndex < 3)
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenHitboxes * rowIndex;
			for (uint16_t columnIndex = 0; columnIndex < 2 * rowIndex + 6; ++columnIndex)
			{
				float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex) + xOffsetBetweenHitboxes * columnIndex;
				if ((yOffset <= mousePosition.y && mousePosition.y <= yOffset + hitboxHeight) && (xOffset <= mousePosition.x && mousePosition.x <= xOffset + hitboxWidth))
				{
					if (rowIndex < 2)
						return { rowIndex, columnIndex * 2, rowIndex + 1, columnIndex * 2 + 1 };
					else
						return { rowIndex, columnIndex * 2, rowIndex + 1, columnIndex * 2 };
				}
			}
		}
		else
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenHitboxes * rowIndex;
			for (uint16_t columnIndex = 0; columnIndex < (-2) * rowIndex + 16; ++columnIndex)
			{
				float xOffset = xOffsetFromLeftEdge + xLineOffset * (rowIndex - 2) + xOffsetBetweenHitboxes * columnIndex;
				if ((yOffset <= mousePosition.y && mousePosition.y <= yOffset + hitboxHeight) && (xOffset <= mousePosition.x && mousePosition.x <= xOffset + hitboxWidth))
					return { rowIndex, columnIndex * 2 + 1, rowIndex + 1, columnIndex * 2 };
			}
		}
	}
	return { -1, -1, -1, -1 };
}

std::array<int8_t, 5> GUI::GetResourceQuantitiesFromTradeTextboxes() const
{
	std::array<int8_t, 5> returnMe;
	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
		if (m_tradeWithPlayerWindowTextboxes[resourceIndex].GetString() != "")
			returnMe[resourceIndex] = std::stoi(m_tradeWithPlayerWindowTextboxes[resourceIndex].GetString());
		else
			returnMe[resourceIndex] = 0;
	return returnMe;
}

void GUI::ActivateClickedTextbox(const sf::Vector2i &mousePosition)
{
	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
		if (m_tradeWithPlayerWindowTextboxes[resourceIndex].IsClicked(mousePosition) == true)
		{
			m_activeTextbox = &(m_tradeWithPlayerWindowTextboxes[resourceIndex]);
			return;
		}
	if (m_activeTextbox != nullptr)
		m_activeTextbox->Deactivate();
	m_activeTextbox = nullptr;
}

void GUI::AddLetterToActiveTextbox(char letter)
{
	if (m_activeTextbox != nullptr)
		m_activeTextbox->AddLetter(letter);
}

void GUI::RemoveLetterFromActiveTextbox()
{
	if (m_activeTextbox != nullptr)
		m_activeTextbox->RemoveLetter();
}

void GUI::ProcessHint(const PlayerGameAction &bestAction)
{
	if (bestAction.GetAction() == PlayerGameActionType::BUILD_CITY) 
	{
		auto buildLocation = bestAction.GetBuildSpot();
		AddCityHighlight(std::get<0>(buildLocation), std::get<1>(buildLocation));
	}
	else if (bestAction.GetAction() == PlayerGameActionType::BUILD_SETTLEMENT)
	{
		auto buildLocation = bestAction.GetBuildSpot();
		AddSettlementHighlight(std::get<0>(buildLocation), std::get<1>(buildLocation));
	}
	else if (bestAction.GetAction() == PlayerGameActionType::BUILD_ROAD)
	{
		auto buildLocation = bestAction.GetRoadLocation();
		AddRoadHighlight(std::get<0>(buildLocation), std::get<1>(buildLocation), std::get<2>(buildLocation), std::get<3>(buildLocation));
	}
	else if (bestAction.GetAction() == PlayerGameActionType::BUY_DEVELOPMENT)
	{
		m_buyCardButton.Highlight();
	}
	else if (bestAction.GetAction() == PlayerGameActionType::PLAY_DEVELOPMENT)
	{
		auto developmentCardAction = bestAction.GetDevelopmentCardAction();
		if (developmentCardAction == DevelopmentCardAction::KnightCard)
			m_knightButton.Highlight();
		else if (developmentCardAction == DevelopmentCardAction::Monopoly)
			m_monopolyButton.Highlight();
		else if (developmentCardAction == DevelopmentCardAction::RoadBuilding)
			m_buildRoadsButton.Highlight();
		else if (developmentCardAction == DevelopmentCardAction::YearOfPlenty)
			m_yearOfPlentyButton.Highlight();
	}
	else if (bestAction.GetAction() == PlayerGameActionType::DICE_ROLL)
	{
		m_rollButton.Highlight();
	}
	else if (bestAction.GetAction() == PlayerGameActionType::TRADE_WITH_BANK)
	{
		m_tradeButton.Highlight();
		m_playerOrBankWindowBankButton.Highlight();
		m_resourceHighlight = bestAction.GetResourceExchange();
		std::swap(std::get<0>(m_resourceHighlight), std::get<1>(m_resourceHighlight));
		m_tradeWithBankWindowResourceButtons[static_cast<uint8_t>(std::get<0>(m_resourceHighlight))].Highlight();
	}
	else if (bestAction.GetAction() == PlayerGameActionType::END_TURN)
	{
		m_endTurnButton.Highlight();
	}
}

void GUI::MoveThiefOnBoard(const std::pair<int, int> &thiefPosition)
{
	sf::Vector2u windowSize = m_whereToDrawPointer->GetSize();
	float xTileOffset = 0.1214f * windowSize.x, yTileOffset = 0.09f * windowSize.y, xSmallOffsetBetweenTiles = 0.075f * windowSize.x;
	float xOffsetBetweenTiles = 0.1514f * windowSize.x, yOffsetBetweenTiles = 0.1552f * windowSize.y;
	std::random_device device;
	std::mt19937 numberGenerator{ device() };
	std::uniform_real_distribution<float> xDist(-0.02f, 0.02f), yDist(-0.02f, 0.02f);
	float xResult = 0.0557f * windowSize.x + xDist(numberGenerator) * windowSize.x, yResult = 0.0797f * windowSize.y + yDist(numberGenerator) * windowSize.y;
	if (thiefPosition.first < 3)
		m_thiefSprite.SetPosition({ xTileOffset + (2 - thiefPosition.first) * xSmallOffsetBetweenTiles + thiefPosition.second * xOffsetBetweenTiles + xResult, yTileOffset + thiefPosition.first * yOffsetBetweenTiles + yResult });
	else
		m_thiefSprite.SetPosition({ xTileOffset + (thiefPosition.first - 2) * xSmallOffsetBetweenTiles + thiefPosition.second * xOffsetBetweenTiles + xResult, yTileOffset + thiefPosition.first * yOffsetBetweenTiles + yResult });
}

std::pair<uint8_t, uint8_t> GUI::GetNewThiefPosition(const sf::Vector2i &mousePosition)
{
	sf::Vector2u windowSize = m_whereToDrawPointer->GetSize();
	float xTileOffset = 0.1214f * windowSize.x, yTileOffset = 0.149f * windowSize.y, xSmallOffsetBetweenTiles = 0.075f * windowSize.x;
	float xOffsetBetweenTiles = 0.1514f * windowSize.x, yOffsetBetweenTiles = 0.1552f * windowSize.y, hitboxHeight = 0.063f * windowSize.y, hitboxWidth = 0.1514 * windowSize.x;
	for (uint8_t rowIndex = 0; rowIndex < 5; ++rowIndex)
		if (rowIndex < 3)
			for (uint8_t columnIndex = 0; columnIndex < rowIndex + 3; ++columnIndex)
			{
				float xOffset = xTileOffset + (2 - rowIndex) * xSmallOffsetBetweenTiles + columnIndex * xOffsetBetweenTiles, yOffset = yTileOffset + rowIndex * yOffsetBetweenTiles;
				if ((xOffset <= mousePosition.x && mousePosition.x <= xOffset + hitboxWidth) && (yOffset <= mousePosition.y && mousePosition.y <= yOffset + hitboxHeight))
					return { rowIndex, columnIndex };
			}
		else
			for (uint8_t columnIndex = 0; columnIndex < 7 - rowIndex; ++columnIndex)
			{
				float xOffset = xTileOffset + (rowIndex - 2) * xSmallOffsetBetweenTiles + columnIndex * xOffsetBetweenTiles, yOffset = yTileOffset + rowIndex * yOffsetBetweenTiles;
				if ((xOffset <= mousePosition.x && mousePosition.x <= xOffset + hitboxWidth) && (yOffset <= mousePosition.y && mousePosition.y <= yOffset + hitboxHeight))
					return { rowIndex, columnIndex };
			}
	return { -1, -1 };
}

void GUI::RefreshPieces()
{
	// cycle through settlements and cities
	auto windowSize = m_whereToDrawPointer->GetSize();
	float yOffsetFromTop, xOffsetFromLeftEdge, yOffsetBetweenPieces, xOffsetBetweenPieces, xLineOffset, piecesHeightDifference;
	uint8_t initialSize = m_piecesOnBoard.size();
	m_piecesOnBoard.clear();
	m_piecesOnBoard.reserve(126);
	m_layers["4 Buildings"].ClearLayer();
	// cycle through roads
	// diagonals
	yOffsetFromTop = 0.09f * windowSize.y, xOffsetFromLeftEdge = 0.1494f * windowSize.x, yOffsetBetweenPieces = 0.1552f * windowSize.y, xOffsetBetweenPieces = 0.075f * windowSize.x, xLineOffset = 0.075f * windowSize.x;
	for (uint8_t rowIndex = 0; rowIndex < 6; ++rowIndex)
	{
		if (rowIndex < 3)
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
			for (uint8_t columnIndex = 0; columnIndex < 2 * rowIndex + 6; ++columnIndex)
			{
				if (m_controllerPointer->GetBoard().GetRoadAtEdgeCoordinates(rowIndex, columnIndex, rowIndex, columnIndex + 1) != nullptr)
				{
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex) + xOffsetBetweenPieces * columnIndex;
					m_piecesOnBoard.emplace_back(*(m_controllerPointer->GetBoard().GetRoadAtEdgeCoordinates(rowIndex, columnIndex, rowIndex, columnIndex + 1)));
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToCenter();
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetPosition({ xOffset, yOffset });
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].Transpose({ 0, windowSize.y * 0.02f });
					if (columnIndex % 2 == 0)
					{
						m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetRotation(150);
						m_piecesOnBoard[m_piecesOnBoard.size() - 1].Transpose({ 0, windowSize.y * 0.02f });
					}
					else
						m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetRotation(20);
					// m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToDefault();
					m_layers["4 Buildings"].AddObjectInLayer(m_piecesOnBoard[m_piecesOnBoard.size() - 1]);
				}
			}
		}
		else
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
			for (uint8_t columnIndex = 0; columnIndex < (-2) * rowIndex + 16; ++columnIndex)
			{
				if (m_controllerPointer->GetBoard().GetRoadAtEdgeCoordinates(rowIndex, columnIndex, rowIndex, columnIndex + 1) != nullptr)
				{
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (rowIndex - 3) + xOffsetBetweenPieces * columnIndex;
					m_piecesOnBoard.emplace_back(*(m_controllerPointer->GetBoard().GetRoadAtEdgeCoordinates(rowIndex, columnIndex, rowIndex, columnIndex + 1)));
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToCenter();
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetPosition({ xOffset, yOffset });
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].Transpose({ 0, windowSize.y * 0.01f });
					if (columnIndex % 2 == 0)
						m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetRotation(20);
					else
					{
						m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetRotation(150);
						m_piecesOnBoard[m_piecesOnBoard.size() - 1].Transpose({ 0, windowSize.y * 0.01f });
					}
					// m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToDefault();
					m_layers["4 Buildings"].AddObjectInLayer(m_piecesOnBoard[m_piecesOnBoard.size() - 1]);
				}
			}
		}
	}
	// verticals
	yOffsetFromTop = 0.139f * windowSize.y, xOffsetFromLeftEdge = 0.1484f * windowSize.x, yOffsetBetweenPieces = 0.1552f * windowSize.y, xOffsetBetweenPieces = 0.1514f * windowSize.x, xLineOffset = 0.075f * windowSize.x;
	for (uint8_t rowIndex = 0; rowIndex < 5; ++rowIndex)
	{
		if (rowIndex < 3)
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
			for (uint8_t columnIndex = 0; columnIndex < 2 * rowIndex + 6; ++columnIndex)
			{
				if (m_controllerPointer->GetBoard().GetRoadAtEdgeCoordinates(rowIndex, columnIndex * 2, rowIndex + 1, columnIndex * 2 + 1) != nullptr)
				{
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex) + xOffsetBetweenPieces * columnIndex;
					m_piecesOnBoard.emplace_back(*(m_controllerPointer->GetBoard().GetRoadAtEdgeCoordinates(rowIndex, columnIndex * 2, rowIndex+1, columnIndex*2 + 1)));
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetPosition({ xOffset, yOffset });
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToCenter();
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetRotation(90);
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToDefault();
					m_layers["4 Buildings"].AddObjectInLayer(m_piecesOnBoard[m_piecesOnBoard.size() - 1]);
				}
				if (m_controllerPointer->GetBoard().GetRoadAtEdgeCoordinates(rowIndex, columnIndex * 2, rowIndex + 1, columnIndex * 2) != nullptr)
				{
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex) + xOffsetBetweenPieces * columnIndex;
					m_piecesOnBoard.emplace_back(*(m_controllerPointer->GetBoard().GetRoadAtEdgeCoordinates(rowIndex, columnIndex*2, rowIndex+1, columnIndex*2)));
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetPosition({ xOffset, yOffset });
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToCenter();
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetRotation(90);
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToDefault();
					m_layers["4 Buildings"].AddObjectInLayer(m_piecesOnBoard[m_piecesOnBoard.size() - 1]);
				}
			}
		}
		else
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
			for (uint8_t columnIndex = 0; columnIndex < (-2) * rowIndex + 16; ++columnIndex)
			{
				if (m_controllerPointer->GetBoard().GetRoadAtEdgeCoordinates(rowIndex, columnIndex * 2 + 1, rowIndex + 1, columnIndex * 2) != nullptr)
				{
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (rowIndex - 2) + xOffsetBetweenPieces * columnIndex;
					m_piecesOnBoard.emplace_back(*(m_controllerPointer->GetBoard().GetRoadAtEdgeCoordinates(rowIndex, columnIndex*2+1, rowIndex+1, columnIndex*2)));
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetPosition({ xOffset, yOffset });
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToCenter();
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetRotation(90);
					m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToDefault();
					m_layers["4 Buildings"].AddObjectInLayer(m_piecesOnBoard[m_piecesOnBoard.size() - 1]);
				}
			}
		}
	}
	yOffsetFromTop = 0.045f * windowSize.y, xOffsetFromLeftEdge = 0.09f * windowSize.x, yOffsetBetweenPieces = 0.1502f * windowSize.y, xOffsetBetweenPieces = 0.075f * windowSize.x;
	xLineOffset = 0.075f * windowSize.x, piecesHeightDifference = 0.059f * windowSize.y;
	for (uint8_t rowIndex = 0; rowIndex < 6; ++rowIndex)
	{
		if (rowIndex < 3) {
			for (uint8_t columnIndex = 0; columnIndex < rowIndex * 2 + 7; ++columnIndex)
				if (m_controllerPointer->GetBoard().GetBuildingAtCornerCoordinates(rowIndex, columnIndex) !=
					std::nullopt) {
					float yOddColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex, yEvenColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex + piecesHeightDifference;
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex);
					m_piecesOnBoard.emplace_back(*(m_controllerPointer->GetBoard().GetBuildingAtCornerCoordinates(rowIndex, columnIndex).value()));
					if (columnIndex % 2 == 0)
						m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetPosition({xOffset + xOffsetBetweenPieces * columnIndex, yEvenColumnOffset});
					else
						m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetPosition({xOffset + xOffsetBetweenPieces * columnIndex, yOddColumnOffset});
					m_layers["4 Buildings"].AddObjectInLayer(m_piecesOnBoard[m_piecesOnBoard.size() - 1]);
				}
		}
		else
			for (uint8_t columnIndex = 0; columnIndex < 11 - (rowIndex - 3) * 2; ++columnIndex)
				if (m_controllerPointer->GetBoard().GetBuildingAtCornerCoordinates(rowIndex, columnIndex) != std::nullopt)
				{
					float yOddColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex + piecesHeightDifference, yEvenColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (rowIndex - 3);
					m_piecesOnBoard.emplace_back(*(m_controllerPointer->GetBoard().GetBuildingAtCornerCoordinates(rowIndex, columnIndex).value()));
					if (columnIndex % 2 == 0)
						m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetPosition({ xOffset + xOffsetBetweenPieces * columnIndex, yEvenColumnOffset });
					else
						m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetPosition({ xOffset + xOffsetBetweenPieces * columnIndex, yOddColumnOffset });
					m_layers["4 Buildings"].AddObjectInLayer(m_piecesOnBoard[m_piecesOnBoard.size() - 1]);
				}
	}
	if (m_piecesOnBoard.size() == initialSize + 1)
		m_layers["6 Hints"].ClearLayer();
}

void GUI::RefreshIcons(const std::array<uint8_t, 5> &resourceCount)
{
	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
		m_resourceIcons[resourceIndex].SetIconCount(resourceCount[resourceIndex]);
}

void GUI::RefreshCards(const std::array<uint16_t, 4> &cardCount)
{
	m_knightButton.SetButtonDetails(DevelopmentCardAction::KnightCard, std::to_string(cardCount[0]) + " Knight");
	m_buildRoadsButton.SetButtonDetails(DevelopmentCardAction::RoadBuilding, std::to_string(cardCount[1]) + " Build Roads");
	m_yearOfPlentyButton.SetButtonDetails(DevelopmentCardAction::YearOfPlenty, std::to_string(cardCount[2]) + " Year o' Plenty");
	m_monopolyButton.SetButtonDetails(DevelopmentCardAction::Monopoly, std::to_string(cardCount[3]) + " Monopoly");
}

void GUI::DrawWindow(const std::string &windowName)
{
	if (windowName == "tradeWindow")
	{
		EraseWindow();
		DrawTradeWindow();
	}
	else if (windowName == "tradeWithBankWindow")
	{
		EraseWindow();
		DrawTradeWithBankWindow();
	}
	else if (windowName == "tradeWithPlayerWindow")
	{
		EraseWindow();
		DrawTradeWithPlayerWindow();
	}
}

void GUI::EraseWindow()
{
	m_layers["5 Window"].ClearLayer();
}

void GUI::Draw()
{
	for (auto &[layerName, layer] : m_layers)
			layer.Draw(*m_whereToDrawPointer);
}

void GUI::InitializeBackground()
{
	m_background.LoadTexture("resources/sprites/background.png", m_whereToDrawPointer->GetSize());
	m_layers["0 Background"].AddObjectInLayer(m_background);
}

void GUI::InitializeTileMap()
{
	m_drawableTiles[0].resize(3);
	m_drawableTiles[1].resize(4);
	m_drawableTiles[2].resize(5);
	m_drawableTiles[3].resize(4);
	m_drawableTiles[4].resize(3);
	sf::Vector2u windowSize = m_whereToDrawPointer->GetSize();
	float xTileOffset = 0.1214f * windowSize.x, yTileOffset = 0.09f * windowSize.y, xSmallOffsetBetweenTiles = 0.075f * windowSize.x;
	float xOffsetBetweenTiles = 0.1514f * windowSize.x, yOffsetBetweenTiles = 0.1552f * windowSize.y;
	for(uint8_t rowIndex = 0; rowIndex < 5; ++rowIndex)
		if(rowIndex < 3)
			for (uint8_t columnIndex = 0; columnIndex < rowIndex + 3; ++columnIndex) 
			{
				m_drawableTiles[rowIndex][columnIndex].LoadNewTile(m_controllerPointer->GetBoard().GetTileAtPosition(rowIndex, columnIndex));
				m_drawableTiles[rowIndex][columnIndex].SetPosition({ xTileOffset + (2 - rowIndex) * xSmallOffsetBetweenTiles + columnIndex * xOffsetBetweenTiles, 
																	 yTileOffset + rowIndex * yOffsetBetweenTiles });
				m_layers["1 Tiles"].AddObjectInLayer(m_drawableTiles[rowIndex][columnIndex]);
			}
		else
			for (uint8_t columnIndex = 0; columnIndex < 7 - rowIndex; ++columnIndex)
			{
				m_drawableTiles[rowIndex][columnIndex].LoadNewTile(m_controllerPointer->GetBoard().GetTileAtPosition(rowIndex, columnIndex));
				m_drawableTiles[rowIndex][columnIndex].SetPosition({ xTileOffset + (rowIndex - 2) * xSmallOffsetBetweenTiles + columnIndex * xOffsetBetweenTiles, 
																	 yTileOffset + rowIndex * yOffsetBetweenTiles });
				m_layers["1 Tiles"].AddObjectInLayer(m_drawableTiles[rowIndex][columnIndex]);
			}
	std::random_device device;
	std::mt19937 numberGenerator{ device() };
	std::uniform_real_distribution<float> xDist(-0.02f, 0.02f), yDist(-0.02f, 0.02f);
	m_drawableNumberCircles[0].resize(3);
	m_drawableNumberCircles[1].resize(4);
	m_drawableNumberCircles[2].resize(5);
	m_drawableNumberCircles[3].resize(4);
	m_drawableNumberCircles[4].resize(3);
	for (uint8_t rowIndex = 0; rowIndex < 5; ++rowIndex)
		if (rowIndex < 3)
			for (uint8_t columnIndex = 0; columnIndex < rowIndex + 3; ++columnIndex)
			{
				float xResult = 0.0557f * windowSize.x + xDist(numberGenerator) * windowSize.x, yResult = 0.0797f * windowSize.y + yDist(numberGenerator) * windowSize.y;
				if (m_controllerPointer->GetBoard().GetTileAtPosition(rowIndex, columnIndex) != Board::TileType::Desert)
				{
					m_drawableNumberCircles[rowIndex][columnIndex].LoadNewNumber(m_controllerPointer->GetBoard().GetNumberAtTilePosition(rowIndex, columnIndex));
					m_drawableNumberCircles[rowIndex][columnIndex].SetPosition({ xTileOffset + (2 - rowIndex) * xSmallOffsetBetweenTiles + columnIndex * xOffsetBetweenTiles + xResult,
																				 yTileOffset + rowIndex * yOffsetBetweenTiles + yResult });
					m_layers["2 Pieces"].AddObjectInLayer(m_drawableNumberCircles[rowIndex][columnIndex]);
				}
			}
		else
			for (uint8_t columnIndex = 0; columnIndex < 7 - rowIndex; ++columnIndex)
			{
				float xResult = 0.0557f * windowSize.x + xDist(numberGenerator) * windowSize.x, yResult = 0.0797f * windowSize.y + yDist(numberGenerator) * windowSize.y;
				if (m_controllerPointer->GetBoard().GetTileAtPosition(rowIndex, columnIndex) != Board::TileType::Desert)
				{
					m_drawableNumberCircles[rowIndex][columnIndex].LoadNewNumber(m_controllerPointer->GetBoard().GetNumberAtTilePosition(rowIndex, columnIndex));
					m_drawableNumberCircles[rowIndex][columnIndex].SetPosition({ xTileOffset + (rowIndex - 2) * xSmallOffsetBetweenTiles + columnIndex * xOffsetBetweenTiles + xResult,
																				 yTileOffset + rowIndex * yOffsetBetweenTiles + yResult });
					m_layers["2 Pieces"].AddObjectInLayer(m_drawableNumberCircles[rowIndex][columnIndex]);
				}
			}
	MoveThiefOnBoard(m_controllerPointer->GetBoard().GetThiefLocation());
	m_layers["2 Pieces"].AddObjectInLayer(m_thiefSprite);
}

void GUI::InitializeButtons()
{
	sf::Vector2u windowSize = m_whereToDrawPointer->GetSize();
	float xOffset = 0.8f * windowSize.x;
	m_hintButton.SetButtonDetails("hint", "Hint");
	m_hintButton.SetPosition({ xOffset, 0.64f * windowSize.y });
	m_rollButton.SetButtonDetails("roll", "Roll");
	m_rollButton.SetPosition({ xOffset, 0.71f * windowSize.y });
	m_buyCardButton.SetButtonDetails("buyCard", "Buy Card");
	m_buyCardButton.SetPosition({ xOffset, 0.78f * windowSize.y });
	m_tradeButton.SetButtonDetails("trade", "Trade");
	m_tradeButton.SetPosition({ xOffset, 0.85f * windowSize.y });
	m_exitButton.SetButtonDetails("exit", "Exit");
	m_endTurnButton.SetButtonDetails("endTurn", "End Turn");
	m_endTurnButton.SetPosition({ xOffset, 0.92f * windowSize.y });
	m_knightButton.SetButtonDetails(DevelopmentCardAction::KnightCard, "0 Knight");
	m_knightButton.SetPosition({ xOffset, (0.07f * 0 + 0.01f) * windowSize.y });
	m_buildRoadsButton.SetButtonDetails(DevelopmentCardAction::RoadBuilding, "0 Build Roads");
	m_buildRoadsButton.SetPosition({ xOffset, (0.07f * 1 + 0.01f) * windowSize.y });
	m_yearOfPlentyButton.SetButtonDetails(DevelopmentCardAction::YearOfPlenty, "0 Year o' Plenty");
	m_yearOfPlentyButton.SetPosition({ xOffset, (0.07f * 2 + 0.01f) * windowSize.y });
	m_monopolyButton.SetButtonDetails(DevelopmentCardAction::Monopoly, "0 Monopoly");
	m_monopolyButton.SetPosition({ xOffset, (0.07f * 3 + 0.01f) * windowSize.y });
	m_layers["3 Buttons"].AddObjectInLayer(m_hintButton);
	m_layers["3 Buttons"].AddObjectInLayer(m_rollButton);
	m_layers["3 Buttons"].AddObjectInLayer(m_buyCardButton);
	m_layers["3 Buttons"].AddObjectInLayer(m_tradeButton);
	m_layers["3 Buttons"].AddObjectInLayer(m_endTurnButton);
	m_layers["3 Buttons"].AddObjectInLayer(m_knightButton);
	m_layers["3 Buttons"].AddObjectInLayer(m_buildRoadsButton);
	m_layers["3 Buttons"].AddObjectInLayer(m_yearOfPlentyButton);
	m_layers["3 Buttons"].AddObjectInLayer(m_monopolyButton);
}

void GUI::InitializeResourceIcons()
{
	sf::Vector2u windowSize = m_whereToDrawPointer->GetSize();
	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
	{
		m_resourceIcons[resourceIndex] = DrawableResourceIcon(static_cast<ResourceType>(resourceIndex));
		m_resourceIcons[resourceIndex].SetPosition({ windowSize.x * 0.02f, windowSize.y * 0.7f + windowSize.y * 0.05f * resourceIndex });
		m_layers["3 Buttons"].AddObjectInLayer(m_resourceIcons[resourceIndex]);
	}
}

void GUI::InitializeTradeWindow()
{
	sf::Vector2u mainWindowSize = m_whereToDrawPointer->GetSize(), windowSize = { 260, 300 };
	m_playerOrBankWindowBackground.SetWindowSize(windowSize);
	m_playerOrBankWindowBackground.Transpose({ (mainWindowSize.x - windowSize.x) / 2.0f, (mainWindowSize.y - windowSize.y) / 2.0f });
	m_playerOrBankWindowPlayerButton.SetButtonDetails("player", "Trade with player");
	m_playerOrBankWindowPlayerButton.SetPosition({ windowSize.x * 0.1f, windowSize.y * 0.2f });
	m_playerOrBankWindowPlayerButton.Transpose({ (mainWindowSize.x - windowSize.x) / 2.0f, (mainWindowSize.y - windowSize.y) / 2.0f });
	m_playerOrBankWindowBankButton.SetButtonDetails("bank", "Trade with bank");
	m_playerOrBankWindowBankButton.SetPosition({ windowSize.x * 0.1f, windowSize.y * 0.7f });
	m_playerOrBankWindowBankButton.Transpose({ (mainWindowSize.x - windowSize.x) / 2.0f, (mainWindowSize.y - windowSize.y) / 2.0f });
}

void GUI::InitializeTradeWithBankWindow()
{
	sf::Vector2u mainWindowSize = m_whereToDrawPointer->GetSize(), windowSize = { 260, 300 };
	m_tradeWithBankWindowBackground.SetWindowSize(windowSize);
	m_tradeWithBankWindowBackground.Transpose({ (mainWindowSize.x - windowSize.x) / 2.0f, (mainWindowSize.y - windowSize.y) / 2.0f });
	m_tradeWithBankWindowResourceButtons[0] = Button<std::string>("brick", "Bricks");
	m_tradeWithBankWindowResourceButtons[1] = Button<std::string>("grain", "Grain");
	m_tradeWithBankWindowResourceButtons[2] = Button<std::string>("lumber", "Lumber");
	m_tradeWithBankWindowResourceButtons[3] = Button<std::string>("ore", "Ore");
	m_tradeWithBankWindowResourceButtons[4] = Button<std::string>("wool", "Wool");
	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
	{
		m_tradeWithBankWindowResourceButtons[resourceIndex].SetPosition({ windowSize.x * 0.1f, windowSize.y * 0.1f + windowSize.y * 0.15f * resourceIndex });
		m_tradeWithBankWindowResourceButtons[resourceIndex].Transpose({ (mainWindowSize.x - windowSize.x) / 2.0f, (mainWindowSize.y - windowSize.y) / 2.0f });
	}
}

void GUI::InitializeTradeWithPlayerWindow()
{
	sf::Vector2u mainWindowSize = m_whereToDrawPointer->GetSize(), windowSize = { 600, 300 };
	m_tradeWithPlayerWindowBackground.SetWindowSize(windowSize);
	m_tradeWithPlayerWindowBackground.Transpose({ (mainWindowSize.x - windowSize.x) / 2.0f, (mainWindowSize.y - windowSize.y) / 2.0f });
	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
	{
		m_tradeWithPlayerWindowTextboxes[resourceIndex] = Textbox();
		m_tradeWithPlayerWindowTextboxes[resourceIndex].SetPosition({ windowSize.x * 0.05f, windowSize.y * 0.1f + windowSize.y * 0.15f * resourceIndex});
		m_tradeWithPlayerWindowTextboxes[resourceIndex].Transpose({ (mainWindowSize.x - windowSize.x) / 2.0f, (mainWindowSize.y - windowSize.y) / 2.0f });
	}
	for (uint16_t playerIndex = 0; playerIndex < 4; ++playerIndex)
	{
		m_tradeWithPlayerWindowPlayerButtons[playerIndex] = Button<std::string>("player" + std::to_string(playerIndex), "Player " + std::to_string(playerIndex + 1));
		m_tradeWithPlayerWindowPlayerButtons[playerIndex].SetPosition({ windowSize.x * 0.5f, windowSize.y * 0.1f + windowSize.y * 0.15f * playerIndex });
		m_tradeWithPlayerWindowPlayerButtons[playerIndex].Transpose({ (mainWindowSize.x - windowSize.x) / 2.0f, (mainWindowSize.y - windowSize.y) / 2.0f });
	}
}

void GUI::AddSettlementHighlight(const uint8_t x, const uint8_t y)
{
	auto windowSize = m_whereToDrawPointer->GetSize();
	float yOffsetFromTop = 0.045f * windowSize.y, xOffsetFromLeftEdge = 0.09f * windowSize.x, yOffsetBetweenPieces = 0.1502f * windowSize.y, xOffsetBetweenPieces = 0.075f * windowSize.x;
	float xLineOffset = 0.075f * windowSize.x, piecesHeightDifference = 0.059f * windowSize.y;
	for (uint8_t rowIndex = 0; rowIndex < 6; ++rowIndex)
	{
		if (rowIndex < 3) {
			for (uint8_t columnIndex = 0; columnIndex < rowIndex * 2 + 7; ++columnIndex)
				if (rowIndex == x && columnIndex == y)
				{
					m_pieceHighlight.SetRotation(0);
					float yOddColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex, yEvenColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex + piecesHeightDifference;
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex);
					m_pieceHighlight.SetType(PieceType::Settlement);
					if (columnIndex % 2 == 0)
						m_pieceHighlight.SetPosition({ xOffset + xOffsetBetweenPieces * columnIndex, yEvenColumnOffset });
					else
						m_pieceHighlight.SetPosition({ xOffset + xOffsetBetweenPieces * columnIndex, yOddColumnOffset });
					m_layers["6 Hints"].AddObjectInLayer(m_pieceHighlight);
				}
		}
		else
			for (uint8_t columnIndex = 0; columnIndex < 11 - (rowIndex - 3) * 2; ++columnIndex)
				if (rowIndex == x && columnIndex == y)
				{
					m_pieceHighlight.SetRotation(0);
					float yOddColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex + piecesHeightDifference, yEvenColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (rowIndex - 3);
					m_pieceHighlight.SetType(PieceType::Settlement);
					if (columnIndex % 2 == 0)
						m_pieceHighlight.SetPosition({ xOffset + xOffsetBetweenPieces * columnIndex, yEvenColumnOffset });
					else
						m_pieceHighlight.SetPosition({ xOffset + xOffsetBetweenPieces * columnIndex, yOddColumnOffset });
					m_layers["6 Hints"].AddObjectInLayer(m_pieceHighlight);
				}
	}
}

void GUI::AddCityHighlight(const uint8_t x, const uint8_t y)
{
	auto windowSize = m_whereToDrawPointer->GetSize();
	float yOffsetFromTop = 0.045f * windowSize.y, xOffsetFromLeftEdge = 0.09f * windowSize.x, yOffsetBetweenPieces = 0.1502f * windowSize.y, xOffsetBetweenPieces = 0.075f * windowSize.x;
	float xLineOffset = 0.075f * windowSize.x, piecesHeightDifference = 0.059f * windowSize.y;
	for (uint8_t rowIndex = 0; rowIndex < 6; ++rowIndex)
	{
		if (rowIndex < 3) {
			for (uint8_t columnIndex = 0; columnIndex < rowIndex * 2 + 7; ++columnIndex)
				if (rowIndex == x && columnIndex == y)
				{
					m_pieceHighlight.SetRotation(0);
					float yOddColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex, yEvenColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex + piecesHeightDifference;
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex);
					m_pieceHighlight.SetType(PieceType::City);
					if (columnIndex % 2 == 0)
						m_pieceHighlight.SetPosition({ xOffset + xOffsetBetweenPieces * columnIndex, yEvenColumnOffset });
					else
						m_pieceHighlight.SetPosition({ xOffset + xOffsetBetweenPieces * columnIndex, yOddColumnOffset });
					m_layers["6 Hints"].AddObjectInLayer(m_pieceHighlight);
				}
		}
		else
			for (uint8_t columnIndex = 0; columnIndex < 11 - (rowIndex - 3) * 2; ++columnIndex)
				if (rowIndex == x && columnIndex == y)
				{
					m_pieceHighlight.SetRotation(0);
					float yOddColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex + piecesHeightDifference, yEvenColumnOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (rowIndex - 3);
					m_pieceHighlight.SetType(PieceType::City);
					if (columnIndex % 2 == 0)
						m_pieceHighlight.SetPosition({ xOffset + xOffsetBetweenPieces * columnIndex, yEvenColumnOffset });
					else
						m_pieceHighlight.SetPosition({ xOffset + xOffsetBetweenPieces * columnIndex, yOddColumnOffset });
					m_layers["6 Hints"].AddObjectInLayer(m_pieceHighlight);
				}
	}
}

void GUI::AddRoadHighlight(const uint8_t x1, const uint8_t y1, const uint8_t x2, const uint8_t y2)
{
	auto windowSize = m_whereToDrawPointer->GetSize();
	float yOffsetFromTop, xOffsetFromLeftEdge, yOffsetBetweenPieces, xOffsetBetweenPieces, xLineOffset, piecesHeightDifference;
	// cycle through roads
	// diagonals
	yOffsetFromTop = 0.09f * windowSize.y, xOffsetFromLeftEdge = 0.1494f * windowSize.x, yOffsetBetweenPieces = 0.1552f * windowSize.y, xOffsetBetweenPieces = 0.075f * windowSize.x, xLineOffset = 0.075f * windowSize.x;
	for (uint8_t rowIndex = 0; rowIndex < 6; ++rowIndex)
	{
		if (rowIndex < 3)
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
			for (uint8_t columnIndex = 0; columnIndex < 2 * rowIndex + 6; ++columnIndex)
			{
				if (rowIndex == x1 && columnIndex == y1 && rowIndex == x2 && columnIndex + 1 == y2)
				{
					m_pieceHighlight.SetRotation(0);
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex) + xOffsetBetweenPieces * columnIndex;
					m_pieceHighlight.SetType(PieceType::Road);
					m_pieceHighlight.SetPosition({ xOffset, yOffset });
					if (columnIndex % 2 == 0) {
						m_pieceHighlight.SetRotation(150);
						m_pieceHighlight.Transpose({ 0.05f * windowSize.x, 0.04f * windowSize.y });
					}
					else {
						m_pieceHighlight.SetRotation(20);
						m_pieceHighlight.Transpose({ 0, -0.02f * windowSize.y });
					}
					// m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToDefault();
					m_layers["6 Hints"].AddObjectInLayer(m_pieceHighlight);
				}
			}
		}
		else
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
			for (uint8_t columnIndex = 0; columnIndex < (-2) * rowIndex + 16; ++columnIndex)
			{
				if (rowIndex == x1 && columnIndex == y1 && rowIndex == x2 && columnIndex + 1 == y2)
				{
					m_pieceHighlight.SetRotation(0);
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (rowIndex - 3) + xOffsetBetweenPieces * columnIndex;
					m_pieceHighlight.SetType(PieceType::Road);
					m_pieceHighlight.SetOriginToCenter();
					m_pieceHighlight.SetPosition({ xOffset, yOffset });
					if (columnIndex % 2 == 0) {
						m_pieceHighlight.SetRotation(20);
						m_pieceHighlight.Transpose({ 0.01f * windowSize.x, 0 });
					}
					else {
						m_pieceHighlight.SetRotation(150);
						m_pieceHighlight.Transpose({ 0, 0.02f * windowSize.y });
					}
					// m_piecesOnBoard[m_piecesOnBoard.size() - 1].SetOriginToDefault();
					m_layers["6 Hints"].AddObjectInLayer(m_pieceHighlight);
				}
			}
		}
	}
	// verticals
	yOffsetFromTop = 0.139f * windowSize.y, xOffsetFromLeftEdge = 0.1484f * windowSize.x, yOffsetBetweenPieces = 0.1552f * windowSize.y, xOffsetBetweenPieces = 0.1514f * windowSize.x, xLineOffset = 0.075f * windowSize.x;
	for (uint8_t rowIndex = 0; rowIndex < 5; ++rowIndex)
	{
		if (rowIndex < 3)
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
			for (uint8_t columnIndex = 0; columnIndex < 2 * rowIndex + 6; ++columnIndex)
			{
				if (rowIndex == x1 && columnIndex * 2 == y1 && rowIndex + 1 == x2 && columnIndex * 2 + 1 == y2)
				{
					m_pieceHighlight.SetRotation(0);
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex) + xOffsetBetweenPieces * columnIndex;
					m_pieceHighlight.SetType(PieceType::Road);
					m_pieceHighlight.SetPosition({ xOffset, yOffset });
					m_pieceHighlight.SetOriginToCenter();
					m_pieceHighlight.SetRotation(90);
					m_pieceHighlight.SetOriginToDefault();
					m_layers["6 Hints"].AddObjectInLayer(m_pieceHighlight);
				}
				if (rowIndex == x1 && columnIndex * 2 == y1 && rowIndex + 1 == x2 && columnIndex * 2 == y2)
				{
					m_pieceHighlight.SetRotation(0);
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (2 - rowIndex) + xOffsetBetweenPieces * columnIndex;
					m_pieceHighlight.SetType(PieceType::Road);
					m_pieceHighlight.SetPosition({ xOffset, yOffset });
					m_pieceHighlight.SetOriginToCenter();
					m_pieceHighlight.SetRotation(90);
					m_pieceHighlight.SetOriginToDefault();
					m_layers["6 Hints"].AddObjectInLayer(m_pieceHighlight);
				}
			}
		}
		else
		{
			float yOffset = yOffsetFromTop + yOffsetBetweenPieces * rowIndex;
			for (uint8_t columnIndex = 0; columnIndex < (-2) * rowIndex + 16; ++columnIndex)
			{
				if (rowIndex == x1 && columnIndex * 2 + 1 == y1 && rowIndex + 1 == x2 && columnIndex * 2 == y2)
				{
					m_pieceHighlight.SetRotation(0);
					float xOffset = xOffsetFromLeftEdge + xLineOffset * (rowIndex - 2) + xOffsetBetweenPieces * columnIndex;
					m_pieceHighlight.SetType(PieceType::Road);
					m_pieceHighlight.SetPosition({ xOffset, yOffset });
					m_pieceHighlight.SetOriginToCenter();
					m_pieceHighlight.SetRotation(90);
					m_pieceHighlight.SetOriginToDefault();
					m_layers["6 Hints"].AddObjectInLayer(m_pieceHighlight);
				}
			}
		}
	}
}

void GUI::ClearHints()
{
	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
			m_tradeWithBankWindowResourceButtons[resourceIndex].Dehighlight();
	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
			m_tradeWithBankWindowResourceButtons[resourceIndex].Dehighlight();
	m_playerOrBankWindowBankButton.Dehighlight();
	m_tradeButton.Dehighlight();
	m_exitButton.Dehighlight();
	m_endTurnButton.Dehighlight();
	m_knightButton.Dehighlight();
	m_buildRoadsButton.Dehighlight();
	m_yearOfPlentyButton.Dehighlight();
	m_rollButton.Dehighlight();
	m_buyCardButton.Dehighlight();

	m_layers["6 Hints"].ClearLayer();
}

void GUI::UpdateWithPlayersNames() {
	for (uint16_t playerIndex = 0; playerIndex < 4; ++playerIndex)
	{
		if (playerIndex < m_controllerPointer->GetNumberOfPlayers()) {
			m_tradeWithPlayerWindowPlayerButtons[playerIndex].SetButtonDetails("player" + std::to_string(playerIndex),
					m_controllerPointer->GetSpecificPlayer(playerIndex).GetName());
		}
	}
}

void GUI::DrawTradeWindow()
{
	m_layers["5 Window"].AddObjectInLayer(m_playerOrBankWindowBackground);
	m_layers["5 Window"].AddObjectInLayer(m_playerOrBankWindowPlayerButton);
	m_layers["5 Window"].AddObjectInLayer(m_playerOrBankWindowBankButton);
}

void GUI::DrawTradeWithBankWindow()
{
	m_layers["5 Window"].AddObjectInLayer(m_tradeWithBankWindowBackground);
	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
		m_layers["5 Window"].AddObjectInLayer(m_tradeWithBankWindowResourceButtons[resourceIndex]);
}

void GUI::DrawTradeWithPlayerWindow()
{
	m_layers["5 Window"].AddObjectInLayer(m_tradeWithPlayerWindowBackground);
	for (uint8_t resourceIndex = 0; resourceIndex < 5; ++resourceIndex)
		m_layers["5 Window"].AddObjectInLayer(m_tradeWithPlayerWindowTextboxes[resourceIndex]);
	for (uint16_t playerIndex = 0; playerIndex < m_controllerPointer->GetNumberOfPlayers(); ++playerIndex)
		m_layers["5 Window"].AddObjectInLayer(m_tradeWithPlayerWindowPlayerButtons[playerIndex]);
}

void GUI::AddLayer(const std::string &layerName)
{
	m_layers[layerName] = (DrawableLayer());
}

void GUI::SetGameInfoFeed(const std::string &feed) 
{
	m_feedText.SetText(feed);
    std::cout << "feed: " << feed << "\n";
}
