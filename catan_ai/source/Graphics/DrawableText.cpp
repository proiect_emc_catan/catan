#include "Graphics/DrawableText.h"

DrawableText::DrawableText()
{
	m_text.setFont(m_font);
	m_text.setCharacterSize(24);
	m_text.setFillColor(sf::Color::White);
	m_text.setOutlineColor(sf::Color::Black);
	m_text.setOutlineThickness(3);
}

void DrawableText::Draw(sf::RenderWindow &drawHere)
{
	auto transform = m_sprite.getTransform();
	drawHere.draw(m_text, transform);
}

void DrawableText::SetText(const std::string &text)
{
	m_text.setString(text);
}
