#include "Graphics/DrawableNumberCircle.h"

#include <string>

sf::Texture DrawableNumberCircle::m_numberTexture;

DrawableNumberCircle::DrawableNumberCircle(uint8_t number)
{
	if (m_numberTexture.getSize().x == 0)
	{
		m_numberTexture.loadFromFile("resources/sprites/number_circles.png");
		m_sprite.setTexture(m_numberTexture);
	}
	else
		m_sprite.setTexture(m_numberTexture);
	LoadNewNumber(number);
}

void DrawableNumberCircle::LoadNewNumber(uint8_t number)
{
	int16_t tileWidth = 40, tileHeight = 40;
	for (uint16_t offsetMultiplier = 0; offsetMultiplier < 11; ++offsetMultiplier)
		if (offsetMultiplier + 2 == number)
		{
			m_sprite.setTextureRect({ tileWidth * offsetMultiplier, 0, tileWidth, tileHeight });
			break;
		}
}

