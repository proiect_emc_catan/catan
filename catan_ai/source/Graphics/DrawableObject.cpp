#include <iostream>
#include "Graphics/DrawableObject.h"

sf::Font DrawableObject::m_font;

DrawableObject::DrawableObject(const std::string &imagePath)
{
	DrawableObject::m_font.loadFromFile("resources/fonts/default.ttf");
}

void DrawableObject::Draw(sf::RenderWindow &drawHere)
{
	drawHere.draw(m_sprite);
}

void DrawableObject::SetPosition(const sf::Vector2f &newPosition)
{
	m_sprite.setPosition(newPosition);
}

void DrawableObject::SetRotation(float angle)
{
	m_sprite.setRotation(angle);
}

void DrawableObject::Transpose(const sf::Vector2f &transposeBy)
{
	auto currentPosition = GetPosition();
	SetPosition({ currentPosition.x + transposeBy.x, currentPosition.y + transposeBy.y });
}

void DrawableObject::SetOriginToCenter()
{
	m_sprite.setOrigin(m_sprite.getLocalBounds().width / 2.f, m_sprite.getLocalBounds().height / 2.f);
}

void DrawableObject::SetOriginToDefault()
{
	m_sprite.setOrigin(0, 0);
}

bool DrawableObject::IsClicked(const sf::Vector2i &mousePosition)
{
	auto box = this->GetBoundingBox();
	return (box.left <= mousePosition.x && mousePosition.x <= box.left + box.width) && (box.top <= mousePosition.y && mousePosition.y <= box.top + box.height);
}

const sf::Sprite & DrawableObject::GetSprite() const
{
	return m_sprite;
}

const sf::FloatRect DrawableObject::GetBoundingBox() const
{
	return m_sprite.getGlobalBounds();
}

const sf::Vector2f & DrawableObject::GetPosition() const
{
	return m_sprite.getPosition();
}
