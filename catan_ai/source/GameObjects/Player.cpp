//
// Created by timi on 06.11.2018.
//

#include <GameObjects/Player.h>
#include <algorithm>
#include <numeric>
#include <GameObjects/Dice.h>

#include "GameObjects/Player.h"
#include "GameObjects/Piece.h"
#include "GameObjects/ResourceType.h"
#include "GameObjects/DevelopmentCard.h"



Player::Player() {
	InitAvailablePieces();
}

Player::Player(const std::string & name)
	:m_name(name)
{
	InitAvailablePieces();
}


void Player::InitAvailablePieces() {
	m_freeSettlements = 2;
	m_freeRoads = 2;
	m_availablePieces[static_cast<uint8_t>(PieceType::City)] = 4;
	m_availablePieces[static_cast<uint8_t>(PieceType::Settlement)] = 5;
	m_availablePieces[static_cast<uint8_t>(PieceType::Road)] = 15;
	m_availablePieces[static_cast<uint8_t>(PieceType::DevelopmentCard)] = 100; // No limit actually

	m_availableResources = {0, 0, 0, 0, 0};
}

std::string Player::GetName() const
{
	return m_name;
}

bool Player::operator==(const Player & other) const
{
	if (this->m_name != other.m_name)
		return false;
	if (this->m_availableResources != other.m_availableResources)
		return false;
	if (this->m_availablePieces != other.m_availablePieces)
		return false;
	if (this->m_freeSettlements != other.m_freeSettlements)
		return false;
	if (this->m_freeRoads != other.m_freeRoads)
		return false;
	if (this->m_devCards != other.m_devCards)
		return false;
	if (this->m_playedCards != other.m_playedCards)
		return false;
	return true;
}

bool Player::CanPickPiece(PieceType type) const {
	if (m_availablePieces[static_cast<uint8_t>(type)] == 0)
		return false;
	if ((type == PieceType::Road && m_freeRoads > 0) || (type == PieceType::Settlement && m_freeSettlements > 0)) {
	    if (m_availablePieces[static_cast<uint8_t >(PieceType::Settlement)] == 5)
            return type == PieceType::Settlement;
	    if (m_availablePieces[static_cast<uint8_t >(PieceType::Settlement)] == 4 &&
	        m_availablePieces[static_cast<uint8_t >(PieceType::Road)] == 15)
            return type == PieceType::Road;
        if (m_availablePieces[static_cast<uint8_t >(PieceType::Settlement)] == 4 &&
            m_availablePieces[static_cast<uint8_t >(PieceType::Road)] == 14)
            return type == PieceType::Settlement;
        if (m_availablePieces[static_cast<uint8_t >(PieceType::Settlement)] == 3 &&
            m_availablePieces[static_cast<uint8_t >(PieceType::Road)] == 14)
            return type == PieceType::Road;
        return true;
    }
	for (uint8_t index = 0; index < ResourcesTypeCount; ++index)
		if (m_availableResources[index] < Piece::cost[static_cast<uint8_t>(type)][index])
			return false;
	return true;
}

std::shared_ptr<Piece> Player::BuyPiece(PieceType type) {
	m_availablePieces[static_cast<uint8_t>(type)]--;

	/// If it's not free
    if (!((type == PieceType::Road && m_freeRoads > 0) || (type == PieceType::Settlement && m_freeSettlements > 0)))
        Expand(Piece::cost[static_cast<uint8_t >(type)]);
    else {
    	if (type == PieceType::Road)
    		m_freeRoads--;
    	else
    		m_freeSettlements--;
    }

	if (type != PieceType::DevelopmentCard)
		return std::shared_ptr<Piece>(new Piece(type, *this));
	auto card = std::shared_ptr<DevelopmentCard>(new DevelopmentCard(*this));

	/// Victory Point Cards act as if they are played instantly
	if (card->GetAction() != DevelopmentCardAction::VictoryPointCard)
		this->m_devCards.push_back(card);
	else
		this->m_playedCards.push_back(card);

	return std::dynamic_pointer_cast<Piece>(card);
}

void Player::GiveResource(ResourceType resource, uint8_t amount) {
    if (resource == ResourceType::None)
        return;
	m_availableResources[static_cast<uint8_t>(resource)] += amount;
}

uint8_t Player::GetAvailable(PieceType piece) const {
	return m_availablePieces[static_cast<uint8_t>(piece)];
}

bool Player::HasCard(DevelopmentCardAction cardAction) const {
    return std::any_of(m_devCards.begin(), m_devCards.end(),
            [cardAction](auto card){ return card->GetAction() == cardAction; });
}

void Player::PlayCard(DevelopmentCardAction cardAction) {
	auto card = std::find_if(m_devCards.begin(), m_devCards.end(),
			[cardAction](auto card){ return card->GetAction() == cardAction; });
	m_playedCards.push_back(*card);
	m_devCards.erase(card);
}

const std::vector<std::shared_ptr<DevelopmentCard>>& Player::GetPlayedCards() const {
	return m_playedCards;
}

const std::vector<std::shared_ptr<DevelopmentCard>> &Player::GetPlayableCards() const {
	return m_devCards;
}

std::array<uint16_t, 4 > Player::GetCardsCount() const {
	auto a = std::array<uint16_t, 4 > {0, 0, 0, 0};
	for (const auto &cardptr : m_devCards)
		a[static_cast<uint8_t >(cardptr->GetAction())]++;
	return a;
}

bool Player::CanExpand(const ResourceArray& resources) const {
	for (uint8_t i = 0; i < resources.size(); i++)
		if (resources[i] > m_availableResources[i])
			return false;
	return true;
}

void Player::ExchangeWithPlayer(const ResourceArray&giveResources, const ResourceArray &getResources,
                                Player &otherPlayer) {
	for (uint8_t i = 0; i < giveResources.size(); i++) {
		m_availableResources[i] -= giveResources[i];
		m_availableResources[i] += getResources[i];
		otherPlayer.m_availableResources[i] += giveResources[i];
		otherPlayer.m_availableResources[i] -= getResources[i];
	}
}

void Player::Expand(const ResourceArray& resources) {
    for (uint8_t i = 0; i < resources.size(); i++)
        m_availableResources[i] -= resources[i];
}

void Player::AddFreeRoads(uint8_t amount) {
    m_freeRoads += amount;
}

std::optional<ResourceType> Player::LoseRandomResource() {
	auto availableCount = static_cast<uint8_t>(std::accumulate(m_availableResources.begin(), m_availableResources.end(), 0));
	if (availableCount == 0)
		return {};
	uint8_t choice = Dice(availableCount).RollOneDie();
	uint8_t sum = 0;
	ResourceType resourceType;
	for (uint8_t i = 0; i < m_availableResources.size(); i++) {
		sum += m_availableResources[i];
		if (choice <= sum) {
			resourceType = ResourceType (i);
			m_availableResources[i]--;
			break;
		}
	}
	return resourceType;
}

bool Player::DiscardHalfResourcesIfNecessary() {
	auto availableCount = static_cast<uint8_t>(std::accumulate(m_availableResources.begin(), m_availableResources.end(), 0));

	/// He is safe
	if (availableCount <= 7)
		return false;

	for (uint8_t discarded = 0; discarded < availableCount/2; ++discarded)
		LoseRandomResource();
	return true;
}

const ResourceArray &Player::GetAvailableResources() const {
    return m_availableResources;
}

bool Player::CanExpand(ResourceType resource, uint8_t amount) const {
    return m_availableResources[static_cast<uint8_t >(resource)] >= amount;
}

void Player::Expand(ResourceType resource, uint8_t amount) {
	m_availableResources[static_cast<uint8_t >(resource)] -= amount;
}

uint8_t Player::GetArmySize() const {
	return static_cast<uint8_t>(std::count_if(GetPlayedCards().begin(), GetPlayedCards().end(),
		   [](auto card) {
			   return card->GetAction() == DevelopmentCardAction::KnightCard;
		   }));
}

uint8_t Player::GetFreeSettlementsCount() const {
    return m_freeSettlements;
}

uint8_t Player::GetFreeRoadsCount() const {
    return m_freeRoads;
}





