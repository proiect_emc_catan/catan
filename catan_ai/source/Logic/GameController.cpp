#include <Logic/GameController.h>
#include <GameObjects/DevelopmentCard.h>

#include "Logic/GameController.h"

#include "Graphics/GUI.h"
#include "SFML/Graphics/Color.hpp"


GameController::GameController() :
	m_window("Catan"), m_currentPlayerState(CurrentPlayerState::InitialState), m_playingDice(6), m_turnCounter(1)
{
	m_winFlag = false;
	m_developmentCardPlayedThisTurn = false;
	m_diceRolledThisTurn = false;
	m_guiPointer = new GUI(m_window, *this);
	m_foreseeAsyncAgent = nullptr;
}

GameController::~GameController()
{
	delete m_guiPointer;
}

Board & GameController::GetBoard()
{
	return m_gameBoard;
}

void GameController::InitialSetup()
{
	Player* head;
	m_numberOfPlayers = 2;
	std::string name;
	name = "Ai";
	m_players.push_back(new Player(name));
	m_agent = std::make_unique<AIAgent>(m_players);
	std::cout << "Enter your name:";
	std::cin >> name;
	m_players.push_back(new Player(name));
	SetPlayerOrder();
	for (uint8_t index = 0; index < m_players.size(); ++index)
	{
		if (m_players[index]->GetName() == "Ai")
		{
			m_agentIndex = index;
		}
	}
	m_guiPointer->SetGameInfoFeed("Player: " + GetCurrentPlayer().GetName() + " on move");

	m_guiPointer->UpdateWithPlayersNames();
	m_foreseeAsyncAgent = new AIAgent(m_players);
}

void GameController::UpdateLongestRoad()
{
	auto longestRoad = m_gameBoard.GetLongestRoad();
	if (longestRoad.second > m_longestRoad.second) {
		m_longestRoad.first = std::make_shared<const Player>(longestRoad.first);
		m_longestRoad.second = longestRoad.second;
	}
}

/// Call just before going to next players turn
void GameController::UpdateLargestArmy()
{
	auto armySize = static_cast<uint8_t>(std::count_if(GetCurrentPlayer().GetPlayedCards().begin(), GetCurrentPlayer().GetPlayedCards().end(),
		[](auto card) {
		return card->GetAction() == DevelopmentCardAction::KnightCard;
	}));
	if (armySize > m_largestArmy.second) {
		m_largestArmy.first = std::make_shared<const Player>(GetCurrentPlayer());
		m_largestArmy.second = armySize;
	}
}
bool GameController::CheckIfCurrentPlayerWon()
{
	if (CountPlayerVP(GetCurrentPlayer()) >= 10) {
		m_winFlag = true;
		return true;
	}
	return false;
}


void GameController::SetPlayerOrder()
{
	for (int index = 0; index < m_players.size(); ++index) {
		RollDie();
		std::cout << m_players[index]->GetName() << " rolled a " << m_currentRolledValue << std::endl;
		m_playerOrder.emplace_back(index, m_currentRolledValue);
	}
	std::sort(m_playerOrder.begin(), m_playerOrder.end(), [](const std::pair<int, int> &left, const std::pair<int, int> &right) {
		return left.second > right.second;
	});
	m_currentPlayerIndex = 0;

}

int GameController::GetCurrentPlayerIndex() const
{
	return m_currentPlayerIndex;
}

std::vector<Player*>& GameController::GetPlayers()
{
	return m_players;
}

Player & GameController::GetSpecificPlayer(uint8_t index)
{
	return *m_players[m_playerOrder[index].first];
}

Player & GameController::GetCurrentPlayer()
{
	return *m_players[m_playerOrder[m_currentPlayerIndex].first];
}

GameController::CurrentPlayerState GameController::GetCurrentPlayerState() const
{
	return m_currentPlayerState;
}


void GameController::SetCurrentPlayerState(const CurrentPlayerState & otherPlayerState)
{
	m_currentPlayerState = otherPlayerState;
}

void GameController::PrintCurrentPlayerOrder() const
{
	std::cout << "The order of the players is \n";
	for (int index = 0; index < m_players.size(); ++index) {
		std::cout << m_players[m_playerOrder[index].first]->GetName() << std::endl;
	}
}

void GameController::Play()
{
	try {
		InitialSetup();
		PrintCurrentPlayerOrder();
		ReinitFieldsForTurn();
	}
	catch (const char * exception)
	{
		std::cout << "Invalid Number Of Players";
		Quit();
	}
	while (m_window.IsOpen()) {
		if (GetCurrentPlayer().GetName() == m_players[m_agentIndex]->GetName())
		{
			GameState state(m_gameBoard, GetCurrentPlayer());
			m_guiPointer->RefreshCards(GetCurrentPlayer().GetCardsCount());
			m_guiPointer->RefreshIcons(GetCurrentPlayer().GetAvailableResources());
			if(m_turnCounter <= 2)
				for (uint8_t i = 0; i < 2; ++i)
				{
					m_guiPointer->SetGameInfoFeed("Player: " + GetCurrentPlayer().GetName() + " on move");
					state = GameState(m_gameBoard, GetCurrentPlayer());
					auto bestAction = m_agent->FindBestAction(state, GetCurrentPlayer(), 3);
					state = m_agent->ExecuteAction(state, bestAction.first);
					m_gameBoard = state.GetBoard();
				}
			else
			{
				m_guiPointer->SetGameInfoFeed("Player: " + GetCurrentPlayer().GetName() + " on move");
				state = GameState(m_gameBoard, GetCurrentPlayer());
				auto bestAction = m_agent->FindBestAction(state, GetCurrentPlayer(), 3);
				if (bestAction.first.GetAction() == PlayerGameActionType::END_TURN)
					state = m_agent->ExecuteAction(state, PlayerGameAction(PlayerGameActionType::DICE_ROLL, GetCurrentPlayer(), Dice(6).RollTwoDice()));
				else
					state = m_agent->ExecuteAction(state, bestAction.first);
				m_gameBoard = state.GetBoard();
			}
			NextTurn();
			++m_turnCounter;
			m_guiPointer->RefreshCards(GetCurrentPlayer().GetCardsCount());
			m_guiPointer->RefreshIcons(GetCurrentPlayer().GetAvailableResources());
			m_guiPointer->SetGameInfoFeed("Player: " + GetCurrentPlayer().GetName() + " on move");
			m_guiPointer->RefreshPieces();
			m_window.Clear(sf::Color::Black);
			m_guiPointer->Draw();
			m_window.Display();
		}
		WindowEventHandler::MainWindowHandle(m_window, *this, *m_guiPointer);
		m_window.Clear(sf::Color::Black);
		m_guiPointer->Draw();
		m_window.Display();
		if (m_winFlag) {
			m_window.CloseWindow();
		}
	}
}




void GameController::NextTurn()
{
	UpdateLargestArmy();
	UpdateLongestRoad();
	if (CheckIfCurrentPlayerWon())
		return;

	if (AreAllPlayersSettled()) {
		SetCurrentPlayerState(GameController::CurrentPlayerState::PressingButtons);
	}

	if (m_currentPlayerIndex == m_numberOfPlayers - 1) {
		m_currentPlayerIndex = 0;
	}
	else {
		m_currentPlayerIndex++;
	}
	ReinitFieldsForTurn();
}

uint8_t GameController::RollDie()
{
	m_diceRolledThisTurn = true;
	m_currentRolledValue = m_playingDice.RollTwoDice();
	if (m_currentPlayerState != CurrentPlayerState::InitialState)
		std::cout << GetCurrentPlayer().GetName() << " rolled " << static_cast<int>(m_currentRolledValue) << "\n";
	return m_currentRolledValue;
}

uint8_t GameController::GetNumberOfPlayers()
{
	return m_numberOfPlayers;
}

void GameController::Quit()
{
	m_window.CloseWindow();
}

uint8_t GameController::CountPlayerVP(const Player &player) const {
	uint8_t vp = m_gameBoard.CountBuildingScore(player);
	vp += std::count_if(player.GetPlayedCards().begin(), player.GetPlayedCards().end(),
		[](auto card) {
		return card->GetAction() == DevelopmentCardAction::VictoryPointCard;
	});

	if (m_longestRoad.first.get() == &player && m_longestRoad.second >= 5) {
		vp += 2;
	}
	if (m_largestArmy.first.get() == &player && m_largestArmy.second >= 3) {
		vp += 2;
	}
	return vp;
}

bool GameController::AreAllPlayersSettled() const {
	for (const Player* player : m_players)
		if (player->GetAvailable(PieceType::Road) > 13)
			return false;
	return true;
}

void GameController::ReinitFieldsForTurn() {
	m_developmentCardPlayedThisTurn = false;
	m_diceRolledThisTurn = false;
	builtRoad = false;
	builtSettlement = false;
	std::cout << "Now is " << GetCurrentPlayer().GetName() << "'s turn\n";
	if (&GetCurrentPlayer() != m_players[m_agentIndex]) {
        StartLookingForHint(GetCurrentPlayer());
    }
	else {
		ShortCircuitHint();
	}
}

bool GameController::ExchangeWithBank(uint8_t exchangeResource) {
	if (m_gameBoard.CanPlayerExchangeWithBank(GetCurrentPlayer(), ResourceType(exchangeResource))) {
		m_gameBoard.PlayerExchangeWithBank(GetCurrentPlayer(), ResourceType(exchangeResource), ResourceType(wantedResource));
		return true;
	}
	return false;
}

void GameController::StartLookingForHint(Player& player) {
	if (m_futureHint.valid()) {
		m_foreseeAsyncAgent->SendStopSignal();
		m_futureHint.wait();
	}
	GameState currentGameState(GetBoard(), player);
	//m_foreseeAsyncAgent->FindBestAction(currentGameState, player, 3);
	//m_futureHint = std::async(std::launch::async, [&](){return m_foreseeAsyncAgent->FindBestAction(currentGameState, player, 3);});
	Player* pp = &player;
	m_futureHint = std::async(std::launch::async, [=](){return HintWorker(currentGameState, pp);});
}

StateProp GameController::GetBestForeseenAction() {
	return m_futureHint.get();
}

StateProp GameController::HintWorker(GameState state, Player *player) {
	return m_foreseeAsyncAgent->FindBestAction(state, *player, 3);
}

void GameController::ShortCircuitHint() {
    if (m_futureHint.valid()) {
        m_foreseeAsyncAgent->SendStopSignal();
        m_futureHint.wait();
    }
}





