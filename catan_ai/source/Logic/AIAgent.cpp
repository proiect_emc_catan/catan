//
// Created by timi on 28.12.2018.
//

#include <Logic/AIAgent.h>
#include <GameObjects/DevelopmentCard.h>
#include <algorithm>
#include <iostream>

const std::array<double, 13> AIAgent::DiceProbabilities = { 0, 0, 1.0 / 36, 2.0 / 36, 3.0 / 36, 4.0 / 36, 5.0 / 36, 6.0 / 36, 5.0 / 36, 4.0 / 36, 3.0 / 36, 2.0 / 36, 1.0 / 36 };

std::vector<PlayerGameAction> AIAgent::GetAvailableActionsForPlayer(const Board &board, Player &player) {
	std::vector<PlayerGameAction> availableActions;
	if (player.CanPickPiece(PieceType::Settlement)) {
		for (uint8_t row = 0; row < board.GetBuildSpotMap().size(); row++)
			for (uint8_t col = 0; col < board.GetBuildSpotMap()[row].size(); col++)
				if (board.CanPlaceBuilding(row, col, PieceType::Settlement, player))
					availableActions.emplace_back(PlayerGameActionType::BUILD_SETTLEMENT, player, std::make_pair(row, col));
	}
	if (player.CanPickPiece(PieceType::Road)) {
		for (uint8_t row = 0; row < board.GetBuildSpotMap().size(); row++)
			for (uint8_t col = 0; col < board.GetBuildSpotMap()[row].size(); col++) {
				for (uint8_t dir = 0; dir < 3; dir++) {
					const auto[newX, newY] = Board::StepDirection(dir, row, col);
					if (newX >= 0 && newX < board.GetBuildSpotMap().size() &&
						newY >= 0 && newY < board.GetBuildSpotMap()[newX].size() &&
						board.CanPlaceRoad(row, col, newX, newY, PieceType::Road, player))
						availableActions.emplace_back(PlayerGameActionType::BUILD_ROAD, player, std::make_tuple(row, col, newX, newY));

				}
			}
	}
	if (player.CanPickPiece(PieceType::City)) {
		for (uint8_t row = 0; row < board.GetBuildSpotMap().size(); row++)
			for (uint8_t col = 0; col < board.GetBuildSpotMap()[row].size(); col++)
				if (board.CanPlaceBuilding(row, col, PieceType::City, player))
					availableActions.emplace_back(PlayerGameActionType::BUILD_CITY, player, std::make_pair(row, col));
	}
	for (uint8_t offerResource = 0; offerResource < ResourcesTypeCount; offerResource++) {
		if (board.CanPlayerExchangeWithBank(player, ResourceType(offerResource))) {
			for (uint8_t getResource = 0; getResource < ResourcesTypeCount; getResource++)
				if (offerResource != getResource)
					availableActions.emplace_back(PlayerGameActionType::TRADE_WITH_BANK, player,
						std::make_pair(ResourceType(offerResource), ResourceType(getResource)));
		}
	}
	if (player.GetFreeSettlementsCount() == 0 && player.GetAvailable(PieceType::Road) <= 13)
		availableActions.emplace_back(player);

	for (int index = 0; index < 5; ++index)
	{
		if (player.HasCard(static_cast<DevelopmentCardAction>(index)))
		{
			availableActions.emplace_back(PlayerGameActionType::PLAY_DEVELOPMENT,player,static_cast<DevelopmentCardAction>(index));
		}
	}

	return availableActions;
}

/*
 * Calculates a score of how good a State is to be in for a specific Player
 */

uint8_t AIAgent::EvaluateExpectedVP(const GameState &state, const Player &player) const {
	uint8_t vp = state.GetBoard().CountBuildingScore(player);
	vp += std::count_if(player.GetPlayedCards().begin(), player.GetPlayedCards().end(),
		[](auto card) {
		return card->GetAction() == DevelopmentCardAction::VictoryPointCard;
	});

	const auto& longestRoad = state.GetBoard().GetLongestRoad();
	auto myArmySize = player.GetArmySize();
	bool biggestArmy = true;
	for (const Player* p : playingPlayers)
		if (p != &player && p->GetArmySize() > myArmySize)
			biggestArmy = false;

	if (&longestRoad.first == &player && longestRoad.second >= 5) {
		vp += 2;
	}
	if (biggestArmy && myArmySize >= 3) {
		vp += 2;
	}
	return vp;
}

float AIAgent::HeuristicStateEvaluation(const GameState &state, const Player &player) const {
	/// Simplest Heuristic would be the number of VP the player has
	float vp = EvaluateExpectedVP(state, player);
	float hp = 0;
	/// Add heuristic points for resources
	const float basicResourceValue = 0.095;
	const float firstResourceFactor = 1.5f;
	const float after4ReduceFactor = 4.0f;
	const float diceValueFactor = 2.34f;
	for (uint8_t res = 0; res < ResourcesTypeCount; ++res) {
		int available = static_cast<int>(player.GetAvailableResources()[res]);
		if (available > 0) {
			hp += firstResourceFactor * basicResourceValue; /// It's good to have at least one resource of each kind
			available--;
			hp += std::min(3, available) * basicResourceValue;
			hp += std::max(0, available - 3) * basicResourceValue / after4ReduceFactor; /// It's not as worthed to stack a lot of the same resource
		}
	}
	std::set<Board::TileType> neighbourTiles;
	auto spotMap = state.GetBoard().GetBuildSpotMap();
	for (uint8_t line = 0; line < spotMap.size(); ++line)
	{
		for (uint8_t column = 0; column < spotMap[line].size(); ++column)
		{
			if (spotMap[line][column] != std::nullopt)
			{
				if (spotMap[line][column].value()->GetOwner().GetName() == player.GetName())
				{
					auto tilesAroundBuilding = state.GetBoard().GetNumberAndResourceFromTilesAroundBuilding(line, column);
					for (auto it : tilesAroundBuilding)
					{
						hp += diceValueFactor * Piece::pieceValue[static_cast<uint8_t >(spotMap[line][column].value()->GetType())]
								* DiceProbabilities[it.first];
						if (it.second != Board::TileType::Desert)
						{
							neighbourTiles.insert(it.second);
						}
					}
				}
			}
		}
	}
	const auto differentTilesPoints = std::array{0.0f, 0.1f, 0.2f, 0.3f, 0.5f, 1.0f};
	hp += differentTilesPoints[neighbourTiles.size()];
	return vp + hp;
}

AIAgent::AIAgent(std::vector<Player*> & players) : playingPlayers(players) {
}

GameState AIAgent::ExecuteAction(const GameState &state, const PlayerGameAction &action) {
    Board newBoard(state.GetBoard());
    if (action.GetAction() == PlayerGameActionType::BUILD_SETTLEMENT) {
        const auto& position = action.GetBuildSpot();
        const auto piece = action.GetActionMaker().BuyPiece(PieceType::Settlement);
        //const auto piece = std::make_shared<Piece>(PieceType::Settlement, action.GetActionMaker());
        newBoard.PlaceBuilding(position.first, position.second, piece);
    }
    else if (action.GetAction() == PlayerGameActionType::BUILD_ROAD) {
        const auto& position = action.GetRoadLocation();
        const auto piece = action.GetActionMaker().BuyPiece(PieceType::Road);
        newBoard.PlaceRoad(std::get<0>(position), std::get<1>(position), std::get<2>(position), std::get<3>(position), piece);
    }
    else if (action.GetAction() == PlayerGameActionType::BUILD_CITY) {
        const auto& position = action.GetBuildSpot();
        const auto piece = action.GetActionMaker().BuyPiece(PieceType::City);
        newBoard.PlaceBuilding(position.first, position.second, piece);
    }
    else if (action.GetAction() == PlayerGameActionType::TRADE_WITH_BANK) {
        newBoard.PlayerExchangeWithBank(action.GetActionMaker(),
                action.GetResourceExchange().first, action.GetResourceExchange().second);
    }
    else if (action.GetAction() == PlayerGameActionType::DICE_ROLL) {
        const auto& diceValue = action.GetDiceValue();
        // newBoard.TriggerObturateResourceGathering(diceValue, action.GetActionMaker());
        newBoard.TriggerResourceGathering(diceValue);
        if (diceValue == 7)
			for (auto playingPlayer : playingPlayers) {
				playingPlayer->DiscardHalfResourcesIfNecessary();
			}
    }
    else if (action.GetAction() == PlayerGameActionType::BUY_DEVELOPMENT) {
		action.GetActionMaker().BuyPiece(PieceType::DevelopmentCard);
	}
    else if (action.GetAction() == PlayerGameActionType::PLAY_DEVELOPMENT) {
		const auto& developmentCard = action.GetDevelopmentCardAction();
		newBoard.PlayerPlayCard(action.GetActionMaker(), developmentCard);
	}
	// TODO Implement all other actions
	return GameState(newBoard, state.GetPlayer());
}

std::pair<PlayerGameAction, float>  AIAgent::FindBestAction(const GameState &gameState, Player &player, uint8_t maxTreeDepth) {
    this->m_stateMemoization.clear();
    m_stopFlag = false;

    ///DEBUG
    /*for (int i = 0; i < gameState.GetBoard().GetBuildSpotMap().size(); ++i)
        for (int j = 0; j < gameState.GetBoard().GetBuildSpotMap()[i].size(); ++j)
            std::cout << gameState.GetBoard().GetBuildSpotMap()[i][j].has_value() << " ";
    std::cout << "\n";*/

    std::vector<Player> copyPlayersState;
	for (uint8_t i = 0; i < playingPlayers.size(); i++)
		copyPlayersState.push_back(*playingPlayers[i]);
    StateProp stateProp = FindBestAction(gameState, player, maxTreeDepth, this->m_stateMemoization);
    for (uint8_t i = 0; i < playingPlayers.size(); i++)
        *playingPlayers[i] = copyPlayersState[i];
    return stateProp;
}

std::pair<PlayerGameAction, float>
AIAgent::FindBestAction(const GameState &gameState, Player &player, uint8_t maxTreeDepth,
	std::unordered_map<GameState, StateProp, SimpleGameStateHasher> &stateMemoization) {
	if (maxTreeDepth == 0 || m_stopFlag)
		return { PlayerGameAction(player), HeuristicStateEvaluation(gameState, player) };
	const auto memIt = stateMemoization.find(gameState);
	if (memIt != stateMemoization.end())
		return memIt->second;
	const auto& available = GetAvailableActionsForPlayer(gameState.GetBoard(), player);
	std::pair<PlayerGameAction, float> bestAction{ PlayerGameAction(player), -1 };
	Player savedPlayer = player;
	bool hasEnd = false;
	for (const auto& action : available) {
	    if (m_stopFlag)
	        break;
		if (action.GetAction() == PlayerGameActionType::END_TURN) {
			hasEnd = true;
			continue;
		}
		const GameState& newState = ExecuteAction(gameState, action);
		const auto& result = FindBestAction(newState, player, static_cast<uint8_t>(maxTreeDepth - 1), stateMemoization);

		/// Simplified Revert Action
		// RevertAction(newState, action);
		player = savedPlayer;

		if (result.second > bestAction.second) {
			bestAction.second = result.second;
			bestAction.first = action;
		}
	}
	if (m_stopFlag)
        // return { PlayerGameAction(player), HeuristicStateEvaluation(gameState, player) };
	    return bestAction;

    /// End Turn
	if (hasEnd) {
		float expectedValue = 0;
		for (uint8_t diceValue = 2; diceValue <= 12; diceValue++) {
		    if (m_stopFlag)
                // return { PlayerGameAction(player), HeuristicStateEvaluation(gameState, player) };
                return bestAction;

            PlayerGameAction diceAction(PlayerGameActionType::DICE_ROLL, player, diceValue);
			const GameState &newState = ExecuteAction(gameState, diceAction);
			const auto &result = FindBestAction(newState, player, static_cast<uint8_t>(maxTreeDepth - 1), stateMemoization);
			// RevertAction(newState, diceAction);
			player = savedPlayer;
			expectedValue += AIAgent::DiceProbabilities[diceValue] * result.second;
		}
		if (expectedValue > bestAction.second) {
			bestAction.second = expectedValue;
			bestAction.first = PlayerGameAction(player); /// Default End_Turn action
		}
	}
	stateMemoization.insert({ gameState, bestAction });
	return bestAction;
}

void AIAgent::SendStopSignal() {
	m_stopFlag = true;
}




