//
// Created by timi on 28.12.2018.
//

#include <Logic/GameState.h>

/*GameState::GameState(const Board &board) {
    this->board = board;
    this->player = nullptr;
}

GameState::GameState(const Board &board, Player& player) {
    this->board = board;
    this->player = std::shared_ptr<Player>(&player);
}

GameState::GameState(const Board &board, const std::shared_ptr<Player> &playerPtr) : board(board), player(playerPtr) {
    this->precomputedHash = ComputeSimpleHash();
}
 */

GameState::GameState(const Board &board, Player& player) : board(board), player(player) {
    this->precomputedHash = ComputeSimpleHash();
}

const Board &GameState::GetBoard() const {
    return board;
}

Player& GameState::GetPlayer() const {
    return player;
}

bool GameState::operator==(const GameState &otherState) const {
    if (&otherState.GetPlayer() != &player.get())
        return false;
    return board == otherState.GetBoard();
}

std::size_t GameState::ComputeSimpleHash() const {
    size_t hash = 0;
    for (const auto& row : GetBoard().GetTileMap())
        for (const auto& val : row)
            hash = hash * (static_cast<uint8_t >(Board::TileType::Desert) +1) + static_cast<uint8_t >(val);

    for (const auto& row : GetBoard().GetBuildSpotMap())
        for (const auto& val : row)
            hash = (hash << 1) + val.has_value();

    for (const auto& row : GetBoard().GetNumberMap())
        for (const auto& val : row)
            hash = hash * 12 + val - 1; /// 12 is the highest number that one can roll

    const uint8_t high = GetBoard().GetBuildSpotMap().size();
    for (const auto&[loc, road] : GetBoard().GetPlacedRoads()) {
        hash = hash*high*high*high*high + std::get<0>(loc) * high * high * high
               + std::get<1>(loc) * high * high
               + std::get<2>(loc) * high
               + std::get<3>(loc);
    }

    hash = hash * high * high + GetBoard().GetThiefLocation().first * high + GetBoard().GetThiefLocation().second;

    const auto& stringHasher = std::hash<std::string>{};
    hash ^= stringHasher(GetPlayer().GetName());

    /// Add players resources to hash
    size_t playerHash = 0;
    for (uint8_t res = 0; res < ResourcesTypeCount; ++res)
        playerHash = playerHash*131 + player.get().GetAvailableResources()[res]; /// 131 Should be about the max exp number of resources
    for (const auto& played : player.get().GetPlayedCards())
        playerHash = playerHash*5 + static_cast<uint8_t >(played->GetAction());
    for (const auto& played : player.get().GetPlayableCards())
        playerHash = playerHash*5 + static_cast<uint8_t >(played->GetAction());

    return hash ^ playerHash;
}


std::size_t SimpleGameStateHasher::operator()(const GameState &state) const {
    return state.precomputedHash;
}
