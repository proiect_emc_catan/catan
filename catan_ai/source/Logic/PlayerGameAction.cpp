//
// Created by timi on 28.12.2018.
//

#include <Logic/PlayerGameAction.h>

PlayerGameActionType PlayerGameAction::GetAction() const {
    return m_actionType;
}

std::pair<uint8_t, uint8_t> PlayerGameAction::GetBuildSpot() const {
    if (!std::holds_alternative<std::pair<uint8_t, uint8_t> >(m_actionSpecific))
        throw "Invalid Variant";
    return std::get<std::pair<uint8_t, uint8_t>>(m_actionSpecific);
}

DevelopmentCardAction PlayerGameAction::GetDevelopmentCardAction() const {
    if (!std::holds_alternative<DevelopmentCardAction >(m_actionSpecific))
        throw "Invalid Variant";
    return std::get<DevelopmentCardAction>(m_actionSpecific);
}

std::tuple<uint8_t, uint8_t, uint8_t, uint8_t> PlayerGameAction::GetRoadLocation() const {
    if (!std::holds_alternative<std::tuple<uint8_t, uint8_t, uint8_t, uint8_t>>(m_actionSpecific))
        throw "Invalid Variant";
    return std::get<std::tuple<uint8_t, uint8_t, uint8_t, uint8_t>>(m_actionSpecific);
}

std::pair<ResourceType, ResourceType> PlayerGameAction::GetResourceExchange() const {
    if (!std::holds_alternative<std::pair<ResourceType, ResourceType>>(m_actionSpecific))
        throw "Invalid Variant";
    return std::get<std::pair<ResourceType, ResourceType>>(m_actionSpecific);
}

uint8_t PlayerGameAction::GetDiceValue() const {
    if (!std::holds_alternative<uint8_t >(m_actionSpecific))
        throw "Invalid Variant";
    return std::get<uint8_t >(m_actionSpecific);
}


Player &PlayerGameAction::GetActionMaker() const {
    return actionMaker;
}

PlayerGameAction::PlayerGameAction(PlayerGameActionType actionType, Player &actionMaker,
                                   std::pair<uint8_t, uint8_t> position)
       : m_actionType(actionType), actionMaker(actionMaker), m_actionSpecific(position)
{

}

PlayerGameAction::PlayerGameAction(PlayerGameActionType actionType, Player &actionMaker,
                                   std::tuple<uint8_t, uint8_t, uint8_t, uint8_t> loc)
        : m_actionType(actionType), actionMaker(actionMaker), m_actionSpecific(loc)
{

}

PlayerGameAction::PlayerGameAction(PlayerGameActionType actionType, Player &actionMaker, DevelopmentCardAction developmentCardAction)
        : m_actionType(actionType), actionMaker(actionMaker), m_actionSpecific(developmentCardAction)
{

}

PlayerGameAction::PlayerGameAction(PlayerGameActionType actionType, Player &actionMaker,
                                   std::pair<ResourceType, ResourceType> exchange)
        : m_actionType(actionType), actionMaker(actionMaker), m_actionSpecific(exchange)
{

}

PlayerGameAction::PlayerGameAction() : actionMaker(Piece::nobody) {
}

PlayerGameAction::PlayerGameAction(Player &actionMaker) : m_actionType(PlayerGameActionType::END_TURN), actionMaker(actionMaker), m_actionSpecific(0) {

}

PlayerGameAction::PlayerGameAction(const PlayerGameAction &copy) : m_actionType(copy.m_actionType), actionMaker(copy.actionMaker), m_actionSpecific(copy.m_actionSpecific) {

}

PlayerGameAction &PlayerGameAction::operator=(const PlayerGameAction &copy) {
    this->m_actionType = copy.m_actionType;
    this->m_actionSpecific = copy.m_actionSpecific;
    this->actionMaker = copy.actionMaker;
    return *this;
}

PlayerGameAction::PlayerGameAction(PlayerGameActionType actionType, Player &actionMaker, std::uint8_t diceValue) : m_actionType(actionType), actionMaker(actionMaker), m_actionSpecific(diceValue) {

}


