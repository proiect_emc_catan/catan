#pragma once

class BasicWindow;
class WindowEventHandler;
class GUI;

#include <iostream>
#include <array>
#include <memory>
#include <string>
#include <future>
#include "Graphics/WindowEventHandler.h"
#include "Logic/AIAgent.h"
#include "SFML/Graphics.hpp"
#include "Graphics/BasicWindow.h"
#include "Graphics/DrawableObject.h"
#include "Graphics/Button.h"
#include "Graphics/GUI.h"
#include "GameObjects/Dice.h"
#include "GameObjects/Player.h"
#include "GameObjects/Board.h"

class GameController {

public:
	enum class CurrentPlayerState {
		InitialState,
		PressingButtons,
		MovingThief,
		PickResourceForMonopoly,
		ChoosingWantedResource,
		ChoosingExchangeResource,
		TradeWithPlayer,
		Build,
		Trade
	};

public:
	GameController();
	~GameController();
	void Play();
	void Quit();
	void NextTurn();
	uint8_t RollDie();
	/// Returns wether the exchange was successful or not
	bool ExchangeWithBank(uint8_t exchangeResource);
	uint8_t GetNumberOfPlayers();
	int GetCurrentPlayerIndex() const;
	std::vector<Player*>& GetPlayers();
	Player & GetSpecificPlayer(uint8_t index);
	Player & GetCurrentPlayer();
	CurrentPlayerState GetCurrentPlayerState()const; 
	void SetCurrentPlayerState(const CurrentPlayerState & otherPlayerState); 
	void PrintCurrentPlayerOrder() const;
	uint8_t CountPlayerVP(const Player& player) const;
	friend WindowEventHandler;
    Board& GetBoard();
    bool AreAllPlayersSettled() const;
    StateProp GetBestForeseenAction(); // Blocking call
    void StartLookingForHint(Player& player); // Non blocking, starts searching on a new thread
    void ShortCircuitHint();

private:
	void InitialSetup();
	void UpdateLongestRoad();
	void UpdateLargestArmy();
	bool CheckIfCurrentPlayerWon();
	void SetPlayerOrder();
	void ReinitFieldsForTurn();
	StateProp HintWorker(GameState state, Player *player);
private:
	BasicWindow m_window;
	bool m_winFlag;
	bool m_developmentCardPlayedThisTurn;
	bool m_diceRolledThisTurn;
	Dice m_playingDice;
	uint8_t m_numberOfPlayers;
	uint8_t m_currentRolledValue;
	Board m_gameBoard;
	std::unique_ptr<AIAgent> m_agent;
	uint8_t m_agentIndex;
	std::vector<DrawableObject> m_objects;
	std::vector<Player*> m_players;
	std::vector <std::pair<int, int>> m_playerOrder;
	std::pair<std::shared_ptr<const Player>, uint8_t> m_longestRoad;
	std::pair<std::shared_ptr<const Player>, uint8_t> m_largestArmy;
	GUI *m_guiPointer;
	CurrentPlayerState m_currentPlayerState;
	int m_currentPlayerIndex;
	uint16_t m_turnCounter;
	uint8_t wantedResource;

	AIAgent* m_foreseeAsyncAgent;
	std::future<StateProp > m_futureHint;

public:
	bool builtSettlement, builtRoad;
};