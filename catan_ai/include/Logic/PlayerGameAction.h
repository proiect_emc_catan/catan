//
// Created by timi on 28.12.2018.
//

#ifndef CATAN_PLAYERGAMEACTION_H
#define CATAN_PLAYERGAMEACTION_H

#include <variant>
#include "GameObjects/Board.h"

/*
 * Dice roll should not be used as an 'available' action. It will be used internally at the end of the turn
 */
enum class PlayerGameActionType {
    BUILD_SETTLEMENT,
    BUILD_CITY,
    BUILD_ROAD,
    BUY_DEVELOPMENT,
    PLAY_DEVELOPMENT,
    TRADE_WITH_BANK,
    DICE_ROLL,

    END_TURN
};

const uint8_t ACTION_SPACE_SIZE = static_cast<uint8_t >(PlayerGameActionType::END_TURN) + 1;

class PlayerGameAction {
public:
	PlayerGameAction();
    explicit PlayerGameAction(Player& actionMaker);
    PlayerGameAction(PlayerGameActionType actionType, Player& actionMaker, std::uint8_t diceValue);
    PlayerGameAction(PlayerGameActionType actionType, Player& actionMaker, std::pair<uint8_t, uint8_t> position);
    PlayerGameAction(PlayerGameActionType actionType, Player& actionMaker, std::tuple<uint8_t , uint8_t , uint8_t , uint8_t >);
    PlayerGameAction(PlayerGameActionType actionType, Player& actionMaker, DevelopmentCardAction);
    PlayerGameAction(PlayerGameActionType actionType, Player& actionMaker, std::pair<ResourceType , ResourceType >);


    PlayerGameAction(const PlayerGameAction& copy);
    PlayerGameAction&operator=(const PlayerGameAction& copy);

    PlayerGameActionType GetAction() const;
    Player& GetActionMaker() const;
    std::pair<uint8_t , uint8_t > GetBuildSpot() const;
    std::tuple<uint8_t , uint8_t , uint8_t , uint8_t > GetRoadLocation() const;
    DevelopmentCardAction GetDevelopmentCardAction() const;
    std::pair<ResourceType , ResourceType > GetResourceExchange() const;
    uint8_t GetDiceValue() const;

private:
    PlayerGameActionType m_actionType;
    Player& actionMaker;
    std::variant<
    std::pair<uint8_t , uint8_t >, /// Location of settlement / city
    std::tuple<uint8_t , uint8_t , uint8_t , uint8_t >, /// Identifies road location
    DevelopmentCardAction, /// Bought / Played Development Card
    std::pair<ResourceType , ResourceType >, /// Offer Resource <-> Get Resource
    uint8_t /// Value used for dice roll / 0 for no action
    > m_actionSpecific ;
};


#endif //CATAN_PLAYERGAMEACTION_H
