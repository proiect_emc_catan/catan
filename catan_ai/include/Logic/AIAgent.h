//
// Created by timi on 28.12.2018.
//

#ifndef CATAN_AIAGENT_H
#define CATAN_AIAGENT_H

#include "GameObjects/Board.h"
#include "PlayerGameAction.h"
#include "GameState.h"

#include <vector>
#include <variant>
#include <atomic>
#include <unordered_map>

using StateProp = std::pair<PlayerGameAction, float>;

class AIAgent {

public:
    explicit AIAgent(std::vector<Player*>& );
    static const std::array<double, 13> DiceProbabilities;

public:
    std::vector<PlayerGameAction> GetAvailableActionsForPlayer(const Board& board, Player& player);
    GameState ExecuteAction(const GameState& state, const PlayerGameAction& action);

    /// This method is needed because the board keeps track of ownership by player reference, thus
    /// action that alter a player's state must mutate the object and restore the attributes after the
    /// game state subtree is evaluated. On the other hand the board is copied, the original not being mutated
    void RevertAction(const GameState& state, const PlayerGameAction& action) const; // Deprecated, found a workaround

    uint8_t EvaluateExpectedVP(const GameState &state, const Player &player) const;
    float HeuristicStateEvaluation(const GameState& state, const Player& player) const;

    /// Returns the action and estimated final state Evaluation
    StateProp FindBestAction(const GameState& gameState, Player& player, uint8_t maxTreeDepth);

    void SendStopSignal();

private:
    std::vector<Player*>& playingPlayers;
    StateProp FindBestAction(const GameState& gameState, Player& player, uint8_t maxTreeDepth, std::unordered_map<GameState, StateProp, SimpleGameStateHasher>& stateMemoization);
    std::unordered_map<GameState, StateProp, SimpleGameStateHasher> m_stateMemoization;

    std::atomic<bool> m_stopFlag;
};

#endif //CATAN_AIAGENT_H
