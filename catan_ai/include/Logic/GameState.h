//
// Created by timi on 28.12.2018.
//

#ifndef CATAN_GAMESTATE_H
#define CATAN_GAMESTATE_H

#include <GameObjects/Board.h>
#include <GameObjects/DevelopmentCard.h>

class SimpleGameStateHasher;

class GameState {
public:
    /*explicit GameState(const Board& board);
    GameState(const Board& board, Player& player);*/
    //GameState(const Board& board, const std::shared_ptr<Player>& playerPtr);
    GameState(const Board& board, Player& player);

    bool operator==(const GameState& otherState) const;

    const Board& GetBoard() const;
    Player& GetPlayer() const;

private:
    std::size_t ComputeSimpleHash() const;

private:
    Board board; /// Each game state should keep it's own COPY of the board in that state.
    //std::shared_ptr<Player> player; /// If player is selected than GameState only keeps track of that player's state in the board
    std::reference_wrapper<Player> player; /// Dropped the idea of implementing the other type of AI (that tracks all players)
    std::size_t precomputedHash;

    friend SimpleGameStateHasher;
};


class SimpleGameStateHasher {
public:
    std::size_t operator()(const GameState& state) const;
};


#endif //CATAN_GAMESTATE_H
