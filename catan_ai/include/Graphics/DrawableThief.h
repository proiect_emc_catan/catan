#pragma once

#include "Graphics/DrawableObject.h"

class DrawableThief : public DrawableObject
{
public:
	DrawableThief();
	~DrawableThief() = default;
private:
	static sf::Texture m_thiefTexture;
};

