#pragma once

#include "Graphics/DrawableObject.h"
#include "GameObjects/Piece.h"

class DrawablePieceHighlight : public DrawableObject
{
public:
	DrawablePieceHighlight(const PieceType & = PieceType::Settlement);
	~DrawablePieceHighlight() = default;

	void SetType(const PieceType &);
private:
	static sf::Texture m_pieceTexture;
};