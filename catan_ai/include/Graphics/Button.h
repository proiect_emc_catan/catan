#pragma once

#include <string>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "Graphics/DrawableObject.h"

template<typename ButtonInfoType>
class Button : public DrawableObject
{
public:
	Button(const ButtonInfoType &info = ButtonInfoType(), const std::string &buttonText = "") :
		m_storedInformation(info),
		m_isPressed(false),
		m_isHighlighted(false)
	{
		if (m_buttonTexture.getSize().x == 0)
		{
			m_buttonTexture.loadFromFile("resources/sprites/tile_button.png");
			m_sprite.setTexture(m_buttonTexture);
		}
		else
			m_sprite.setTexture(m_buttonTexture);
		m_buttonText.setFont(m_font);
		m_buttonText.setCharacterSize(24);
		m_buttonText.setFillColor(sf::Color::White);
		m_buttonText.setOutlineColor(sf::Color::Black);
		m_buttonText.setOutlineThickness(3);
		m_buttonText.setString(buttonText);
		m_sprite.setTextureRect(sf::IntRect{ 0, 0, 200, 80 });
	}

	void SetButtonDetails(const ButtonInfoType &info, const std::string &buttonText)
	{
		m_storedInformation = info;
		m_buttonText.setString(buttonText);
	}

	void Draw(sf::RenderWindow &drawHere) override
	{
		m_sprite.setScale({ 1.0f, 0.6f });
		if (m_isHighlighted == true) {
			auto position = GetPosition();
			sf::RectangleShape tempHighlight;
			tempHighlight.setSize({ 208, 56 });
			tempHighlight.setPosition({ position.x - 4, position.y - 4 });
			tempHighlight.setFillColor(sf::Color::Cyan);
			drawHere.draw(tempHighlight);
		}
		DrawableObject::Draw(drawHere);
		m_sprite.setScale({ 1.0f, 1.0f });
		sf::Transform transform = this->m_sprite.getTransform();
		transform.translate({ 6, 6 });
		drawHere.draw(m_buttonText, transform);
		m_sprite.setScale({ 1.0f, 0.6f });
	}

	bool IsClicked(const sf::Vector2i &mousePosition) override
	{
		if (DrawableObject::IsClicked(mousePosition) == true)
		{
			m_isPressed = true;
			m_sprite.setTextureRect(sf::IntRect{ 200, 0, 200, 80 });
			return true;
		}
		return false;
	}

	const ButtonInfoType* GetInfo()
	{
		if (m_isPressed == true)
		{
			m_isPressed = false;
			m_sprite.setTextureRect(sf::IntRect{ 0, 0, 200, 80 });
			return &m_storedInformation;
		}
		return nullptr;
	}

	void Highlight() 
	{
		m_isHighlighted = true;
	}

	void Dehighlight()
	{
		m_isHighlighted = false;
	}

	const bool& IsHighlighted()
	{
		return m_isHighlighted;
	}

private:
	static sf::Texture m_buttonTexture;
	ButtonInfoType m_storedInformation;
	sf::Text m_buttonText;
	bool m_isPressed;
	bool m_isHighlighted;
};

template<typename T>
sf::Texture Button<T>::m_buttonTexture;