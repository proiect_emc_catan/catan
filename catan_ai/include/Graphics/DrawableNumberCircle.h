#pragma once

#include <stdint.h>
#include "Graphics/DrawableObject.h"

class DrawableNumberCircle : public DrawableObject
{
public:
	DrawableNumberCircle(uint8_t = 0);
	~DrawableNumberCircle() = default;

	void LoadNewNumber(uint8_t);
private:
	static sf::Texture m_numberTexture;
};

