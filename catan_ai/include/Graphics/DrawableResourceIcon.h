#pragma once

#include "SFML/Graphics/Text.hpp"

#include "Graphics/DrawableObject.h"
#include "GameObjects/ResourceType.h"

class DrawableResourceIcon : public DrawableObject
{
public:
	DrawableResourceIcon(ResourceType = ResourceType::None, const std::string & = "");
	~DrawableResourceIcon() = default;

	void Draw(sf::RenderWindow &) override;
	void SetIconCount(uint16_t = 0);
private:
	static sf::Texture m_iconTexture;
	ResourceType m_type;
	sf::Text m_iconText;
};