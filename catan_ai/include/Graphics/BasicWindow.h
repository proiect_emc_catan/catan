#pragma once

class DrawableLayer;

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <string>
#include <optional>
#include "Graphics/DrawableObject.h"
#include "Graphics/DrawableLayer.h"

class BasicWindow 
{
public:
	BasicWindow(const std::string& = "");
	~BasicWindow() = default;

	void DrawObject(DrawableObject *);
	void DrawLayer(const DrawableLayer &);
	void Clear(const sf::Color &);
	void Display();
	const sf::Vector2u GetSize() const;

	bool GetEvent(sf::Event&);
	bool IsOpen() const;
	void CloseWindow();
private:
	sf::RenderWindow m_window;
};