#pragma once

#include "Graphics/DrawableObject.h"

class DrawableBackground : public DrawableObject
{
public:
	DrawableBackground();
	~DrawableBackground() = default;

	void LoadTexture(const std::string &, const sf::Vector2u &);
private:
	static sf::Texture m_backgroundTexture;
};