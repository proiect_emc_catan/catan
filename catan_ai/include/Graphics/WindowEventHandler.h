#pragma once

class BasicWindow;
class GameController;

#include <SFML/Window/Event.hpp>
#include "Graphics/BasicWindow.h"
#include "Graphics/GUI.h"
#include "Logic/GameController.h"

class WindowEventHandler
{
public:
	static void MainWindowHandle(BasicWindow &, GameController &, GUI &);
};

