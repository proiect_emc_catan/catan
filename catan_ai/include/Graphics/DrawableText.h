#pragma once

#include <string>
#include "SFML/Graphics/Text.hpp"

#include "Graphics/DrawableObject.h"

class DrawableText : public DrawableObject
{
public:
	DrawableText();
	~DrawableText() = default;

	void Draw(sf::RenderWindow &) override;

	void SetText(const std::string &);
private:
	sf::Text m_text;
};

