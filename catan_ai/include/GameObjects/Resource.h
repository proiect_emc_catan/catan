//
// Created by timi on 06.11.2018.
//

#ifndef CATAN_RESOURCE_H
#define CATAN_RESOURCE_H


#include <cstdint>

enum class ResourceType : uint8_t {
    Brick,
    Grain,
    Lumber,
    Ore,
    Wool
};


#endif //CATAN_RESOURCE_H
