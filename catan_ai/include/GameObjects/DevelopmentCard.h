//
// Created by timi on 24.11.2018.
//

#ifndef CATAN_DEVELOPMENTCARD_H
#define CATAN_DEVELOPMENTCARD_H

#include "Piece.h"
#include "DevelopmentCardAction.h"

class DevelopmentCard : public Piece {
public:

public:
    static ResourceArray pool;
public:
    explicit DevelopmentCard(Player& owner);
    DevelopmentCardAction GetAction() const;

private:
    DevelopmentCardAction m_Action;
};


#endif //CATAN_DEVELOPMENTCARD_H
