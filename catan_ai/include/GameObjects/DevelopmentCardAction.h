//
// Created by timi on 24.11.2018.
//

#ifndef CATAN_DEVELOPMENTCARDACTION_H
#define CATAN_DEVELOPMENTCARDACTION_H

#include <cstdint>

enum class DevelopmentCardAction : uint8_t {
    KnightCard,
    RoadBuilding,
    YearOfPlenty,
    Monopoly,
    VictoryPointCard
};

#endif //CATAN_DEVELOPMENTCARDACTION_H
