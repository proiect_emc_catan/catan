//
// Created by timi on 06.11.2018.
//

#ifndef CATAN_PLAYER_H
#define CATAN_PLAYER_H

class DevelopmentCard;
class Piece;

#include <array>
#include <memory>
#include <vector>
#include <optional>
#include "GameObjects/Piece.h"
#include "GameObjects/PieceType.h"
#include "GameObjects/ResourceType.h"
#include "DevelopmentCardAction.h"

const uint8_t ResourcesTypeCount = 5;
using ResourceArray = std::array<uint8_t, ResourcesTypeCount>;

class Player {
public:
	Player();

	explicit Player(const std::string & name);

private:
	void InitAvailablePieces();

public:
	std::string GetName() const;
	bool operator == (const Player & other) const;
	bool CanPickPiece(PieceType type) const;
	std::shared_ptr<Piece> BuyPiece(PieceType type);
	void GiveResource(ResourceType resource, uint8_t amount = 1);
	uint8_t GetAvailable(PieceType piece) const;
	bool HasCard(DevelopmentCardAction cardAction) const;

	/// Assumes HasCard was already checked
	void PlayCard(DevelopmentCardAction cardAction);

	bool CanExpand(ResourceType resource, uint8_t amount = 1) const;
	bool CanExpand(const ResourceArray& resources) const;
	/// Assumes CanExpand was already checked
	void Expand(const ResourceArray& resources);
	void Expand(ResourceType resource, uint8_t amount = 1);

	std::optional<ResourceType> LoseRandomResource();

	/// Returns true if cards were discarded
	bool DiscardHalfResourcesIfNecessary();

	/// Assumes CanExpand was already checked for both players
	void ExchangeWithPlayer(const ResourceArray& giveResources,
			const ResourceArray& getResources, Player& otherPlayer);

	void AddFreeRoads(uint8_t amount);

	const std::vector<std::shared_ptr<DevelopmentCard>>& GetPlayedCards() const;
	const std::vector<std::shared_ptr<DevelopmentCard>>& GetPlayableCards() const;
	const ResourceArray& GetAvailableResources() const;
	std::array<uint16_t, 4> GetCardsCount() const;
	uint8_t GetArmySize() const;
	uint8_t GetFreeSettlementsCount() const;
	uint8_t GetFreeRoadsCount() const;

private:
	std::string m_name;
	std::array<uint8_t, 4> m_availablePieces;
	ResourceArray m_availableResources; // auto initialized with 0 each

	std::vector<std::shared_ptr<DevelopmentCard>> m_devCards;
	std::vector<std::shared_ptr<DevelopmentCard>> m_playedCards;

	uint8_t m_freeSettlements;
	uint8_t m_freeRoads;
};


#endif //CATAN_PLAYER_H
