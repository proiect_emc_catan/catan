//
// Created by timi on 27.11.2018.
//

#ifndef CATAN_HARBOR_H
#define CATAN_HARBOR_H

/// Enum aligned with resource types
enum class HarborType {
    BrickChange,
    GrainChange,
    LumberChange,
    OreChange,
    WoolChange,
    ThreeToOne
};

#endif //CATAN_HARBOR_H
